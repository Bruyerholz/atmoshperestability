
#define X_BLOCK (44)
#define Y_BLOCK (44)
#define Z_BLOCK (164)

#define X_PROC (4)
#define Y_PROC (4)

#define X_TOTAL ((X_BLOCK-4)*X_PROC + 4)
#define Y_TOTAL ((Y_BLOCK-4)*Y_PROC + 4)
#define Z_TOTAL  (Z_BLOCK)

#define X_DRAW ((X_BLOCK-4)*X_PROC)
#define Y_DRAW ((Y_BLOCK-4)*Y_PROC)
#define Z_DRAW  (Z_BLOCK-4)

#define SLICE_SIZE_XY ((X_BLOCK-4)*(Y_BLOCK-4))
#define SLICE_SIZE_XZ ((X_BLOCK-4)*(Z_BLOCK-4))

#define OUTPUT_FREQ (100)

#define FIS_CONST (1.0d0/3.0d0)
#define BET_CONST ((3.0d0-FIS_CONST)/(1.0d0-FIS_CONST))
