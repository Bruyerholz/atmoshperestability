# ----------------------------------------------------------------------
# --- Makefile for KIAE ------------------------------------------------
# ----------------------------------------------------------------------

OBJ_FILES = exoplanet.o helper.o

# ----------------------------------------------------------------------

F77 = /home/alexander/Main/OpenMPI/bin/mpif77
CC  = /home/alexander/Main/OpenMPI/bin/mpicc
LD  = /home/alexander/Main/OpenMPI/bin/mpif77
#F77 = mpif77
#CC  = mpicc
#LD  = mpif77

# ----------------------------------------------------------------------

#FOPTS  = -xT -O3 -zero -pad -132 -convert little_endian
#LDOPTS = -xT -O3 -zero -pad -132 -convert little_endian
#COPTS  = -xT -O3 -ipo

#FOPTS  = -zero -pad -132 -cpp -convert little_endian
#LDOPTS = -zero -pad -132 -cpp -convert little_endian
#COPTS  =

#FOPTS  = -xHost -O3 -zero -pad -132 -cpp -convert little_endian
#LDOPTS = -xHost -O3 -zero -pad -132 -cpp -convert little_endian
#COPTS  = -O3

FOPTS  = -O3 -cpp -ffixed-line-length-none
LDOPTS = -O3 -cpp -ffixed-line-length-none
COPTS  = -O3

# ----------------------------------------------------------------------

MPIRUN = mpirun
MPIRUN = /home/alexander/Main/OpenMPI/bin/mpirun

NPROC = 16
RUNOPTS = -width

TIME = 1000
QUANT = 30
F_TIME = 10000

# ----------------------------------------------------------------------

all: exoplanet mesh

clean:
	rm -f exoplanet makeMesh *.o

# ----------------------------------------------------------------------

run:
	${MPIRUN} -np ${NPROC} exoplanet

exoplanet: ${OBJ_FILES}
	${LD} ${LDOPTS} ${OBJ_FILES} dislin-10.2.a -lGL -lX11 -o $@

makeMesh: makeMesh.f
	${F77} ${FOPTS} $< -o $@

# ----------------------------------------------------------------------

%.o: %.f
	${F77} ${FOPTS} -c $< -o $@

%.o: %.c
	${CC} ${COPTS} -c $< -o $@

# ----------------------------------------------------------------------
