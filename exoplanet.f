! ----------------------------------------------------------------------
! --- The best ever known program --------------------------------------
! ----------------------------------------------------------------------

      implicit none

      include "mpif.h"
#     include "constants.h"
#     include "physicalConstants.h"

      common /localCord/     x,     y,     z
      common /lclCord_1/    dx,    dy,    dz
      common /lclCord_2/   dxh,   dyh,   dzh
      common /lclCord_3/  dxhi,  dyhi,  dzhi
                                                   
      common /globCoord/ xGlob, yGlob, zGlob

      common /chiefData/ dens, velx, vely, velz,
     &                   pres, enth, srho, wave

      common /dirtyData/ dens_1, velx_1, vely_1, velz_1, pres_1
      common /dummyData/         velx_2, vely_2, velz_2, ener_2

      common /xCalcFlow/ xFlow, xFlowMins, xFlowPlus
      common /yCalcFlow/ yFlow, yFlowMins, yFlowPlus
      common /zCalcFlow/ zFlow, zFlowMins, zFlowPlus

      common /rocheData/ xroc_1, yroc_1, zroc_1,
     &                   xroc_2, yroc_2, zroc_2

      common /sondSpeed/ soundVel

      common /specAreas/ plntZone, starZone

      common /procStuff/ myRank,
     &                      xRank, yRank,
     &                      xOfset,yOfset,
     &                   xLeftBlock,xRghtBlock,
     &                   yLeftBlock,yRghtBlock,
     &                      ialgn,timeStep,courantCoef,
     &                   imin1,jmin1,imax1,jmax1,
     &                   imin2,imax2,jmin2,jmax2,
     &                      imax,jmax,kmax,
     &                   ialgn2,errorFlag

      common /physParam/ prop, gravMass, plntOrbit, omega, innrRadius

      common /densMinim/ densMin, iteration

      common /windParam/ windDensAmbient, windVelsAmbient, windTempAmbient

      common /xyBorders/ xMinBorder, xMaxBorder,
     &                   yMinBorder, yMaxBorder

      common /plntParam/ plntRadius, lagrangePoint_1,
     &                               lagrangePoint_2,
     &                               lagrangePoint_3

      common /someCount/ fileNumber

      common /timeParam/ time, period

! ----------------------------------------------------------------------

      real(8) :: initWindDens
      real(8) :: initWindVelx
      real(8) :: initWindVely
      real(8) :: initWindVelz

      real(8) :: calcEnthalpy

      integer(4) :: calcPlntZone
      integer(4) :: calcStarZone
      integer(4) :: calcExtendStarZone

! ----------------------------------------------------------------------

      real(8) ::     x(X_BLOCK),     y(Y_BLOCK),     z(Z_BLOCK)
      real(8) ::    dx(X_BLOCK),    dy(Y_BLOCK),    dz(Z_BLOCK)
      real(8) ::   dxh(X_BLOCK),   dyh(Y_BLOCK),   dzh(Z_BLOCK)
      real(8) ::  dxhi(X_BLOCK),  dyhi(Y_BLOCK),  dzhi(Z_BLOCK)

      real(8) :: xGlob(X_TOTAL), yGlob(Y_TOTAL), zGlob(Z_TOTAL)

      real(8) :: dens(X_BLOCK,Y_BLOCK,Z_BLOCK)
      real(8) :: velx(X_BLOCK,Y_BLOCK,Z_BLOCK)
      real(8) :: vely(X_BLOCK,Y_BLOCK,Z_BLOCK)
      real(8) :: velz(X_BLOCK,Y_BLOCK,Z_BLOCK)
      real(8) :: pres(X_BLOCK,Y_BLOCK,Z_BLOCK)
      real(8) :: enth(X_BLOCK,Y_BLOCK,Z_BLOCK)
      real(8) :: srho(X_BLOCK,Y_BLOCK,Z_BLOCK)
      real(8) :: wave(X_BLOCK,Y_BLOCK,Z_BLOCK)

      real(8) :: dens_1(X_BLOCK,Y_BLOCK,Z_BLOCK)
      real(8) :: velx_1(X_BLOCK,Y_BLOCK,Z_BLOCK)
      real(8) :: vely_1(X_BLOCK,Y_BLOCK,Z_BLOCK)
      real(8) :: velz_1(X_BLOCK,Y_BLOCK,Z_BLOCK)
      real(8) :: pres_1(X_BLOCK,Y_BLOCK,Z_BLOCK)

      real(8) :: velx_2(X_BLOCK,Y_BLOCK,Z_BLOCK)
      real(8) :: vely_2(X_BLOCK,Y_BLOCK,Z_BLOCK)
      real(8) :: velz_2(X_BLOCK,Y_BLOCK,Z_BLOCK)
      real(8) :: ener_2(X_BLOCK,Y_BLOCK,Z_BLOCK)

      real(8) :: xFlow(X_BLOCK,5)
      real(8) :: xFlowMins(X_BLOCK,5,5)
      real(8) :: xFlowPlus(X_BLOCK,5,5)

      real(8) :: yFlow(Y_BLOCK,5)
      real(8) :: yFlowMins(Y_BLOCK,5,5)
      real(8) :: yFlowPlus(Y_BLOCK,5,5)

      real(8) :: zFlow(Z_BLOCK,5)
      real(8) :: zFlowMins(Z_BLOCK,5,5)
      real(8) :: zFlowPlus(Z_BLOCK,5,5)

      real(8) :: xroc_1(X_BLOCK,Y_BLOCK,Z_BLOCK)
      real(8) :: yroc_1(X_BLOCK,Y_BLOCK,Z_BLOCK)
      real(8) :: zroc_1(X_BLOCK,Y_BLOCK,Z_BLOCK)
      real(8) :: xroc_2(X_BLOCK,Y_BLOCK,Z_BLOCK)
      real(8) :: yroc_2(X_BLOCK,Y_BLOCK,Z_BLOCK)
      real(8) :: zroc_2(X_BLOCK,Y_BLOCK,Z_BLOCK)

      real(8) :: soundVel(X_BLOCK,Y_BLOCK,Z_BLOCK)

      integer(4) :: plntZone(X_BLOCK,Y_BLOCK,Z_BLOCK)
      integer(4) :: starZone(X_BLOCK,Y_BLOCK,Z_BLOCK)

#ifdef BLOCKED
      real(8) :: globalFlow(X_BLOCK,Y_BLOCK,Z_BLOCK,3)
      real(4) :: globalFlowShrink(X_BLOCK,Y_BLOCK,Z_BLOCK,3)
#endif

! ----------------------------------------------------------------------

      real(8) :: errorFlag !!! delete, deprecated

      integer(4) :: needNumberProc
      integer(4) :: recvNumberProc

      integer(4) :: myRank
      integer(4) :: xRank, yRank
      integer(4) :: xOfset, yOfset

      integer(4) :: xTotalCheck
      integer(4) :: yTotalCheck
      integer(4) :: zTotalCheck

      real(8) :: xSkip, ySkip, zSkip

      integer(4) :: xLeftBlock, xRghtBlock
      integer(4) :: yLeftBlock, yRghtBlock

      integer(4) :: fileNumber, readAttempt

      integer(4) :: iteration, maxIteration, writeFreq

      real(8) :: xLeft 
      real(8) :: xRght 
      real(8) :: xCenter 
      real(8) :: valueCenter 

      real(8) :: lagrangePoint_1
      real(8) :: lagrangePoint_2
      real(8) :: lagrangePoint_3

      real(8) :: maxVelocity, maxTemperature

      integer(4) :: ierr

      integer(4) :: imin1, imin2
      integer(4) :: imax1, imax2
      integer(4) :: jmin1, jmin2
      integer(4) :: jmax1, jmax2

      real(8) :: courantCoef
      real(8) :: time, timeStep, timeMax

      real(8) :: timeFirst
      real(8) :: timeSecnd
      real(8) :: timeThird
      real(8) :: timeForth

      real(8) :: starMass 
      real(8) :: plntMass 
      real(8) :: gravMass 

      real(8) :: plntOrbit 

      real(8) :: plntRadius 
      real(8) :: innrRadius
      real(8) :: kernRadius

      real(8) :: omega 
      real(8) :: period 
      real(8) :: prop 
      real(8) :: massRatio

      real(8) :: xMinBorder 
      real(8) :: xMaxBorder 
      real(8) :: yMinBorder 
      real(8) :: yMaxBorder 

      real(8) :: y0       !!! delete, unused
      integer(4) :: j0    !!! delete, unused

      real(8) :: atmsTemp 
      real(8) :: atmsDens
      real(8) :: kernDens
      real(8) :: atmCoef

      real(8) :: windDensAmbient 
      real(8) :: windVelsAmbient 
      real(8) :: windTempAmbient 

      real(8) :: windDensFirst 
      real(8) :: windVelsFirst 
      real(8) :: windTempFirst 

      real(8) :: windDensSecnd
      real(8) :: windVelsSecnd
      real(8) :: windTempSecnd

      real(8) :: windDensThird
      real(8) :: windVelsThird
      real(8) :: windTempThird

      real(8) :: alamder  !!! delete, unused
      real(8) :: gfunder  !!! delete, unused

      real(8) :: xRocheShort
      real(8) :: yRocheShort
      real(8) :: zRocheShort
      real(8) :: xRoche
      real(8) :: yRoche
      real(8) :: zRoche

      real(8) :: scnds
      real(8) :: begTimeMeasure 
      real(8) :: endTimeMeasure 

      real(8) :: xRocheForce 
      real(8) :: yRocheForce 
      real(8) :: zRocheForce 

      real(8) :: densVelx_1 
      real(8) :: densVely_1 
      real(8) :: densVelz_1 

      real(8) :: localMass
      real(8) :: totalMass

      real(8) :: plntDist 
      real(8) :: absVelocity 
      real(8) :: velProject 
      real(8) :: enerDeriv 
      real(8) :: localEner 
      real(8) :: localTemp
      real(8) :: presTmp 
      real(8) :: densTmp 

      real(8) :: densMinCut
      real(8) :: densMin 

      real(8) :: relAccuracy
      real(8) :: relDifference

      real(8) :: starDist
      real(8) :: startPerturbDist

      real(8) :: perturbDistFirst
      real(8) :: perturbDistSecnd
      real(8) :: perturbDistThird
      real(8) :: perturbDistForth


      integer(4) :: ialgn, ialgn2 !!! delete, deprecated

      integer(4) :: imax, jmax, kmax

      integer(4) :: i, j, k

      character(len=21) :: fileName
      character(len=25) :: fileNameShrink
      character(len=23) :: flowFileName

      integer(4) :: flowWriteFreq = 4

      integer(4) :: processor

      integer(4) :: istats(MPI_STATUS_SIZE)

! ----------------------------------------------------------------------

110   format ('./Data/data_',i5.5,'.',i3.3) !!! filename : ./Data/data_ -> data_
#ifdef BLOCKED
115   format ('./Flows/flows_',i5.5,'.',i3.3)
#endif

      errorFlag = 0.0d0 !!! delete, deprecate errorFlag

      needNumberProc = X_PROC*Y_PROC

      call MPI_INIT( ierr )
      call MPI_COMM_RANK( MPI_COMM_WORLD, myRank, ierr )
      call MPI_COMM_SIZE( MPI_COMM_WORLD, recvNumberProc, ierr )

      if         (myRank.ge.needNumberProc) goto 210
      if (recvNumberProc.lt.needNumberProc) goto 200

      xRank = mod (myRank,X_PROC)
      yRank = int (myRank/X_PROC)
               
      xLeftBlock = yRank*X_PROC + xRank - 1
      xRghtBlock = yRank*X_PROC + xRank + 1
               
      yLeftBlock = (yRank-1)*X_PROC + xRank
      yRghtBlock = (yRank+1)*X_PROC + xRank
               
      xOfset = xRank*(X_BLOCK-4)
      yOfset = yRank*(Y_BLOCK-4)
      
      imin1 = 1
      imax1 = X_BLOCK
              
      jmin1 = 1
      jmax1 = Y_BLOCK

      if (xRank.gt.0)        imin1 = 2
      if (yRank.gt.0)        jmin1 = 2
      if (xRank.lt.X_PROC-1) imax1 = X_BLOCK-1
      if (yRank.lt.Y_PROC-1) jmax1 = Y_BLOCK-1

      imin2 = 1
      imax2 = X_BLOCK

      jmin2 = 1
      jmax2 = Y_BLOCK

      if (xRank.gt.0)        imin2 = 3
      if (yRank.gt.0)        jmin2 = 3
      if (xRank.lt.X_PROC-1) imax2 = X_BLOCK-2
      if (yRank.lt.Y_PROC-1) jmax2 = Y_BLOCK-2

! ----------------------------------------------------------------------
      
      open (1,file='parameters.dat')
         read (1,*) maxIteration
         read (1,*) writeFreq
         read (1,*) courantCoef
         read (1,*) fileNumber
      close (1)

! ----------------------------------------------------------------------

      starMass = 1.1*SUN_MASS
      plntMass = 0.0006*SUN_MASS

      massRatio = starMass/plntMass

      gravMass = GRAV_CONST*(starMass + plntMass)

      plntOrbit = 10.1*SUN_RADIUS

      omega = dsqrt(gravMass/plntOrbit**3)
      period = 2.0d0*PI_CONST/omega

      prop = massRatio/(massRatio+1.0d0)

! ----------------------------------------------------------------------

      write (*,*) 'q=', massRatio          !!! rewrite string ? delete
      write (*,*) 'period=', (period/60.0) !!! rewrite string ? delete
      write (*,*) 'period=', (period) !!! rewrite string ? delete

      plntRadius = 0.135*SUN_RADIUS

      innrRadius = 0.75*plntRadius
      kernRadius = 0.75*innrRadius

! ----------------------------------------------------------------------


      open (1,file="./Data/x.dat")
      read (1,*) xTotalCheck
      if (xTotalCheck.ne.X_TOTAL) print*, "Something is really wrong!"
      do i=1,X_TOTAL
            read (1,*) xGlob(i), xSkip
      enddo
      close (1)
      open (1,file="./Data/y.dat")
      read (1,*) yTotalCheck
      if (yTotalCheck.ne.Y_TOTAL) print*, "Something is really wrong!"
      do i=1,Y_TOTAL
            read (1,*) yGlob(i), ySkip
      enddo
      close (1)
      open (1,file="./Data/z.dat")
      read (1,*) zTotalCheck
      if (zTotalCheck.ne.Z_TOTAL) print*, "Something is really wrong!"
      do i=1,Z_TOTAL
            read (1,*) zGlob(i), zSkip
      enddo
      close (1)


! ----------------------------------------------------------------------

#ifdef NOT_DEFINED
      open (1,file="./Data/x.dat")
         read (1,*) (xGlob(i), i=1,X_TOTAL)
      close (1)
      open (1,file="./Data/y.dat")
         read (1,*) (yGlob(i), i=1,Y_TOTAL)
      close (1)
      open (1,file="./Data/z.dat")
         read (1,*) (zGlob(i), i=1,Z_TOTAL)
      close (1)
#endif

! ----------------------------------------------------------------------

      do i=1,X_TOTAL
         if (i.gt.xOfset.and.
     &       i.le.(xOfset+X_BLOCK)) then
            x(i-xOfset) = xGlob(i)
         endif
      enddo

      do i=1,Y_TOTAL
         if (i.gt.yOfset.and.
     &       i.le.(yOfset+Y_BLOCK)) then
            y(i-yOfset) = yGlob(i)
         endif
      enddo
      
      xMinBorder = xGlob(1)
      xMaxBorder = xGlob(X_TOTAL)
      yMinBorder = yGlob(1)
      yMaxBorder = yGlob(Y_TOTAL)
      
      if (myRank.eq.0) then
         write(*,*) 'BBB: ', xMinBorder/plntOrbit,
     &                       xMaxBorder/plntOrbit,
     &                       yMinBorder/plntOrbit, 
     &                       yMaxBorder/plntOrbit
      end if

      do i=1,Z_BLOCK
         z(i) = zGlob(i)
      enddo

      do i=2,X_BLOCK-1
         dx(i) = 0.5d0*(x(i+1)-x(i-1))
      enddo
      do i=2,Y_BLOCK-1
         dy(i) = 0.5d0*(y(i+1)-y(i-1))
      enddo
      do i=2,Z_BLOCK-1
         dz(i) = 0.5d0*(z(i+1)-z(i-1))
      enddo

      do i=1,X_BLOCK-1
         dxh(i) = x(i+1)-x(i)
         dxhi(i) = 1.0d0/dxh(i)
      enddo
      do i=1,Y_BLOCK-1
         dyh(i) = y(i+1)-y(i)
         dyhi(i) = 1.0d0/dyh(i)
      enddo
      do i=1,Z_BLOCK-1
         dzh(i) = z(i+1)-z(i)
         dzhi(i) = 1.0d0/dzh(i)
      enddo

! ----------------------------------------------------------------------
      
      atmsTemp = 7500.0
      atmsDens = 10.0d10*HYGE_MASS
      
      atmCoef = (GRAV_CONST_CLONE*plntMass/(GAS_CONST*atmsTemp))

      kernDens = atmsDens*
     &                 exp(-atmCoef*((1.0/plntRadius)-(1.0/kernRadius)))
      
      windDensAmbient = 1.0d4*HYGE_MASS
      windVelsAmbient = 1.0d7
      windTempAmbient = 7.3d5

      timeMax = 1.0*period
c     timeMax = 0.02*period
      
      densMin = 1.0d-8*windDensAmbient

      alamder = 0.0                   !!! delete, it's deprecated
      gfunder = 0.0                   !!! delete, it's deprecated

! ----------------------------------------------------------------------
      
      relAccuracy = 1.0e-11

      xLeft = 0.0
      xRght = plntOrbit

      do while (.true.)
         xCenter = 0.5*(xLeft+xRght)
         valueCenter = xRoche(xCenter,0.0d0,0.0d0)
         if (valueCenter.eq.0.0) goto 120
         if (valueCenter.gt.0) then
            xLeft = xCenter
         else
            xRght = xCenter
         endif
         relDifference = dabs(xLeft-xRght)/plntOrbit
         if (relDifference.lt.relAccuracy) goto 120
      enddo

120   continue

      lagrangePoint_1 = xCenter

      xLeft = plntOrbit
      xRght = plntOrbit*2.0

      do while (.true.)
         xCenter = 0.5*(xLeft+xRght)
         valueCenter = xRoche(xCenter,0.0d0,0.0d0)
         if (valueCenter.eq.0.0) goto 130
         if (valueCenter.gt.0) then
            xLeft = xCenter
         else
            xRght = xCenter
         end if
         relDifference = dabs(xLeft-xRght)/plntOrbit
         if (relDifference.lt.relAccuracy) goto 130
      enddo

130   continue

      lagrangePoint_2 = xCenter

      xLeft =-plntOrbit
      xRght = 0.0

      do while (.true.)
         xCenter = 0.5*(xLeft+xRght)
         valueCenter = xRoche(xCenter,0.0d0,0.0d0)
         if (valueCenter.eq.0.0) goto 140
         if (valueCenter.gt.0) then
            xLeft = xCenter
         else
            xRght = xCenter
         end if
         relDifference = dabs(xLeft-xRght)/plntOrbit
         if (relDifference.lt.relAccuracy) goto 140
      enddo

140   continue

      lagrangePoint_3 = xCenter

! ----------------------------------------------------------------------

      if (fileNumber.eq.0) then

         time = 0.0

         do i=1,X_BLOCK
         do j=1,Y_BLOCK
         do k=1,Z_BLOCK

#ifdef BLOCKED
            dens(i,j,k) = initWindDens(windDensAmbient,x(i),y(j),z(k))
            pres(i,j,k) = dens(i,j,k)*GAS_CONST*windTempAmbient
            velx(i,j,k) = initWindVelx(windVelsAmbient,x(i),y(j),z(k))
            vely(i,j,k) = initWindVely(windVelsAmbient,x(i),y(j),z(k))
            velz(i,j,k) = initWindVelz(windVelsAmbient,x(i),y(j),z(k))
            
            plntDist = sqrt((x(i)-plntOrbit)**2+y(j)**2+z(k)**2)
            
            if (plntDist.ge.kernRadius) then
               atmCoef = (GRAV_CONST_CLONE*plntMass/(GAS_CONST*atmsTemp)) !!! delete, it will be unused
               densTmp = atmsDens*exp(-atmCoef*
     &                        ((1.0/plntRadius)-(1.0/plntDist)))
            else
               densTmp = kernDens
            endif
            
            velProject = (velx(i,j,k)*(x(i)-plntOrbit)
     &                  + vely(i,j,k)*y(j)
     &                  + velz(i,j,k)*z(k))/
     &                 sqrt((x(i)-plntOrbit)**2+y(j)**2+z(k)**2)
          
            if (densTmp*GAS_CONST*atmsTemp.ge.
     &          dens(i,j,k)*(GAS_CONST*windTempAmbient+0.5*min(velProject,0.0)**2)) then

               dens(i,j,k) = densTmp
               pres(i,j,k) = densTmp*GAS_CONST*atmsTemp
               velx(i,j,k) = 0.0
               vely(i,j,k) = 0.0
               velz(i,j,k) = 0.0

            endif
#endif

            plntDist = sqrt((x(i)-plntOrbit)**2+y(j)**2+z(k)**2)
            
            if (plntDist.ge.kernRadius) then
               atmCoef = (GRAV_CONST_CLONE*plntMass/(GAS_CONST*atmsTemp)) !!! delete, it will be unused
               densTmp = atmsDens*exp(-atmCoef*
     &                        ((1.0/plntRadius)-(1.0/plntDist)))
            else
               densTmp = kernDens
            endif
            
            dens(i,j,k) = densTmp
            pres(i,j,k) = densTmp*GAS_CONST*atmsTemp
            velx(i,j,k) = 0.0
            vely(i,j,k) = 0.0
            velz(i,j,k) = 0.0


         enddo
         enddo
         enddo
                  
      else

         readAttempt = 0

150      write (fileName,110)  fileNumber, myRank
         write (*,*) 'reading of file ', fileName
         open (1, file=fileName, form='unformatted')
         do j=1,Y_BLOCK
            read (1,END=160) ((dens(i,j,k),i=1,X_BLOCK),k=1,Z_BLOCK)
            read (1,END=160) ((velx(i,j,k),i=1,X_BLOCK),k=1,Z_BLOCK)
            read (1,END=160) ((vely(i,j,k),i=1,X_BLOCK),k=1,Z_BLOCK)
            read (1,END=160) ((velz(i,j,k),i=1,X_BLOCK),k=1,Z_BLOCK)
            read (1,END=160) ((pres(i,j,k),i=1,X_BLOCK),k=1,Z_BLOCK)
         enddo
         read (1,END=160) time
         goto 170

160      if (readAttempt.lt.2) then
            readAttempt = readAttempt + 1 !!! move line: -1
            close (1)
            fileNumber = fileNumber - 1
            goto 150
         else
            stop
         endif

170      continue

      endif

! ----------------------------------------------------------------------

      call MPI_BARRIER(MPI_COMM_WORLD,ierr)

      if (myRank.eq.0) then

         do processor=1,(X_PROC*Y_PROC-1)

            call MPI_SEND(time,1,MPI_DOUBLE_PRECISION,
     &                 processor,950,MPI_COMM_WORLD,ierr)
         enddo

      else

         call MPI_RECV(time,1,MPI_DOUBLE_PRECISION,
     &                 0,950,MPI_COMM_WORLD,istats,ierr)

      endif

      call MPI_BARRIER(MPI_COMM_WORLD,ierr)

! ----------------------------------------------------------------------
      
      do i=1,X_BLOCK
      do j=1,Y_BLOCK
      do k=1,Z_BLOCK
         enth(i,j,k) = calcEnthalpy(i,j,k)
      enddo
      enddo
      enddo

      do i=1,X_BLOCK
      do j=1,Y_BLOCK
      do k=1,Z_BLOCK
         plntZone(i,j,k) = calcPlntZone(i,j,k)
         starZone(i,j,k) = calcStarZone(i,j,k)
      enddo
      enddo
      enddo

      do i=1,X_BLOCK
      do j=1,Y_BLOCK
      do k=1,Z_BLOCK

         if (plntZone(i,j,k).eq.0) then

            xroc_1 (i,j,k) = xRocheShort (x(i),y(j),z(k))
            yroc_1 (i,j,k) = yRocheShort (x(i),y(j),z(k))
            zroc_1 (i,j,k) = zRocheShort (x(i),y(j),z(k))
                                     
            xroc_2 (i,j,k) = xRoche (x(i),y(j),z(k))
            yroc_2 (i,j,k) = yRoche (x(i),y(j),z(k))
            zroc_2 (i,j,k) = zRoche (x(i),y(j),z(k))

         endif

      enddo
      enddo
      enddo

      call calcTimeStep (dens,velx,vely,velz,pres)

#ifdef BLOCKED
      call calcWaveFront
#endif

      call drawPlot

      iteration = 0

      call saveLine

c ----------------------------------------------------------------------
c ----------------------------------------------------------------------
c ----------------------------------------------------------------------

c     if (myRank.eq.9) 
c    &   open (10,file="dumpLeft.log",form='unformatted')

c     if (myRank.eq.10) 
c    &   open (10,file="dumpRght.log",form='unformatted')

c     k = (Z_BLOCK/2)
c     j = 3
c     do i = 5, 22
c     if (myRank.eq.9) then
c        write(*,*) "Dens check left secnd", 0, i, dens(24 - i,j,k)
c        write(10) i, dens(24-i,j,k)
c     endif
c     if (myRank.eq.10) then
c        write(*,*) "Dens check rght secnd", 0, i, dens(i + 1,j,k)
c        write(10) i, dens(i+1,j,k)
c     endif
c     enddo

c     if (myRank.eq.9 ) close(10)

c     if (myRank.eq.10) close(10)

c ----------------------------------------------------------------------
c ----------------------------------------------------------------------
c ----------------------------------------------------------------------
      
      do iteration=1,maxIteration
          
         do i=1,X_BLOCK
         do j=1,Y_BLOCK
         do k=1,Z_BLOCK

            starDist = sqrt(x(i)**2+y(j)**2+z(k)**2)
            plntDist = sqrt((x(i)-plntOrbit)**2+y(j)**2+z(k)**2)

            if (
     &         (((i.eq.1)      .or.(i.eq.2))          .and.(xRank.eq.0)       ).or.
     &         (((i.eq.X_BLOCK).or.(i.eq.(X_BLOCK-1))).and.(xRank.eq.X_PROC-1)).or.
     &         (((j.eq.1)      .or.(j.eq.2))          .and.(yRank.eq.0)       ).or.
     &         (((j.eq.Y_BLOCK).or.(j.eq.(Y_BLOCK-1))).and.(yRank.eq.Y_PROC-1)).or.
     &         (k.eq.1).or.
     &         (k.eq.2).or.
     &         (k.eq.Z_BLOCK).or.
     &         (k.eq.Z_BLOCK-1)) then

               densTmp = atmsDens*exp(-atmCoef*
     &                        ((1.0/plntRadius)-(1.0/plntDist)))
           
               dens(i,j,k) = densTmp
               pres(i,j,k) = densTmp*GAS_CONST*atmsTemp
               velx(i,j,k) = 0.0
               vely(i,j,k) = 0.0
               velz(i,j,k) = 0.0

            endif

         enddo
         enddo
         enddo

         call checkSymmetry


         begTimeMeasure = scnds() !!! deprecated

180      format (i8.8,' time =',e10.3,' time step =',e10.3)

         if (mod(iteration,OUTPUT_FREQ).eq.0) then

            if (myRank.eq.0.and.mod(iteration,OUTPUT_FREQ).eq.0) then !!! delete one condition
               write (*,180) iteration, (time/period), (timeStep/period)
            endif

         endif

         call calcSqrtDens
            
190      if (iteration.ne.1.or.fileNumber.ne.1) time = time + timeStep

         do j=jmin1+1,jmax1-1
         do k=2,Z_BLOCK-1

            call xFlowCalculation(j,k,myRank,iteration)
            call xFlowCorrection(j,k)

c           if (iteration.eq.1) then
c              if (k.eq.(Z_BLOCK/2)) then
c              if (j.eq.3) then
c                 if (myRank.eq.9) then
c                       open (10,file="dumpLeft.log",form='unformatted')
c                       write(10) xFlow(14,2)
c                       close(10)
c                    write(*,*) "Left  ", iteration, xFlow(14,2), dens_l, velx_l, dens_r, velx_r, pres_l, pres_r
c                    write(*,*) "Left in program before", iteration, velx(15,j,k), xFlow(15,2),xFlow(14,2)
c                 endif
c                 if (myRank.eq.10) then
c                       open (10,file="dumpRght.log",form='unformatted')
c                       write(10) xFlow(10,2)
c                       close(10)
c                    write(*,*) "Rght in program before", iteration, velx(10,j,k), xFlow(10,2),xFlow(9,2)
c                 endif
c              endif
c              endif
c            endif

c           if (myRank.eq.9) then
c           if (k.eq.(Z_BLOCK/2)) then
c           if (j.eq.3) then
c              write(*,*) 'Flow left ', iteration, xFlow(14,1)
c           endif
c           endif
c           endif
c           if (myRank.eq.10) then
c           if (k.eq.(Z_BLOCK/2)) then
c           if (j.eq.3) then
c              write(*,*) 'Flow right', iteration, xFlow(10,1)
c           endif
c           endif
c           endif


            do i=imin1+1,imax1-1
               dens_1(i,j,k) = dens(i,j,k)
     &                       - timeStep/dx(i)*(xFlow(i,1)-xFlow(i-1,1))
               velx_2(i,j,k) = dens(i,j,k)*velx(i,j,k)
     &                       - timeStep/dx(i)*(xFlow(i,2)-xFlow(i-1,2))
               vely_2(i,j,k) = dens(i,j,k)*vely(i,j,k)
     &                       - timeStep/dx(i)*(xFlow(i,3)-xFlow(i-1,3))
               velz_2(i,j,k) = dens(i,j,k)*velz(i,j,k)
     &                       - timeStep/dx(i)*(xFlow(i,4)-xFlow(i-1,4))
               ener_2(i,j,k) = pres(i,j,k)/(GAMMA_CONST-1)
     &                       + dens(i,j,k)*(velx(i,j,k)**2
     &                                    + vely(i,j,k)**2
     &                                    + velz(i,j,k)**2)/2.0d0
               ener_2(i,j,k) = ener_2(i,j,k)
     &                       - timeStep/dx(i)*(xFlow(i,5)-xFlow(i-1,5))
#ifdef BLOCKED
               if ((mod(iteration,writeFreq).eq.0).and.
     &             (mod(fileNumber,flowWriteFreq).eq.0)) then
                  globalFlow(i,j,k,1) = xFlow(i,1)
               endif
#endif
            enddo
c              if (k.eq.(Z_BLOCK/2)) then
c              if (j.eq.3) then
c                 if (myRank.eq.9) then
c                    write(*,*) "Left in program", iteration, xFlow(12,1),xFlow(11,1)
c                 endif
c                 if (myRank.eq.10) then
c                    write(*,*) "Rght in program", iteration, xFlow(12,1),xFlow(13,1)
c                 endif
c              endif
c              endif
         enddo
         enddo

c       k = (Z_BLOCK/2)
c       j = 3
c       if (myRank.eq.9) then
c          write(*,*) "dens check after", iteration, dens_1(12,j,k)
c       endif
c       if (myRank.eq.10) then
c          write(*,*) "dens check after", iteration, dens_1(13,j,k)
c       endif

cifdef BLOCKED

         do i=imin1+1,imax1-1
         do k=2,Z_BLOCK-1

            call yFlowCalculation(i,k)
            call yFlowCorrection(i,k)

            do j=jmin1+1,jmax1-1
               dens_1(i,j,k) = dens_1(i,j,k)
     &                       - timeStep/dy(j)*(yFlow(j,1)-yFlow(j-1,1))
               velx_2(i,j,k) = velx_2(i,j,k)
     &                       - timeStep/dy(j)*(yFlow(j,2)-yFlow(j-1,2))
               vely_2(i,j,k) = vely_2(i,j,k)
     &                       - timeStep/dy(j)*(yFlow(j,3)-yFlow(j-1,3))
               velz_2(i,j,k) = velz_2(i,j,k)
     &                       - timeStep/dy(j)*(yFlow(j,4)-yFlow(j-1,4))
               ener_2(i,j,k) = ener_2(i,j,k)
     &                       - timeStep/dy(j)*(yFlow(j,5)-yFlow(j-1,5))
#ifdef BLOCKED
               if ((mod(iteration,writeFreq).eq.0).and.
     &             (mod(fileNumber,flowWriteFreq).eq.0)) then
                  globalFlow(i,j,k,2) = yFlow(j,1)
               endif
#endif
            enddo
         enddo
         enddo

c#endif

c#ifdef BLOCKED

         errorFlag = 0.d0

         do i=imin1+1,imax1-1
         do j=jmin1+1,jmax1-1

            call zFlowCalculation(i,j)
            call zFlowCorrection(i,j)

            do k=2,Z_BLOCK-1
               dens_1(i,j,k) = dens_1(i,j,k)
     &                       - timeStep/dz(k)*(zFlow(k,1)-zFlow(k-1,1))
               if (dens_1(i,j,k).lt.0.0) then
                  dens_1(i,j,k) = dens(i,j,k)
               endif
               velx_2(i,j,k) = velx_2(i,j,k)
     &                       - timeStep/dz(k)*(zFlow(k,2)-zFlow(k-1,2))
               vely_2(i,j,k) = vely_2(i,j,k)
     &                       - timeStep/dz(k)*(zFlow(k,3)-zFlow(k-1,3))
               velz_2(i,j,k) = velz_2(i,j,k)
     &                       - timeStep/dz(k)*(zFlow(k,4)-zFlow(k-1,4))
               ener_2(i,j,k) = ener_2(i,j,k)
     &                       - timeStep/dz(k)*(zFlow(k,5)-zFlow(k-1,5))
#ifdef BLOCKED
               if ((mod(iteration,writeFreq).eq.0).and.
     &             (mod(fileNumber,flowWriteFreq).eq.0)) then
                  globalFlow(i,j,k,3) = zFlow(k,1)
               endif
#endif
            enddo
         enddo
         enddo
c#endif

c        k = (Z_BLOCK/2)
c        j = 3
c        if (myRank.eq.9) then
c           write(*,*) "Variable check left zero dens", iteration, dens_1(15,j,k)
c           write(*,*) "Variable check left zero velx", iteration, velx_2(15,j,k)
c           write(*,*) "Variable check left zero vely", iteration, vely_2(15,j,k)
c           write(*,*) "Variable check left zero velz", iteration, velz_2(15,j,k)
c           write(*,*) "Variable check left zero ener", iteration, ener_2(15,j,k)
c        endif
c        if (myRank.eq.10) then
c           write(*,*) "Variable check rght zero dens", iteration, dens_1(10,j,k)
c           write(*,*) "Variable check rght zero velx", iteration, velx_2(10,j,k)
c           write(*,*) "Variable check rght zero vely", iteration, vely_2(10,j,k)
c           write(*,*) "Variable check rght zero velz", iteration, velz_2(10,j,k)
c           write(*,*) "Variable check rght zero ener", iteration, ener_2(10,j,k)
c        endif


         do i=imin1+1,imax1-1
         do j=jmin1+1,jmax1-1
         do k=2,Z_BLOCK-1

            if (plntZone(i,j,k).eq.1) then
            else
#ifdef BLOCKED
               if (wave(i,j,k).lt.1.0) then
                  xRocheForce = xroc_1 (i,j,k)
                  yRocheForce = yroc_1 (i,j,k)
                  zRocheForce = zroc_1 (i,j,k)
               else                    
                  xRocheForce = xroc_2 (i,j,k)
                  yRocheForce = yroc_2 (i,j,k)
                  zRocheForce = zroc_2 (i,j,k)
               endif
#endif

               xRocheForce = xroc_2 (i,j,k)
               yRocheForce = yroc_2 (i,j,k)
               zRocheForce = zroc_2 (i,j,k)
                    
               densVelx_1 = velx_2(i,j,k)
     &                    - dens(i,j,k)*xRocheForce*timeStep
c    &                    + 2.0d0*omega*dens(i,j,k)*vely(i,j,k)*timeStep

               velx_1(i,j,k) = densVelx_1/dens_1(i,j,k)

               densVely_1 = vely_2(i,j,k)
     &                    - dens(i,j,k)*yRocheForce*timeStep
c    &                    - 2.0d0*omega*dens(i,j,k)*velx(i,j,k)*timeStep

               vely_1(i,j,k) = densVely_1/dens_1(i,j,k)

               densVelz_1 = velz_2(i,j,k)
     &                    - dens(i,j,k)*zRocheForce*timeStep

               velz_1(i,j,k) = densVelz_1/dens_1(i,j,k)

               maxVelocity = 1.0e9
               absVelocity = dsqrt(velx_1(i,j,k)**2 + 
     &                             vely_1(i,j,k)**2 + 
     &                             velz_1(i,j,k)**2)
               densMinCut = 1e-15


c              if (k.eq.(Z_BLOCK/2)) then
c              if (j.eq.3) then
c                 if (myRank.eq.9) then
c                    if (i.eq.15) then
c                       write(*,*) "Inside procedure left velx  ", iteration,  velx_1(i,j,k)
c                       write(*,*) "Inside procedure left vely  ", iteration,  vely_1(i,j,k)
c                       write(*,*) "Inside procedure left velz  ", iteration,  velz_1(i,j,k)
c                    endif
c                 endif
c                 if (myRank.eq.10) then
c                    if (i.eq.10) then
c                       write(*,*) "Inside procedure rght velx  ", iteration, velx_1(i,j,k)
c                       write(*,*) "Inside procedure rght vely  ", iteration, vely_1(i,j,k)
c                       write(*,*) "Inside procedure rght velz  ", iteration, velz_1(i,j,k)
c                    endif
c                 endif
c              endif
c              endif

               if (dens(i,j,k).lt.densMinCut.and.
     &             absVelocity.gt.maxVelocity) then
                 velx_1(i,j,k) = (velx_1(i,j,k)/absVelocity)*maxVelocity
                 vely_1(i,j,k) = (vely_1(i,j,k)/absVelocity)*maxVelocity
                 velz_1(i,j,k) = (velz_1(i,j,k)/absVelocity)*maxVelocity
               endif

               enerDeriv = -dens(i,j,k)*
     &                    (xRocheForce*(velx(i,j,k)+velx_1(i,j,k))/2.d0 !!! move 2 to expression begging
     &                   + yRocheForce*(vely(i,j,k)+vely_1(i,j,k))/2.d0 !!! move 2 to expression begging
     &                   + zRocheForce*(velz(i,j,k)+velz_1(i,j,k))/2.d0)!!! move 2 to expression begging


               localEner = ener_2(i,j,k)
     &                   + timeStep*enerDeriv
     &                   - timeStep*                    !!! delete (gfunder-alamder) member
     &                     (gfunder-alamder)*windTempAmbient*  !!! delete (gfunder-alamder) member
     &                     dens_1(i,j,k)**2/HYGE_MASS**2 !!! delete (gfunder-alamder) member


               localEner = localEner
     &                   - dens_1(i,j,k)*(velx_1(i,j,k)**2 + 
     &                                    vely_1(i,j,k)**2 + 
     &                                    velz_1(i,j,k)**2)/2.d0
               localTemp = localEner/
     &                      (dens_1(i,j,k)*GAS_CONST/(GAMMA_CONST-1.0d0)
     &                     -timeStep*(gfunder-alamder)*   !!! delete (gfunder-alamder) member
     &                      dens_1(i,j,k)**2/HYGE_MASS**2) !!! delete (gfunder-alamder) member
 
               

               maxTemperature = 1e7

               if (localTemp.gt.maxTemperature) then
                  localTemp = maxTemperature
               endif

               pres_1(i,j,k) = dens_1(i,j,k)*localTemp*GAS_CONST

               if (pres_1(i,j,k).lt.0.0) pres_1(i,j,k) = pres(i,j,k)

            endif

         enddo
         enddo
         enddo

c        k = (Z_BLOCK/2)
c        j = 3
c        if (myRank.eq.9) then
c           write(*,*) "Pressure check left zero", iteration, pres_1(15,j,k), dens_1(15,j,k), localTemp
c        endif
c        if (myRank.eq.10) then
c           write(*,*) "Pressure check rght zero", iteration, pres_1(10,j,k), dens_1(10,j,k), localTemp
c        endif

#ifdef BLOCKED
         do i=imin1+1,imax1-1 !!! delete this empty cycle
         do j=jmin1+1,jmax1-1 !!! delete this empty cycle
         do k=2,Z_BLOCK-1     !!! delete this empty cycle
         enddo                !!! delete this empty cycle
         enddo                !!! delete this empty cycle
         enddo                !!! delete this empty cycle
#endif

         call exchangeBoundCells

         if (errorFlag.eq.1.d0) then  !!! delete, deprecate errorFlag

            time = time - timeStep    !!! delete, deprecate errorFlag
            timeStep = 0.1d0*timeStep !!! delete, deprecate errorFlag
         
            goto 190                  !!! delete, deprecate errorFlag

         endif                        !!! delete, deprecate errorFlag

         localMass = 0.0

         do i=1,X_BLOCK
         do j=1,Y_BLOCK
         do k=1,Z_BLOCK

            if (((i.ge.imin1+1.and.i.le.imax1-1).or.
     &           (j.ge.jmin1+1.and.j.le.jmax1-1)).and.
     &           plntZone(i,j,k).eq.0.and.
     &           starZone(i,j,k).eq.0) then

               dens(i,j,k) = dens_1(i,j,k)
               velx(i,j,k) = velx_1(i,j,k)
               vely(i,j,k) = vely_1(i,j,k)
               velz(i,j,k) = velz_1(i,j,k)
               pres(i,j,k) = pres_1(i,j,k)

               enth(i,j,k) = calcEnthalpy(i,j,k)

               localMass = localMass
     &                   + dens(i,j,k)*dx(i)*dy(j)*dz(k)
            endif

         enddo
         enddo
         enddo

c        k = (Z_BLOCK/2)
c        j = 3
c        if (myRank.eq.9) then
c           write(*,*) "Pressure check left first", iteration, pres(15,j,k)
c        endif
c        if (myRank.eq.10) then
c           write(*,*) "Pressure check rght first", iteration, pres(10,j,k)
c        endif

         
         do i=1,X_BLOCK
         do j=1,Y_BLOCK
         do k=1,Z_BLOCK

            plntDist = sqrt((x(i)-plntOrbit)**2+y(j)**2+z(k)**2)

            if (plntZone(i,j,k).eq.0.and.
     &          starZone(i,j,k).eq.0) then

               if (plntDist.le.1.0*plntRadius.or. !!! really strange moment
     &                dens(i,j,k).ge.atmsDens) then

                   velProject = (velx(i,j,k)*(x(i)-plntOrbit) + 
     &                           vely(i,j,k)*y(j) + 
     &                           velz(i,j,k)*z(k))/plntDist

                   velx(i,j,k) = velx(i,j,k)
     &                         - velProject*(x(i)-plntOrbit)/plntDist
                   vely(i,j,k) = vely(i,j,k)
     &                         - velProject*y(j)/plntDist
                   velz(i,j,k) = velz(i,j,k)
     &                         - velProject*z(k)/plntDist
              
                   localTemp = pres(i,j,k)/(dens(i,j,k)*GAS_CONST)
                   
                   if (localTemp.gt.atmsTemp) then
                      pres(i,j,k) = dens(i,j,k)*GAS_CONST*atmsTemp
                   endif
              
                   velx(i,j,k) = velx(i,j,k)*0.001
                   vely(i,j,k) = vely(i,j,k)*0.001
                   velz(i,j,k) = velz(i,j,k)*0.001
              
               endif
               
               if (plntDist.le.plntRadius) then

                  if (plntDist.ge.kernRadius) then
                     atmCoef = (GRAV_CONST_CLONE*plntMass/(GAS_CONST*atmsTemp)) !!! delete in the future
                     densTmp = atmsDens* exp(-atmCoef*
     &                             ((1.0/plntRadius)-(1.0/plntDist)))
                  else
                     densTmp = kernDens
                  endif

                  presTmp = densTmp*GAS_CONST*atmsTemp

                  dens(i,j,k) = 0.5*(dens(i,j,k)+densTmp)
                  pres(i,j,k) = 0.5*(pres(i,j,k)+presTmp)
                  velx(i,j,k) = 0.5*velx(i,j,k)
                  vely(i,j,k) = 0.5*vely(i,j,k)
                  velz(i,j,k) = 0.5*velz(i,j,k)

               endif
            endif
             
            if (pres(i,j,k).le.0) then
               pres(i,j,k) = dens(i,j,k)*GAS_CONST*atmsTemp
            endif
             
            if (isnan(dens(i,j,k)).or.       !!! totally rewrite this block
     &                dens(i,j,k).le.0.0.or. !!! totally rewrite this block
     &          isnan(pres(i,j,k)).or.       !!! totally rewrite this block
     &                pres(i,j,k).le.0.0.or. !!! totally rewrite this block
     &          isnan(velx(i,j,k)).or.       !!! totally rewrite this block
     &          isnan(vely(i,j,k)).or.       !!! totally rewrite this block
     &          isnan(velz(i,j,k))) then     !!! totally rewrite this block
               write(*,*) 'NAN: ',           !!! totally rewrite this block
     &                       i+xOfset,       !!! totally rewrite this block
     &                       j+yOfset,       !!! totally rewrite this block
     &                       k,              !!! totally rewrite this block
     &                          dens(i,j,k), !!! totally rewrite this block
     &                          pres(i,j,k), !!! totally rewrite this block
     &                          velx(i,j,k), !!! totally rewrite this block
     &                          vely(i,j,k), !!! totally rewrite this block
     &                          velz(i,j,k)  !!! totally rewrite this block
               stop                          !!! totally rewrite this block
            endif                            !!! totally rewrite this block

         enddo
         enddo
         enddo

            if (iteration.eq.1) then
               k = (Z_BLOCK/2)
               j = 3
                  if (myRank.eq.9) then
c                       open (10,file="dumpLeft.log",form='unformatted')
c                       write(10) xFlow(14,2)
c                       close(10)
c                    write(*,*) "Left  ", iteration, xFlow(14,2), dens_l, velx_l, dens_r, velx_r, pres_l, pres_r
                     write(*,*) "Left in program 03", iteration, velx(15,j,k)
                  endif
                  if (myRank.eq.10) then
c                       open (10,file="dumpRght.log",form='unformatted')
c                       write(10) xFlow(10,2)
c                       close(10)
                     write(*,*) "Rght in program 03", iteration, velx(10,j,k)
                  endif
             endif

      if (myRank.eq.9) 
     &   open (10,file="dumpLeft.tmp.log",
     &          form='unformatted',access="append")

      if (myRank.eq.10) 
     &   open (10,file="dumpRght.tmp.log",
     &          form='unformatted',access="append")

c       k = (Z_BLOCK/2)
c       j = 3
c       if (myRank.eq.9) then
c          write(*,*) "Dens check left secnd", iteration, dens(12,j,k)
c          write(10) iteration, dens(12,j,k)
c       endif
c       if (myRank.eq.10) then
c          write(*,*) "Dens check rght secnd", iteration, dens(13,j,k)
c          write(10) iteration, dens(13,j,k)
c       endif

      if (myRank.eq.9)  close(10)

      if (myRank.eq.10) close(10) 

#ifdef BLOCKED
         call calcWaveFront
#endif

! ----------------------------------------------------------------------
! ----------------------------------------------------------------------
! ----------------------------------------------------------------------
#ifdef BLOCKED
116   format ('./Data/dataLine_',i5.5,'.',i3.3)

         if (yRank.eq.(Y_PROC/2)) then

            write(fileNameShrink,116) iteration, myRank
c           write (*,*) 'writing to file ', fileNameShrink
            open (1,file=fileNameShrink)
            do i=1,X_BLOCK
               write (1,*) dens(i,3,(Z_BLOCK/2)),
     &                     velx(i,3,(Z_BLOCK/2)),
     &                     vely(i,3,(Z_BLOCK/2)),
     &                     velz(i,3,(Z_BLOCK/2)),
     &                     pres(i,3,(Z_BLOCK/2))
            enddo
            write (1,*) time
            close (1)

         endif
#endif

         if (mod(iteration,writeFreq).eq.0) then
            call saveLine
         endif
! ----------------------------------------------------------------------
! ----------------------------------------------------------------------
! ----------------------------------------------------------------------

         if (mod(iteration,writeFreq).eq.0.or.
     &                           time.ge.timeMax) then

            fileNumber = fileNumber + 1

            call drawPlot

            write (fileName,110)  fileNumber, myRank 
            write (*,*) 'writing to file ', fileName 
            open (1,file=fileName,form='unformatted')
               do j=1,Y_BLOCK
                  write (1) ((dens_1(i,j,k),i=1,X_BLOCK),k=1,Z_BLOCK)
                  write (1) ((velx_1(i,j,k),i=1,X_BLOCK),k=1,Z_BLOCK)
                  write (1) ((vely_1(i,j,k),i=1,X_BLOCK),k=1,Z_BLOCK)
                  write (1) ((velz_1(i,j,k),i=1,X_BLOCK),k=1,Z_BLOCK)
                  write (1) ((pres_1(i,j,k),i=1,X_BLOCK),k=1,Z_BLOCK)
               enddo
            write (1) time
            close (1)

! ----------------------------------------------------------------------

#ifdef BLOCKED
            if ((mod(iteration,writeFreq).eq.0).and.
     &          (mod(fileNumber,flowWriteFreq).eq.0)) then

               write (flowFileName,115)  fileNumber, myRank
               write (*,*) 'writing to file ', flowFileName
             
               globalFlowShrink = globalFlow
             
               open (1,file=flowFileName,form='unformatted')
                  do j=1,Y_BLOCK
                     write (1) ((globalFlowShrink(i,j,k,1),i=1,X_BLOCK),k=1,Z_BLOCK)
                     write (1) ((globalFlowShrink(i,j,k,2),i=1,X_BLOCK),k=1,Z_BLOCK)
                     write (1) ((globalFlowShrink(i,j,k,3),i=1,X_BLOCK),k=1,Z_BLOCK)
                  enddo
               write (1) time
               close (1)

            endif
#endif

! ----------------------------------------------------------------------


            call MPI_Reduce(localMass,totalMass,1,MPI_DOUBLE_PRECISION,
     &                                   MPI_SUM,0,MPI_COMM_WORLD,ierr)

            if (myRank.eq.0) then

               open (1,file='mass.log',POSITION='append') !!! lowercase all
               write (1,*) time/period, totalMass/SUN_MASS
               close (1)
            
c              open (1,file='parameters.dat',STATUS='REPLACE') !!! lowercase
c                 write (1,*) maxIteration
c                 write (1,*) writeFreq
c                 write (1,*) courantCoef
c                 write (1,*) fileNumber
c              close (1)

            endif
                  
            if ((fileNumber-3).gt.0) then !!! save only two ouputs (-4 -> -3)
               if (mod(fileNumber-3,40).ne.0) then
                  write (fileName,110)  fileNumber-3, myRank          !!! save only two ouputs (-4 -> -3)
                  open (1,file=fileName,form='unformatted')
                  close (1,status='delete')
               endif
            endif

         endif

         if (mod(iteration,OUTPUT_FREQ).eq.0) then                    !!! totally rewrite this block

            endTimeMeasure = scnds() !!! deprecate this               !!! totally rewrite this block

            if (myRank.eq.0.and.mod(iteration,OUTPUT_FREQ).eq.0) then !!! totally rewrite this block
               print *,                                               !!! totally rewrite this block
     &            ' processor time of this step is',                  !!! totally rewrite this block
     &            (endTimeMeasure-begTimeMeasure),                    !!! totally rewrite this block
     &            (endTimeMeasure-begTimeMeasure)/                    !!! totally rewrite this block
     &                      (X_BLOCK*Y_BLOCK*Z_BLOCK)                 !!! totally rewrite this block
            endif                                                     !!! totally rewrite this block
                                                                    
         endif                                                        !!! totally rewrite this block

         if (time.ge.timeMax) goto 210

      enddo

      goto 210

200   if (myRank.eq.0) print*, 
     &      'ERROR: number of allocated processors < ', needNumberProc

      call MPI_BARRIER(MPI_COMM_WORLD,ierr)
210   call MPI_FINALIZE(0)

      end program

! ----------------------------------------------------------------------
! --- The best ever known program --------------------------------------
! ----------------------------------------------------------------------
! ----------------------------------------------------------------------
! --- The best ever known program --------------------------------------
! ----------------------------------------------------------------------
! ----------------------------------------------------------------------
! --- The best ever known program --------------------------------------
! ----------------------------------------------------------------------

      subroutine calcTimeStep (argDens,argVelx,argVely,argVelz,argPres)

         implicit none

         include "mpif.h"

! ----------------------------------------------------------------------

         common /localCord/  x,  y,  z
         common /lclCord_1/ dx, dy, dz

         common /sondSpeed/ soundVel
                      
         common /specAreas/ plntZone, starZone

         common /procStuff/ myRank,
     &                         xRank, yRank,
     &                         xOfset,yOfset,
     &                      xLeftBlock,xRghtBlock,
     &                      yLeftBlock,yRghtBlock,
     &                         ialgn,timeStep,courantCoef,
     &                      imin1,jmin1,imax1,jmax1,
     &                      imin2,imax2,jmin2,jmax2,
     &                         imax,jmax,kmax,
     &                      ialgn2,errorFlag

         common /physParam/ prop, gravMass, plntOrbit, omega, innrRadius

         common /densMinim/ densMin, iteration

         common /windParam/ windDensAmbient, windVelsAmbient, windTempAmbient

         common /plntParam/ plntRadius, lagrangePoint_1,
     &                                  lagrangePoint_2,
     &                                  lagrangePoint_3

! ----------------------------------------------------------------------

         real(8) :: initWindDens
         real(8) :: initWindVelx
         real(8) :: initWindVely
         real(8) :: initWindVelz

! ----------------------------------------------------------------------

         real(8) ::  x(X_BLOCK),  y(Y_BLOCK),  z(Z_BLOCK)
         real(8) :: dx(X_BLOCK), dy(Y_BLOCK), dz(Z_BLOCK)

         real(8) :: soundVel(X_BLOCK,Y_BLOCK,Z_BLOCK)

         integer(4) :: plntZone(X_BLOCK,Y_BLOCK,Z_BLOCK)
         integer(4) :: starZone(X_BLOCK,Y_BLOCK,Z_BLOCK)

         integer(4) :: myRank
         integer(4) :: xRank,  yRank
         integer(4) :: xOfset, yOfset
         integer(4) :: xLeftBlock, xRghtBlock
         integer(4) :: yLeftBlock, yRghtBlock

         integer(4) :: imin1, jmin1, imax1, jmax1
         integer(4) :: imin2, jmin2, imax2, jmax2

         integer(4) :: imax, jmax, kmax

         integer(4) :: ialgn, ialgn2

         real(8) :: timeStep, courantCoef

         real(8) :: errorFlag

         real(8) :: prop, gravMass, plntOrbit, omega, innrRadius

         real(8) :: densMin

         real(8) :: windDensAmbient, windVelsAmbient, windTempAmbient

         real(8) :: plntRadius

         real(8) :: lagrangePoint_1
         real(8) :: lagrangePoint_2
         real(8) :: lagrangePoint_3

         integer(4) :: iteration

! ----------------------------------------------------------------------

         real(8) :: argDens(X_BLOCK,Y_BLOCK,Z_BLOCK)
         real(8) :: argVelx(X_BLOCK,Y_BLOCK,Z_BLOCK)
         real(8) :: argVely(X_BLOCK,Y_BLOCK,Z_BLOCK)
         real(8) :: argVelz(X_BLOCK,Y_BLOCK,Z_BLOCK)
         real(8) :: argPres(X_BLOCK,Y_BLOCK,Z_BLOCK)

         real(8) :: timeInverted
         real(8) :: timeInvertedMax

         real(8) ::  localTimeStep
         real(8) :: commonTimeStep

         integer(4) :: i,j,k, ierr

! ----------------------------------------------------------------------

         imax = 1
         jmax = 1
         kmax = 1
       
         do i=imin2,imax2
         do j=jmin2,jmax2
         do k=1,Z_BLOCK

            if (plntZone(i,j,k).eq.0) then

               if (j+yOfset.le.5) then

!                 argVelx(i,j,k) = 0.5*(argVelx(i,j,k)
!    &                            + 0.1*initWindVelx(windVelsAmbient,x(i),y(j),z(k)))
!                 argVely(i,j,k) = 0.5*(argVely(i,j,k)
!    &                            + 0.1*initWindVely(windVelsAmbient,x(i),y(j),z(k)))
!                 argVelz(i,j,k) = 0.5*(argVelz(i,j,k)
!    &                            + 0.1*initWindVelz(windVelsAmbient,x(i),y(j),z(k)))
!                 argDens(i,j,k) = 0.5*(argDens(i,j,k)
!    &                                + initWindDens(windDensAmbient,x(i),y(j),z(k)))
!                 argPres(i,j,k) = argDens(i,j,k)*GAS_CONST*windTempAmbient

               endif

               soundVel(i,j,k) = dsqrt(GAMMA_CONST*argPres(i,j,k)/
     &                                             argDens(i,j,k))

            endif

         enddo
         enddo
         enddo

         timeInvertedMax = 0

         do i=imin1+1,imax1-1
         do j=jmin1+1,jmax1-1
         do k=2,Z_BLOCK-1
            if (plntZone(i,j,k).eq.0) then

               timeInverted = dmax1(
     &              (dabs(argVelx(i,j,k)) + soundVel(i,j,k))/dx(i),
     &              (dabs(argVely(i,j,k)) + soundVel(i,j,k))/dy(j),
     &              (dabs(argVelz(i,j,k)) + soundVel(i,j,k))/dz(k))

               if (timeInverted.gt.timeInvertedMax) then

                  imax = i
                  jmax = j
                  kmax = k

                  timeInvertedMax = timeInverted

               endif
            endif
         enddo
         enddo
         enddo

         localTimeStep = courantCoef/timeInvertedMax
   
         if (errorFlag.gt.0.0d0) localTimeStep = 0 

         call MPI_ALLREDUCE(localTimeStep,
     &                     commonTimeStep,1,MPI_DOUBLE_PRECISION,
     &                               MPI_MIN,MPI_COMM_WORLD,ierr)

         if (commonTimeStep.eq.0.) then

            errorFlag = 1.0d0

         else

            if (mod(iteration,10).eq.0.and.                   !!! consider rewriting
     &              localTimeStep.eq.commonTimeStep) then     !!! consider rewriting
               write(*,*) 'TMIN',                             !!! consider rewriting
     &                  argDens(imax,jmax,kmax)/densMin,      !!! consider rewriting
     &            dsqrt(argVelx(imax,jmax,kmax)**2            !!! consider rewriting
     &                + argVely(imax,jmax,kmax)**2            !!! consider rewriting
     &                + argVelz(imax,jmax,kmax)**2)/          !!! consider rewriting
     &                 soundVel(imax,jmax,kmax),              !!! consider rewriting
     &                        (x(imax)-plntOrbit)/plntRadius, !!! consider rewriting
     &                         y(jmax)/plntRadius,            !!! consider rewriting
     &                         z(kmax)/plntRadius             !!! consider rewriting

            endif

            timeStep = commonTimeStep

         endif

! ----------------------------------------------------------------------

      end subroutine calcTimeStep

! ----------------------------------------------------------------------
! --- The best ever known program --------------------------------------
! ----------------------------------------------------------------------

      function calcPlntZone(i,j,k)

         implicit none

! ----------------------------------------------------------------------
       
         common /localCord/ x, y, z
                      
         common /physParam/ prop, gravMass, plntOrbit, omega, innrRadius

         common /xyBorders/ xMinBorder, xMaxBorder,
     &                      yMinBorder, yMaxBorder

! ----------------------------------------------------------------------

         real(8) :: x(X_BLOCK), y(Y_BLOCK), z(Z_BLOCK)

         real(8) :: prop,gravMass,plntOrbit,omega,innrRadius

         real(8) :: xMinBorder
         real(8) :: xMaxBorder
         real(8) :: yMinBorder
         real(8) :: yMaxBorder

! ----------------------------------------------------------------------

         integer(4) :: calcPlntZone, i, j, k

         real(8) :: plntDist

! ----------------------------------------------------------------------

         calcPlntZone = 0

         plntDist = (x(i)-plntOrbit)**2 + y(j)**2 + z(k)**2 

         if (plntDist.lt.(innrRadius)**2) calcPlntZone = 1

! ----------------------------------------------------------------------

      end function calcPlntZone

! ----------------------------------------------------------------------
! --- The best ever known program --------------------------------------
! ----------------------------------------------------------------------

      function calcStarZone (i,j,k)

         implicit none

! ----------------------------------------------------------------------

         common /localCord/ x, y, z
                      
         common /physParam/ prop, gravMass, plntOrbit, omega, innrRadius

         common /xyBorders/ xMinBorder, xMaxBorder,
     &                      yMinBorder, yMaxBorder

! ----------------------------------------------------------------------

         real(8) :: x(X_BLOCK), y(Y_BLOCK), z(Z_BLOCK)

         real(8) :: prop,gravMass,plntOrbit,omega,innrRadius

         real(8) :: xMinBorder
         real(8) :: xMaxBorder
         real(8) :: yMinBorder
         real(8) :: yMaxBorder

! ----------------------------------------------------------------------

         integer(4) :: calcStarZone, i, j, k

         real(8) :: zoneDist
         real(8) :: starDistXY

! ----------------------------------------------------------------------

         calcStarZone = 0
       
#ifdef BLOCKED
         zoneDist = xMinBorder**2 + yMaxBorder**2
         starDistXY = (x(i)**2+y(j)**2)
         
         if (starDistXY.le.zoneDist) calcStarZone = 1
#endif

! ----------------------------------------------------------------------

      end function calcStarZone

! ----------------------------------------------------------------------
! --- The best ever known program --------------------------------------
! ----------------------------------------------------------------------

      function calcExtendStarZone (i,j,k)

         implicit none

! ----------------------------------------------------------------------

         common /localCord/ x, y, z
                      
         common /physParam/ prop, gravMass, plntOrbit, omega, innrRadius

         common /xyBorders/ xMinBorder, xMaxBorder,
     &                      yMinBorder, yMaxBorder

! ----------------------------------------------------------------------

         real(8) :: x(X_BLOCK), y(Y_BLOCK), z(Z_BLOCK)

         real(8) :: prop,gravMass,plntOrbit,omega,innrRadius

         real(8) :: xMinBorder
         real(8) :: xMaxBorder
         real(8) :: yMinBorder
         real(8) :: yMaxBorder

! ----------------------------------------------------------------------

         integer(4) :: calcExtendStarZone, i, j, k

         real(8) :: zoneDist
         real(8) :: starDistXY

! ----------------------------------------------------------------------

         calcExtendStarZone = 0
       
         zoneDist = 1.1*(xMinBorder**2 + yMaxBorder**2)
         starDistXY = (x(i)**2+y(j)**2)
         
         if (starDistXY.le.zoneDist) calcExtendStarZone = 1

! ----------------------------------------------------------------------

      end function calcExtendStarZone

! ----------------------------------------------------------------------
! --- The best ever known program --------------------------------------
! ----------------------------------------------------------------------

      function scnds ()              !!! delete, it's deprecated

         implicit none               !!! delete, it's deprecated

         real(8) :: scnds, second    !!! delete, it's deprecated
      
         scnds = second()            !!! delete, it's deprecated

      end function scnds             !!! delete, it's deprecated

! ----------------------------------------------------------------------
! --- The best ever known program --------------------------------------
! ----------------------------------------------------------------------

      subroutine xFlowCorrection (j,k)

         implicit none

! ----------------------------------------------------------------------
   
         common /xCalcFlow/ xFlow, xFlowMins, xFlowPlus
                      
         common /specAreas/ plntZone, starZone

! ----------------------------------------------------------------------

         real(8) :: xFlow     (X_BLOCK,5)
         real(8) :: xFlowMins (X_BLOCK,5,5)
         real(8) :: xFlowPlus (X_BLOCK,5,5)

         integer(4) :: plntZone (X_BLOCK,Y_BLOCK,Z_BLOCK)
         integer(4) :: starZone (X_BLOCK,Y_BLOCK,Z_BLOCK)

! ----------------------------------------------------------------------

         integer(4) :: i,j,k

! ----------------------------------------------------------------------

         do i=2,X_BLOCK-1
            if (plntZone(i+1,j,k).eq.1.and.
     &          plntZone(i  ,j,k).eq.0) then

               xFlow(i,1) = 0.0d0

            endif
            if (plntZone(i  ,j,k).eq.1.and.
     &          plntZone(i+1,j,k).eq.0) then

               xFlow(i,1) = 0.0d0

            endif
         enddo

      end subroutine xFlowCorrection

! ----------------------------------------------------------------------
! --- The best ever known program --------------------------------------
! ----------------------------------------------------------------------

      subroutine yFlowCorrection (i,k)

         implicit none

! ----------------------------------------------------------------------
   
         common /yCalcFlow/ yFlow, yFlowMins, yFlowPlus
                      
         common /specAreas/ plntZone, starZone

! ----------------------------------------------------------------------

         real(8) :: yFlow     (Y_BLOCK,5)
         real(8) :: yFlowMins (Y_BLOCK,5,5)
         real(8) :: yFlowPlus (Y_BLOCK,5,5)

         integer(4) :: plntZone (X_BLOCK,Y_BLOCK,Z_BLOCK)
         integer(4) :: starZone (X_BLOCK,Y_BLOCK,Z_BLOCK)

! ----------------------------------------------------------------------

         integer(4) :: i,j,k

! ----------------------------------------------------------------------
   
         do j=2,Y_BLOCK-1
            if (plntZone(i,j+1,k).eq.1.and.
     &          plntZone(i,j  ,k).eq.0) then
       
               yFlow(j,1) = 0.0d0
       
            endif
            if (plntZone(i,j  ,k).eq.1.and.
     &          plntZone(i,j+1,k).eq.0) then
       
               yFlow(j,1) = 0.0d0
       
            endif
         enddo

! ----------------------------------------------------------------------

      end subroutine yFlowCorrection

! ----------------------------------------------------------------------
! --- The best ever known program --------------------------------------
! ----------------------------------------------------------------------

      subroutine zFlowCorrection (i,j)

         implicit none

! ----------------------------------------------------------------------
   
         common /zCalcFlow/ zFlow, zFlowMins, zFlowPlus
                      
         common /specAreas/ plntZone, starZone

! ----------------------------------------------------------------------

         real(8) :: zFlow     (Z_BLOCK,5)
         real(8) :: zFlowMins (Z_BLOCK,5,5)
         real(8) :: zFlowPlus (Z_BLOCK,5,5)

         integer(4) :: plntZone (X_BLOCK,Y_BLOCK,Z_BLOCK)
         integer(4) :: starZone (X_BLOCK,Y_BLOCK,Z_BLOCK)

! ----------------------------------------------------------------------

         integer(4) :: i,j,k

! ----------------------------------------------------------------------

         do k=2,Z_BLOCK-1
            if (plntZone(i,j,k+1).eq.1.and.
     &          plntZone(i,j,k  ).eq.0) then

               zFlow(k,1) = 0.0d0

            endif
            if (plntZone(i,j,k  ).eq.1.and.
     &          plntZone(i,j,k+1).eq.0) then

               zFlow(k,1) = 0.0d0

            endif
         enddo

! ----------------------------------------------------------------------

      end subroutine zFlowCorrection

! ----------------------------------------------------------------------
! --- The best ever known program --------------------------------------
! ----------------------------------------------------------------------

      subroutine xFlowCalculation (j,k,myRank,iteration)

         implicit none

! ----------------------------------------------------------------------

         common /lclCord_2/ dxh,  dyh,  dzh
         common /lclCord_3/ dxhi, dyhi, dzhi

         common /chiefData/ dens, velx, vely, velz,
     &                      pres, enth, srho, wave

         common /xCalcFlow/ xFlow, xFlowMins, xFlowPlus

         common /sondSpeed/ soundVel

         common /specAreas/ plntZone, starZone

! ----------------------------------------------------------------------

         real(8) ::  dxh (X_BLOCK),  dyh (Y_BLOCK),  dzh (Z_BLOCK)
         real(8) :: dxhi (X_BLOCK), dyhi (Y_BLOCK), dzhi (Z_BLOCK)

         real(8) :: dens (X_BLOCK,Y_BLOCK,Z_BLOCK)
         real(8) :: velx (X_BLOCK,Y_BLOCK,Z_BLOCK)
         real(8) :: vely (X_BLOCK,Y_BLOCK,Z_BLOCK)
         real(8) :: velz (X_BLOCK,Y_BLOCK,Z_BLOCK)
         real(8) :: pres (X_BLOCK,Y_BLOCK,Z_BLOCK)
         real(8) :: enth (X_BLOCK,Y_BLOCK,Z_BLOCK)
         real(8) :: srho (X_BLOCK,Y_BLOCK,Z_BLOCK)
         real(8) :: wave (X_BLOCK,Y_BLOCK,Z_BLOCK)

         real(8) :: xFlow     (X_BLOCK,5)
         real(8) :: xFlowMins (X_BLOCK,5,5)
         real(8) :: xFlowPlus (X_BLOCK,5,5)

         real(8) :: soundVel(X_BLOCK,Y_BLOCK,Z_BLOCK)

         integer(4) :: plntZone(X_BLOCK,Y_BLOCK,Z_BLOCK)
         integer(4) :: starZone(X_BLOCK,Y_BLOCK,Z_BLOCK)

! ----------------------------------------------------------------------

         real(8) :: minmod

! ----------------------------------------------------------------------

         real(8) :: dens_r, dens_l
         real(8) :: velx_r, velx_l
         real(8) :: vely_r, vely_l
         real(8) :: velz_r, velz_l
         real(8) :: enth_r, enth_l
         real(8) :: pres_r, pres_l
         real(8) :: sqdn_r, sqdn_l

         real(8) :: rev(5,5), prod(5),a(5),aa(5)
   
         real(8) :: tempa
         real(8) :: tempm

         real(8) :: velx_h
         real(8) :: vely_h
         real(8) :: velz_h
         real(8) :: enth_h

         real(8) :: temp_1, temp_2, tmp
   
         real(8) :: hdel, ch

         real(8) :: cumulVar
         real(8) :: temporary
         real(8) :: values(5)
         real(8) :: helpValueVelx, helpValuePres
 
         real(8) :: soundVel_h, absVelx_h

         real(8) :: const_1
         real(8) :: const_2
         real(8) :: const_3
         real(8) :: const_4

         integer(4) :: i, j, k , kk, l, m
         integer(4) :: myRank, iteration

! ----------------------------------------------------------------------

         do i=1,X_BLOCK-1

cifdef BLOCKED
            if (plntZone(i+1,j,k).eq.1.and.
     &          plntZone(i  ,j,k).eq.0) then
               sqdn_r = srho(i,j,k)
               velx_r =-velx(i,j,k)
               vely_r = vely(i,j,k)
               velz_r = velz(i,j,k)
               enth_r = enth(i,j,k)
               pres_r = pres(i,j,k)
               dens_r = dens(i,j,k)
            else
cendif
               sqdn_r = srho(i+1,j,k)
               velx_r = velx(i+1,j,k)
               vely_r = vely(i+1,j,k)
               velz_r = velz(i+1,j,k)
               enth_r = enth(i+1,j,k)
               pres_r = pres(i+1,j,k)
               dens_r = dens(i+1,j,k)
cifdef BLOCKED
            endif

            if (plntZone(i  ,j,k).eq.1.and.
     &          plntZone(i+1,j,k).eq.0) then
               velx_l =-velx(i+1,j,k)
               sqdn_l = srho(i+1,j,k)
               vely_l = vely(i+1,j,k)
               velz_l = velz(i+1,j,k)
               enth_l = enth(i+1,j,k)
               pres_l = pres(i+1,j,k)
               dens_l = dens(i+1,j,k)
            else
cendif
               velx_l = velx(i,j,k)
               sqdn_l = srho(i,j,k)
               vely_l = vely(i,j,k)
               velz_l = velz(i,j,k)
               enth_l = enth(i,j,k)
               pres_l = pres(i,j,k)
               dens_l = dens(i,j,k)
cifdef BLOCKED
            endif
cendif
      
            tempa = sqdn_r+sqdn_l
            tempm = sqdn_r*sqdn_l

            velx_h = (velx_r*sqdn_r+velx_l*sqdn_l)/tempa
            vely_h = (vely_r*sqdn_r+vely_l*sqdn_l)/tempa
            velz_h = (velz_r*sqdn_r+velz_l*sqdn_l)/tempa
            enth_h = (enth_r*sqdn_r+enth_l*sqdn_l)/tempa

            soundVel_h = (GAMMA_CONST*pres_l/dens_l*sqdn_l
     &                 +  GAMMA_CONST*pres_r/dens_r*sqdn_r)/tempa

            temp_1 = 0.5*tempm*(GAMMA_CONST-1)/tempa**2
            temp_2 = (velx_l-velx_r)**2 +
     &               (vely_l-vely_r)**2 +
     &               (velz_l-velz_r)**2

            ch = dsqrt(soundVel_h+temp_1*temp_2)

            prod(1) =  (velx_r-velx_l)*tempm + (pres_r-pres_l)/ch
            prod(2) =  (vely_r-vely_l)*tempm
            prod(3) =  (velz_r-velz_l)*tempm
            prod(4) =  (dens_r-dens_l)*ch    - (pres_r-pres_l)/ch
            prod(5) = -(velx_r-velx_l)*tempm + (pres_r-pres_l)/ch
     
            hdel = 0.125*(dabs(velx_h) +
     &                    dabs(vely_h) +
     &                    dabs(velz_h))

            absVelx_h = dabs(velx_h)

            a(1) = min(velx_h,velx_l)+ch
            aa(1) = velx_h+ch
            rev(1,1) = 1.0d0
            rev(1,2) = velx_h+ch
            rev(1,3) = vely_h
            rev(1,4) = velz_h
            rev(1,5) = enth_h+velx_h*ch

            a(2) = absVelx_h
            aa(2) = velx_h
            rev(2,1) = 0.0d0
            rev(2,2) = 0.0d0
            rev(2,3) = 2*ch
            rev(2,4) = 0.0d0
            rev(2,5) = 2*vely_h*ch

            a(3) = absVelx_h
            aa(3) = velx_h
            rev(3,1) = 0.0d0
            rev(3,2) = 0.0d0
            rev(3,3) = 0.0d0
            rev(3,4) = 2.0d0*ch
            rev(3,5) = 2.0d0*velz_h*ch

            a(4) = absVelx_h
            aa(4) = velx_h
            rev(4,1) = 2.0d0
            rev(4,2) = 2.0d0*velx_h
            rev(4,3) = 2.0d0*vely_h
            rev(4,4) = 2.0d0*velz_h
            rev(4,5) = velx_h**2+vely_h**2+velz_h**2

            a(5) = max(velx_h,velx_r)-ch
            aa(5) = velx_h-ch
            rev(5,1) = 1.0d0
            rev(5,2) = velx_h-ch
            rev(5,3) = vely_h
            rev(5,4) = velz_h
            rev(5,5) = enth_h-velx_h*ch

            xFlow(i,1) = dens_l*velx_l
c           xFlow(i,2) = dens_l*(velx_l**2)+pres_l
            xFlow(i,3) = dens_l*velx_l*vely_l
            xFlow(i,4) = dens_l*velx_l*velz_l
            xFlow(i,5) = dens_l*enth_l*velx_l

            helpValueVelx = dens_l*(velx_l**2) + dens_r*(velx_r**2)
            helpValuePres = pres_l + pres_r

            xFlow(i,2) = helpValueVelx + helpValuePres

c           xFlow(i,2) = xFlow(i,2) + pres_l
c           xFlow(i,2) = xFlow(i,2) + pres_r

c           xFlow(i,1) = dens_l*velx_l           + dens_r*velx_r
c           xFlow(i,2) = dens_l*velx_l**2+pres_l + dens_r*velx_r**2+pres_r
c           xFlow(i,3) = dens_l*velx_l*vely_l    + dens_r*velx_r*vely_r
c           xFlow(i,4) = dens_l*velx_l*velz_l    + dens_r*velx_r*velz_r
c           xFlow(i,5) = dens_l*enth_l*velx_l    + dens_r*enth_r*velx_r

c           if (k.eq.(Z_BLOCK/2)) then
c           if (j.eq.3) then
c           if (i.eq.12)then
c              if (myRank.eq.9) then
c                 write(*,*) "Left values ---->", iteration, xFlow(i,2), helpValueVelx, helpValuePres
c                 endif
c           endif
c           if (i.eq.12)then
c              if (myRank.eq.10) then
c                 write(*,*) "Rght values ---->", iteration, xFlow(i,2), helpValueVelx, helpValuePres
c                 endif
c           endif
c           endif
c           endif

            xFlow(i,1) = xFlow(i,1) + dens_r*velx_r
c           xFlow(i,2) = xFlow(i,2) + dens_r*(velx_r**2)+pres_r
            xFlow(i,3) = xFlow(i,3) + dens_r*velx_r*vely_r
            xFlow(i,4) = xFlow(i,4) + dens_r*velx_r*velz_r
            xFlow(i,5) = xFlow(i,5) + dens_r*enth_r*velx_r

c           if (k.eq.(Z_BLOCK/2)) then
c           if (j.eq.3) then
c           if (i.eq.12)then
c              if (myRank.eq.9) then
c                 write(*,*) "Left values after ", iteration, xFlow(i,2), dens_r*(velx_r**2), pres_r
c                 endif
c           endif
c           if (i.eq.12)then
c              if (myRank.eq.10) then
c                 write(*,*) "Rght values after", iteration, xFlow(i,2), dens_r*(velx_r**2), pres_r
c                 endif
c           endif
c           endif
c           endif

            xFlow(i,1) = xFlow(i,1)/2.0d0
            xFlow(i,2) = xFlow(i,2)/2.0d0
            xFlow(i,3) = xFlow(i,3)/2.0d0
            xFlow(i,4) = xFlow(i,4)/2.0d0
            xFlow(i,5) = xFlow(i,5)/2.0d0

c           if (k.eq.(Z_BLOCK/2)) then
c           if (j.eq.3) then
c           if (i.eq.12)then
c              if (myRank.eq.9) then
c                 write(*,*) "Left values", iteration, xFlow(i,2), dens_l, velx_l, pres_l, dens_r, velx_r, pres_r
c                 endif
c           endif
c           if (i.eq.12)then
c              if (myRank.eq.10) then
c                 write(*,*) "Rght values", iteration, xFlow(i,2), dens_r, velx_r, pres_r, dens_l, velx_l, pres_l
c                 endif
c           endif
c           endif
c           endif

c           if (k.eq.(Z_BLOCK/2)) then
c           if (j.eq.3) then
c           if (i.eq.12) then
c              if (myRank.eq.9) then
c                 write(*,*) "Left left before", iteration, xFlow(i,2)
c              endif
c              if (myRank.eq.10) then
c                 write(*,*) "Rght rght before", iteration, xFlow(i,2)
c              endif
c           endif
c           endif
c           endif


            do kk=1,5
c              cumulVar = 0.0d0
               do l=1,5
                  tmp = dabs(a(l))/(4.0d0*ch)*prod(l)
c                 cumulVar = cumulVar + tmp*rev(l,kk)
                  values(l) = tmp*rev(l,kk)
               enddo
               
               do m=1,4
               do l=1,4
                  if (dabs(values(l+1)).gt.dabs(values(l))) then
                     temporary = values(l)
                     values(l) = values(l+1)
                     values(l+1) = temporary
                  endif
               enddo
               enddo

               cumulVar = 0.0d0

               do l=1,5
                  cumulVar = cumulVar + values(l)

c                  if (kk.eq.5) then
c                  if (k.eq.(Z_BLOCK/2)) then
c                  if (j.eq.3) then
c                  if (i.eq.11) then
c                     if (myRank.eq.9) then
c                        write(*,*) "Inside", iteration, l, cumulVar, values(l)
c                     endif
c                  endif
c                  if (i.eq.13) then
c                     if (myRank.eq.10) then
c                        write(*,*) "Inside", iteration, l, cumulVar, values(l)
c                     endif
c                  endif
c                  endif
c                  endif
c                  endif



               enddo
               xFlow(i,kk) = xFlow(i,kk) - cumulVar

c              if (kk.eq.5) then
c              if (k.eq.(Z_BLOCK/2)) then
c              if (j.eq.3) then
c              if (i.eq.11) then
c                 if (myRank.eq.9) then
c                    write(*,*) "Left left inside", iteration, xFlow(i,5), cumulVar
c                 endif
c              endif
c              if (i.eq.13) then
c                 if (myRank.eq.10) then
c                    write(*,*) "Rght rght inside", iteration, xFlow(i,5), cumulVar
c                 endif
c              endif
c              endif
c              endif
c              endif

            enddo

c           do l=1,5
c              tmp = dabs(a(l))/(4.0d0*ch)*prod(l)
c              do kk=1,5
c                 xFlow(i,kk) = xFlow(i,kk)-tmp*rev(l,kk)
c              enddo
c           enddo



c           if (k.eq.(Z_BLOCK/2)) then
c           if (j.eq.3) then
c           if (i.eq.11) then
c              if (myRank.eq.9) then
c                 write(*,*) "Left left", iteration, xFlow(i,5)
c              endif
c           endif
c           if (i.eq.13) then
c              if (myRank.eq.10) then
c                 write(*,*) "Rght rght", iteration, xFlow(i,5)
c              endif
c           endif
c           endif
c           endif


c           enddo



            do l=1,5
               tmp = aa(l)/(2.0d0*ch)*prod(l)
               if (aa(l).lt.0.0) then
                  do kk=1,5
                     xFlowMins(i,kk,l) = tmp*rev(l,kk)
                     xFlowPlus(i,kk,l) = 0.0d0
                  enddo
               else
                  do kk=1,5
                     xFlowMins(i,kk,l) = 0.0d0
                     xFlowPlus(i,kk,l) = tmp*rev(l,kk)
                  enddo
               endif
            enddo
         enddo

c        if (myRank.eq.9) then
c        if (k.eq.(Z_BLOCK/2)) then
c        if (j.eq.3) then
c           write(*,*) 'Flow left first ', iteration, xFlow(12,2)
c        endif
c        endif
c        endif
c        if (myRank.eq.10) then
c        if (k.eq.(Z_BLOCK/2)) then
c        if (j.eq.3) then
c           write(*,*) 'Flow right first', iteration, xFlow(12,2)
c        endif
c        endif
c        endif

         const_1 = -0.25d0*(1.0d0-FIS_CONST)
         const_2 = -0.25d0*(1.0d0+FIS_CONST)
         const_3 = +0.25d0*(1.0d0+FIS_CONST)
         const_4 = +0.25d0*(1.0d0-FIS_CONST)
     
c#ifdef BLOCKED
         do i=2,X_BLOCK-2
            if (
cc   &          plntZone(i-1,j,k).eq.1.or.
     &          plntZone(i  ,j,k).eq.1.or.
     &          plntZone(i+1,j,k).eq.1.or.
cc   &          starZone(i-1,j,k).eq.1.or.
     &          starZone(i  ,j,k).eq.1.or.
     &          starZone(i+1,j,k).eq.1) cycle
            do kk=1,5
            do l=1,5
c              xFlow(i,kk) = xFlow(i,kk)+dxh(i)*(
c    &                   +const_1*minmod(xFlowMins(i+1,kk,l)*dxhi(i+1),
c    &                         BET_CONST*xFlowMins(i  ,kk,l)*dxhi(i  ))
c    &                   +const_2*minmod(xFlowMins(i  ,kk,l)*dxhi(i  ),
c    &                         BET_CONST*xFlowMins(i+1,kk,l)*dxhi(i+1))
c    &                   +const_3*minmod(xFlowPlus(i  ,kk,l)*dxhi(i  ),
c    &                         BET_CONST*xFlowPlus(i-1,kk,l)*dxhi(i-1))
c    &                   +const_4*minmod(xFlowPlus(i-1,kk,l)*dxhi(i-1),
c    &                         BET_CONST*xFlowPlus(i  ,kk,l)*dxhi(i  )))
               values(l) = +dxh(i)*(
     &                   +const_1*minmod(xFlowMins(i+1,kk,l)*dxhi(i+1),
     &                         BET_CONST*xFlowMins(i  ,kk,l)*dxhi(i  ))
     &                   +const_3*minmod(xFlowPlus(i  ,kk,l)*dxhi(i  ),
     &                         BET_CONST*xFlowPlus(i-1,kk,l)*dxhi(i-1))
     &                   +const_2*minmod(xFlowMins(i  ,kk,l)*dxhi(i  ),
     &                         BET_CONST*xFlowMins(i+1,kk,l)*dxhi(i+1))
     &                   +const_4*minmod(xFlowPlus(i-1,kk,l)*dxhi(i-1),
     &                         BET_CONST*xFlowPlus(i  ,kk,l)*dxhi(i  )))
            enddo
            do m=1,4
            do l=1,4
               if (dabs(values(l+1)).gt.dabs(values(l))) then
                  temporary = values(l)
                  values(l) = values(l+1)
                  values(l+1) = temporary
               endif
            enddo
            enddo
            do l=1,5
               xFlow(i,kk) = xFlow(i,kk) + values(l)
            enddo
             

c           if (iteration.eq.1) then
c              if (k.eq.(Z_BLOCK/2)) then
c              if (j.eq.3) then
c              if (i.eq.15) then
c                 if (myRank.eq.9) then
c                       open (10,file="dumpLeft.log",form='unformatted')
c                       write(10) xFlow(14,2)
c                       close(10)
c                    write(*,*) "Left  ", iteration, xFlow(14,2), dens_l, velx_l, dens_r, velx_r, pres_l, pres_r
c                    write(*,*) "LeftStep", xFlow(15,2)
c                 endif
c              endif
c              if (i.eq.9) then
c                 if (myRank.eq.10) then
c                       open (10,file="dumpRght.log",form='unformatted')
c                       write(10) xFlow(10,2)
c                       close(10)
c                    write(*,*) "RghtStep", xFlow(9,2)
c                    write(*,*) "Rght  ", iteration, xFlow(,2), dens_l, velx_l, dens_r, velx_r, pres_l, pres_r
c                 endif
c              endif
c              endif
c              endif
c           endif
            enddo
         enddo


c#endif


c        if (k.eq.(Z_BLOCK/2)) then
c        if (j.eq.3) then
c           if (myRank.eq.9) then
c              write(*,*) "Left 1 flow", iteration, xFlow(12,1), xFlow(11,1)
c              write(*,*) "Left 2 flow", iteration, xFlow(12,2), xFlow(11,2)
c              write(*,*) "Left 3 flow", iteration, xFlow(12,3), xFlow(11,3)
c              write(*,*) "Left 4 flow", iteration, xFlow(12,4), xFlow(11,4)
c              write(*,*) "Left 5 flow", iteration, xFlow(12,5), xFlow(11,5)
c           endif                                                     
c           if (myRank.eq.10) then                                     
c              write(*,*) "Rght 1 flow", iteration, xFlow(12,1), xFlow(13,1)
c              write(*,*) "Rght 2 flow", iteration, xFlow(12,2), xFlow(13,2)
c              write(*,*) "Rght 3 flow", iteration, xFlow(12,3), xFlow(13,3)
c              write(*,*) "Rght 4 flow", iteration, xFlow(12,4), xFlow(13,4)
c              write(*,*) "Rght 5 flow", iteration, xFlow(12,5), xFlow(13,5)
c           endif
c        endif
c        endif

! ----------------------------------------------------------------------

      end subroutine xFlowCalculation

! ----------------------------------------------------------------------
! --- The best ever known program --------------------------------------
! ----------------------------------------------------------------------

      subroutine yFlowCalculation (i,k)

         implicit none

! ----------------------------------------------------------------------

         common /lclCord_2/ dxh,  dyh,  dzh
         common /lclCord_3/ dxhi, dyhi, dzhi

         common /chiefData/ dens, velx, vely, velz,
     &                      pres, enth, srho, wave

         common /yCalcFlow/ yFlow, yFlowMins, yFlowPlus

         common /sondSpeed/ soundVel

         common /specAreas/ plntZone, starZone

! ----------------------------------------------------------------------

         real(8) ::  dxh (X_BLOCK),  dyh (Y_BLOCK),  dzh (Z_BLOCK)
         real(8) :: dxhi (X_BLOCK), dyhi (Y_BLOCK), dzhi (Z_BLOCK)

         real(8) :: dens (X_BLOCK,Y_BLOCK,Z_BLOCK)
         real(8) :: velx (X_BLOCK,Y_BLOCK,Z_BLOCK)
         real(8) :: vely (X_BLOCK,Y_BLOCK,Z_BLOCK)
         real(8) :: velz (X_BLOCK,Y_BLOCK,Z_BLOCK)
         real(8) :: pres (X_BLOCK,Y_BLOCK,Z_BLOCK)
         real(8) :: enth (X_BLOCK,Y_BLOCK,Z_BLOCK)
         real(8) :: srho (X_BLOCK,Y_BLOCK,Z_BLOCK)
         real(8) :: wave (X_BLOCK,Y_BLOCK,Z_BLOCK)

         real(8) :: yFlow     (Y_BLOCK,5)
         real(8) :: yFlowMins (Y_BLOCK,5,5)
         real(8) :: yFlowPlus (Y_BLOCK,5,5)

         real(8) :: soundVel(X_BLOCK,Y_BLOCK,Z_BLOCK)

         integer(4) :: plntZone(X_BLOCK,Y_BLOCK,Z_BLOCK)
         integer(4) :: starZone(X_BLOCK,Y_BLOCK,Z_BLOCK)

! ----------------------------------------------------------------------

         real(8) :: minmod

! ----------------------------------------------------------------------

         real(8) :: dens_r, dens_l
         real(8) :: velx_r, velx_l
         real(8) :: vely_r, vely_l
         real(8) :: velz_r, velz_l
         real(8) :: enth_r, enth_l
         real(8) :: pres_r, pres_l
         real(8) :: sqdn_r, sqdn_l

         real(8) :: rev(5,5), prod(5),a(5),aa(5)
   
         real(8) :: tempa
         real(8) :: tempm

         real(8) :: velx_h
         real(8) :: vely_h
         real(8) :: velz_h
         real(8) :: enth_h

         real(8) :: temp_1, temp_2, tmp
   
         real(8) :: hdel, ch
 
         real(8) :: soundVel_h, absVely_h

         real(8) :: cumulVar
         real(8) :: temporary
         real(8) :: values(5)
         real(8) :: helpValueVely, helpValuePres
 
c        real(8) :: soundVel_h, absVelx_h

         real(8) :: const_1
         real(8) :: const_2
         real(8) :: const_3
         real(8) :: const_4

         integer(4) :: i, j, k , kk, l, m

! ----------------------------------------------------------------------

         do j=1,Y_BLOCK-1

            if (plntZone(i,j+1,k).eq.1.and. 
     &          plntZone(i,j  ,k).eq.0) then
               sqdn_r = srho(i,j,k)
               vely_r =-vely(i,j,k)
               velx_r = velx(i,j,k)
               velz_r = velz(i,j,k)
               enth_r = enth(i,j,k)
               pres_r = pres(i,j,k)
               dens_r = dens(i,j,k)
            else
               sqdn_r = srho(i,j+1,k)
               vely_r = vely(i,j+1,k)
               velx_r = velx(i,j+1,k)
               velz_r = velz(i,j+1,k)
               enth_r = enth(i,j+1,k)
               pres_r = pres(i,j+1,k)
               dens_r = dens(i,j+1,k)
            end if


            if (plntZone(i,j  ,k).eq.1.and.
     &          plntZone(i,j+1,k).eq.0) then
               vely_l =-vely(i,j+1,k)
               sqdn_l = srho(i,j+1,k)
               velx_l = velx(i,j+1,k)
               velz_l = velz(i,j+1,k)
               enth_l = enth(i,j+1,k)
               pres_l = pres(i,j+1,k)
               dens_l = dens(i,j+1,k)
            else
               vely_l = vely(i,j,k)
               sqdn_l = srho(i,j,k)
               velx_l = velx(i,j,k)
               velz_l = velz(i,j,k)
               enth_l = enth(i,j,k)
               pres_l = pres(i,j,k)
               dens_l = dens(i,j,k)
            endif
              
            tempa = sqdn_r+sqdn_l
            tempm = sqdn_r*sqdn_l

            vely_h = (vely_r*sqdn_r+vely_l*sqdn_l)/tempa
            velx_h = (velx_r*sqdn_r+velx_l*sqdn_l)/tempa
            velz_h = (velz_r*sqdn_r+velz_l*sqdn_l)/tempa
            enth_h = (enth_r*sqdn_r+enth_l*sqdn_l)/tempa

            soundVel_h = (GAMMA_CONST*pres_l/dens_l*sqdn_l
     &                  + GAMMA_CONST*pres_r/dens_r*sqdn_r)/tempa

            temp_1 = 0.5*tempm*(GAMMA_CONST-1)/tempa**2
            temp_2 = (velx_l-velx_r)**2+
     *               (vely_l-vely_r)**2+
     *               (velz_l-velz_r)**2

            ch = dsqrt(soundVel_h+temp_1*temp_2)

            prod(1) =  (vely_r-vely_l)*tempm +(pres_r-pres_l)/ch
            prod(2) =  (velx_r-velx_l)*tempm
            prod(3) =  (velz_r-velz_l)*tempm
            prod(4) =  (dens_r-dens_l)*ch    - (pres_r-pres_l)/ch
            prod(5) = -(vely_r-vely_l)*tempm + (pres_r-pres_l)/ch
     
            hdel = 0.125*(dabs(velx_h) +
     &                    dabs(vely_h) +
     &                    dabs(velz_h))

            absVely_h = dabs(vely_h)
                
            a(1) = min(vely_h,vely_l)+ch
            aa(1) = vely_h+ch
            rev(1,1) = 1.0d0
            rev(1,2) = velx_h
            rev(1,3) = vely_h+ch
            rev(1,4) = velz_h
            rev(1,5) = enth_h+vely_h*ch

            a(2) = absVely_h
            aa(2) = vely_h
            rev(2,1) = 0.0d0
            rev(2,2) = 2.0d0*ch
            rev(2,3) = 0.0d0
            rev(2,4) = 0.0d0
            rev(2,5) = 2*velx_h*ch

            a(3) = absVely_h
            aa(3) = vely_h
            rev(3,1) = 0.0d0
            rev(3,2) = 0.0d0
            rev(3,3) = 0.0d0
            rev(3,4) = 2*ch
            rev(3,5) = 2*velz_h*ch

            a(4) = absVely_h
            aa(4) = vely_h
            rev(4,1) = 2.0d0
            rev(4,2) = 2*velx_h
            rev(4,3) = 2*vely_h
            rev(4,4) = 2*velz_h
            rev(4,5) = velx_h**2+vely_h**2+velz_h**2

            a(5) = max(vely_h,vely_r)-ch
            aa(5) = vely_h-ch
            rev(5,1) = 1.0d0
            rev(5,2) = velx_h
            rev(5,3) = vely_h-ch
            rev(5,4) = velz_h
            rev(5,5) = enth_h-vely_h*ch

            yFlow(j,1) = dens_l*vely_l
            yFlow(j,2) = dens_l*vely_l*velx_l
c           yFlow(j,3) = dens_l*vely_l**2+pres_l
            yFlow(j,4) = dens_l*vely_l*velz_l
            yFlow(j,5) = dens_l*enth_l*vely_l

            yFlow(j,1) = yFlow(j,1)+dens_r*vely_r
            yFlow(j,2) = yFlow(j,2)+dens_r*vely_r*velx_r
c           yFlow(j,3) = yFlow(j,3)+dens_r*vely_r**2+pres_r
            yFlow(j,4) = yFlow(j,4)+dens_r*vely_r*velz_r
            yFlow(j,5) = yFlow(j,5)+dens_r*enth_r*vely_r

            helpValueVely = dens_l*(vely_l**2) + dens_r*(vely_r**2)
            helpValuePres = pres_l + pres_r

            yFlow(j,3) = helpValueVely + helpValuePres

            yFlow(j,1) = yFlow(j,1)/2.0d0
            yFlow(j,2) = yFlow(j,2)/2.0d0
            yFlow(j,3) = yFlow(j,3)/2.0d0
            yFlow(j,4) = yFlow(j,4)/2.0d0
            yFlow(j,5) = yFlow(j,5)/2.0d0

c           do l=1,5
c              tmp = dabs(a(l))/(4.0d0*ch)*prod(l)
c              do kk=1,5
c                 yFlow(j,kk) = yFlow(j,kk)-tmp*rev(l,kk)
c              enddo
c           enddo

            do kk=1,5
               do l=1,5
                  tmp = dabs(a(l))/(4.0d0*ch)*prod(l)
                  values(l) = tmp*rev(l,kk)
               enddo
               
               do m=1,4
               do l=1,4
                  if (dabs(values(l+1)).gt.dabs(values(l))) then
                     temporary = values(l)
                     values(l) = values(l+1)
                     values(l+1) = temporary
                  endif
               enddo
               enddo

               cumulVar = 0.0d0

               do l=1,5
                  cumulVar = cumulVar + values(l)
               enddo
               yFlow(j,kk) = yFlow(j,kk) - cumulVar

            enddo

            do l=1,5
               tmp = aa(l)/(2*ch)*prod(l)
               if (aa(l).lt.0.0d0) then
                  do kk=1,5
                     yFlowMins(j,kk,l) = tmp*rev(l,kk)
                     yFlowPlus(j,kk,l) = 0.0d0
                  enddo
               else
                  do kk=1,5
                     yFlowMins(j,kk,l) = 0.0d0
                     yFlowPlus(j,kk,l) = tmp*rev(l,kk)
                  enddo
               endif
            enddo
         enddo

         const_1 = -0.25d0*(1.0d0-FIS_CONST)
         const_2 = -0.25d0*(1.0d0+FIS_CONST)
         const_3 = +0.25d0*(1.0d0+FIS_CONST)
         const_4 = +0.25d0*(1.0d0-FIS_CONST)

         do j=2,Y_BLOCK-2
            if (
c    &          plntZone(i,j-1,k).eq.1.or.
     &          plntZone(i,j  ,k).eq.1.or.
     &          plntZone(i,j+1,k).eq.1.or.
c    &          starZone(i,j-1,k).eq.1.or.
     &          starZone(i,j  ,k).eq.1.or.
     &          starZone(i,j+1,k).eq.1) cycle
            do kk=1,5
            do l=1,5
c              yFlow(j,kk) = yFlow(j,kk)+dyh(j)*(
c    &  -0.25d0*(1.0d0-FIS_CONST)*minmod(yFlowMins(j+1,kk,l)*dyhi(j+1),
c    &                         BET_CONST*yFlowMins(j  ,kk,l)*dyhi(j  ))
c    &  +0.25d0*(1.0d0+FIS_CONST)*minmod(yFlowPlus(j  ,kk,l)*dyhi(j  ),
c    &                         BET_CONST*yFlowPlus(j-1,kk,l)*dyhi(j-1))
c    &  -0.25d0*(1.0d0+FIS_CONST)*minmod(yFlowMins(j  ,kk,l)*dyhi(j  ),
c    &                         BET_CONST*yFlowMins(j+1,kk,l)*dyhi(j+1))
c    &  +0.25d0*(1.0d0-FIS_CONST)*minmod(yFlowPlus(j-1,kk,l)*dyhi(j-1),
c    &                         BET_CONST*yFlowPlus(j  ,kk,l)*dyhi(j  )))

               values(l) = +dyh(j)*(
     &                   +const_1*minmod(yFlowMins(j+1,kk,l)*dyhi(j+1),
     &                         BET_CONST*yFlowMins(j  ,kk,l)*dyhi(j  ))
     &                   +const_3*minmod(yFlowPlus(j  ,kk,l)*dyhi(j  ),
     &                         BET_CONST*yFlowPlus(j-1,kk,l)*dyhi(j-1))
     &                   +const_2*minmod(yFlowMins(j  ,kk,l)*dyhi(j  ),
     &                         BET_CONST*yFlowMins(j+1,kk,l)*dyhi(j+1))
     &                   +const_4*minmod(yFlowPlus(j-1,kk,l)*dyhi(j-1),
     &                         BET_CONST*yFlowPlus(j  ,kk,l)*dyhi(j  )))
            enddo

            do m=1,4
            do l=1,4
               if (dabs(values(l+1)).gt.dabs(values(l))) then
                  temporary = values(l)
                  values(l) = values(l+1)
                  values(l+1) = temporary
               endif
            enddo
            enddo

            do l=1,5
               yFlow(j,kk) = yFlow(j,kk) + values(l)
            enddo

            enddo
         enddo

      end subroutine yFlowCalculation

! ----------------------------------------------------------------------
! --- The best ever known program --------------------------------------
! ----------------------------------------------------------------------

      subroutine zFlowCalculation (i,j)

         implicit none

! ----------------------------------------------------------------------

         common /lclCord_2/ dxh,  dyh,  dzh
         common /lclCord_3/ dxhi, dyhi, dzhi

         common /chiefData/ dens, velx, vely, velz,
     &                      pres, enth, srho, wave

         common /zCalcFlow/ zFlow, zFlowMins, zFlowPlus

         common /sondSpeed/ soundVel

         common /specAreas/ plntZone, starZone

! ----------------------------------------------------------------------

         real(8) ::  dxh (X_BLOCK),  dyh (Y_BLOCK),  dzh (Z_BLOCK)
         real(8) :: dxhi (X_BLOCK), dyhi (Y_BLOCK), dzhi (Z_BLOCK)

         real(8) :: dens (X_BLOCK,Y_BLOCK,Z_BLOCK)
         real(8) :: velx (X_BLOCK,Y_BLOCK,Z_BLOCK)
         real(8) :: vely (X_BLOCK,Y_BLOCK,Z_BLOCK)
         real(8) :: velz (X_BLOCK,Y_BLOCK,Z_BLOCK)
         real(8) :: pres (X_BLOCK,Y_BLOCK,Z_BLOCK)
         real(8) :: enth (X_BLOCK,Y_BLOCK,Z_BLOCK)
         real(8) :: srho (X_BLOCK,Y_BLOCK,Z_BLOCK)
         real(8) :: wave (X_BLOCK,Y_BLOCK,Z_BLOCK)

         real(8) :: zFlow     (Z_BLOCK,5)
         real(8) :: zFlowMins (Z_BLOCK,5,5)
         real(8) :: zFlowPlus (Z_BLOCK,5,5)

         real(8) :: soundVel (X_BLOCK,Y_BLOCK,Z_BLOCK)

         integer(4) :: plntZone(X_BLOCK,Y_BLOCK,Z_BLOCK)
         integer(4) :: starZone(X_BLOCK,Y_BLOCK,Z_BLOCK)

! ----------------------------------------------------------------------

         real(8) :: minmod

! ----------------------------------------------------------------------

         real(8) :: dens_r, dens_l
         real(8) :: velx_r, velx_l
         real(8) :: vely_r, vely_l
         real(8) :: velz_r, velz_l
         real(8) :: enth_r, enth_l
         real(8) :: pres_r, pres_l
         real(8) :: sqdn_r, sqdn_l

         real(8) :: rev(5,5), prod(5),a(5),aa(5)
   
         real(8) :: tempa
         real(8) :: tempm

         real(8) :: velx_h
         real(8) :: vely_h
         real(8) :: velz_h
         real(8) :: enth_h

         real(8) :: temp_1, temp_2, tmp
   
         real(8) :: hdel, ch
 
         real(8) :: soundVel_h, absVelz_h

         real(8) :: cumulVar
         real(8) :: temporary
         real(8) :: values(5)
         real(8) :: helpValueVelz, helpValuePres
 
         real(8) :: const_1
         real(8) :: const_2
         real(8) :: const_3
         real(8) :: const_4

         integer(4) :: i, j, k , kk, l, m

! ----------------------------------------------------------------------

         do k=1,Z_BLOCK-1

            if (plntZone(i,j,k+1).eq.1.and.
     &          plntZone(i,j,k  ).eq.0) then
               sqdn_r = srho(i,j,k)
               velx_r = velx(i,j,k)
               vely_r = vely(i,j,k)
               velz_r =-velz(i,j,k)
               enth_r = enth(i,j,k)
               pres_r = pres(i,j,k)
               dens_r = dens(i,j,k)
            else
               sqdn_r = srho(i,j,k+1)
               velx_r = velx(i,j,k+1)
               vely_r = vely(i,j,k+1)
               velz_r = velz(i,j,k+1)
               enth_r = enth(i,j,k+1)
               pres_r = pres(i,j,k+1)
               dens_r = dens(i,j,k+1)
            end if

            if (plntZone(i,j,k  ).eq.1.and.
     &          plntZone(i,j,k+1).eq.0) then
               sqdn_l = srho(i,j,k+1)
               velx_l = velx(i,j,k+1)
               vely_l = vely(i,j,k+1)
               velz_l =-velz(i,j,k+1)
               enth_l = enth(i,j,k+1)
               pres_l = pres(i,j,k+1)
               dens_l = dens(i,j,k+1)
            else
               sqdn_l = srho(i,j,k)
               velx_l = velx(i,j,k)
               vely_l = vely(i,j,k)
               velz_l = velz(i,j,k)
               enth_l = enth(i,j,k)
               pres_l = pres(i,j,k)
               dens_l = dens(i,j,k)
            end if

            tempa = sqdn_r+sqdn_l
            tempm = sqdn_r*sqdn_l

            velx_h = (velx_r*sqdn_r+velx_l*sqdn_l)/tempa
            vely_h = (vely_r*sqdn_r+vely_l*sqdn_l)/tempa
            velz_h = (velz_r*sqdn_r+velz_l*sqdn_l)/tempa
            enth_h = (enth_r*sqdn_r+enth_l*sqdn_l)/tempa

            soundVel_h = (GAMMA_CONST*pres_l/dens_l*sqdn_l
     &                  + GAMMA_CONST*pres_r/dens_r*sqdn_r)/tempa

            temp_1 = 0.5*tempm*(GAMMA_CONST-1)/tempa**2

            temp_2 = (velx_l-velx_r)**2+
     &               (vely_l-vely_r)**2+
     &               (velz_l-velz_r)**2

            ch = dsqrt(soundVel_h+temp_1*temp_2)

            prod(1) =  (velz_r-velz_l)*tempm +(pres_r-pres_l)/ch
            prod(2) =  (velx_r-velx_l)*tempm
            prod(3) =  (vely_r-vely_l)*tempm
            prod(4) =  (dens_r-dens_l)*ch    - (pres_r-pres_l)/ch
            prod(5) = -(velz_r-velz_l)*tempm + (pres_r-pres_l)/ch

            hdel = 0.125*(dabs(velx_h) +
     &                    dabs(vely_h) +
     &                    dabs(velz_h))

            absVelz_h = dabs(velz_h)

            a(1) = min(velz_h,velz_l)+ch
            aa(1) = velz_h+ch
            rev(1,1) = 1.0d0
            rev(1,2) = velx_h
            rev(1,3) = vely_h
            rev(1,4) = velz_h+ch
            rev(1,5) = enth_h+velz_h*ch

            a(2) = absVelz_h
            aa(2) = velz_h
            rev(2,1) = 0.0d0
            rev(2,2) = 2.0d0*ch
            rev(2,3) = 0.0d0
            rev(2,4) = 0.0d0
            rev(2,5) = 2.0d0*velx_h*ch

            a(3) = absVelz_h
            aa(3) = velz_h
            rev(3,1) = 0.0d0
            rev(3,2) = 0.0d0
            rev(3,3) = 2.0d0*ch
            rev(3,4) = 0.0d0
            rev(3,5) = 2.0d0*vely_h*ch

            a(4) = absVelz_h
            aa(4) = velz_h
            rev(4,1) = 2.0
            rev(4,2) = 2.0d0*velx_h
            rev(4,3) = 2.0d0*vely_h
            rev(4,4) = 2.0d0*velz_h
            rev(4,5) = velx_h**2+vely_h**2+velz_h**2

            a(5) = max(velz_h,velz_r)-ch
            aa(5) = velz_h-ch
            rev(5,1) = 1.0d0
            rev(5,2) = velx_h
            rev(5,3) = vely_h
            rev(5,4) = velz_h-ch
            rev(5,5) = enth_h-velz_h*ch

            zFlow(k,1) = dens_l*velz_l
            zFlow(k,2) = dens_l*velz_l*velx_l
            zFlow(k,3) = dens_l*velz_l*vely_l
c           zFlow(k,4) = dens_l*velz_l**2 + pres_l
            zFlow(k,5) = dens_l*enth_l*velz_l

            zFlow(k,1) = zFlow(k,1) + dens_r*velz_r
            zFlow(k,2) = zFlow(k,2) + dens_r*velz_r*velx_r
            zFlow(k,3) = zFlow(k,3) + dens_r*velz_r*vely_r
c           zFlow(k,4) = zFlow(k,4) + dens_r*velz_r**2 + pres_r
            zFlow(k,5) = zFlow(k,5) + dens_r*enth_r*velz_r

            helpValueVelz = dens_l*(velz_l**2) + dens_r*(velz_r**2)
            helpValuePres = pres_l + pres_r

            zFlow(k,4) = helpValueVelz + helpValuePres

            zFlow(k,1) = zFlow(k,1)/2.0d0
            zFlow(k,2) = zFlow(k,2)/2.0d0
            zFlow(k,3) = zFlow(k,3)/2.0d0
            zFlow(k,4) = zFlow(k,4)/2.0d0
            zFlow(k,5) = zFlow(k,5)/2.0d0

c           do l=1,5
c              tmp = dabs(a(l))/(4.0d0*ch)*prod(l)
c              do kk=1,5
c                 zFlow(k,kk) = zFlow(k,kk) - tmp*rev(l,kk)
c              enddo
c           enddo

            do kk=1,5
               do l=1,5
                  tmp = dabs(a(l))/(4.0d0*ch)*prod(l)
                  values(l) = tmp*rev(l,kk)
               enddo
               
               do m=1,4
               do l=1,4
                  if (dabs(values(l+1)).gt.dabs(values(l))) then
                     temporary = values(l)
                     values(l) = values(l+1)
                     values(l+1) = temporary
                  endif
               enddo
               enddo

               cumulVar = 0.0d0

               do l=1,5
                  cumulVar = cumulVar + values(l)
               enddo
               zFlow(k,kk) = zFlow(k,kk) - cumulVar

            enddo

            do l=1,5
               tmp = aa(l)/(2.d0*ch)*prod(l)
               if (aa(l).lt.0.0d0) then
                  do kk=1,5
                     zFlowMins(k,kk,l) = tmp*rev(l,kk)
                     zFlowPlus(k,kk,l) = 0.0d0
                  enddo
               else
                  do kk=1,5
                     zFlowMins(k,kk,l) = 0.0d0
                     zFlowPlus(k,kk,l) = tmp*rev(l,kk)
                  enddo
               endif
            enddo
         enddo

         const_1 = -0.25d0*(1.0d0-FIS_CONST)
         const_2 = -0.25d0*(1.0d0+FIS_CONST)
         const_3 = +0.25d0*(1.0d0+FIS_CONST)
         const_4 = +0.25d0*(1.0d0-FIS_CONST)

         do k=2,Z_BLOCK-2
            if (
c    &          plntZone(i,j,k-1).eq.1.or.
     &          plntZone(i,j,k  ).eq.1.or.
     &          plntZone(i,j,k+1).eq.1.or.
c    &          starZone(i,j,k-1).eq.1.or.
     &          starZone(i,j,k  ).eq.1.or.
     &          starZone(i,j,k+1).eq.1) cycle
            do kk=1,5
            do l=1,5
c              zFlow(k,kk) = zFlow(k,kk) + dzh(k)*(
c    &  -0.25d0*(1.0d0-FIS_CONST)*minmod(zFlowMins(k+1,kk,l)*dzhi(k+1),
c    &                         BET_CONST*zFlowMins(k  ,kk,l)*dzhi(k  ))
c    &  -0.25d0*(1.0d0+FIS_CONST)*minmod(zFlowMins(k  ,kk,l)*dzhi(k  ),
c    &                         BET_CONST*zFlowMins(k+1,kk,l)*dzhi(k+1))
c    &  +0.25d0*(1.0d0+FIS_CONST)*minmod(zFlowPlus(k  ,kk,l)*dzhi(k  ),
c    &                         BET_CONST*zFlowPlus(k-1,kk,l)*dzhi(k-1))
c    &  +0.25d0*(1.0d0-FIS_CONST)*minmod(zFlowPlus(k-1,kk,l)*dzhi(k-1),
c    &                         BET_CONST*zFlowPlus(k  ,kk,l)*dzhi(k  )))

               values(l) = +dzh(k)*(
     &                   +const_1*minmod(zFlowMins(k+1,kk,l)*dzhi(k+1),
     &                         BET_CONST*zFlowMins(k  ,kk,l)*dzhi(k  ))
     &                   +const_3*minmod(zFlowPlus(k  ,kk,l)*dzhi(k  ),
     &                         BET_CONST*zFlowPlus(k-1,kk,l)*dzhi(k-1))
     &                   +const_2*minmod(zFlowMins(k  ,kk,l)*dzhi(k  ),
     &                         BET_CONST*zFlowMins(k+1,kk,l)*dzhi(k+1))
     &                   +const_4*minmod(zFlowPlus(k-1,kk,l)*dzhi(k-1),
     &                         BET_CONST*zFlowPlus(k  ,kk,l)*dzhi(k  )))
            enddo

            do m=1,4
            do l=1,4
               if (dabs(values(l+1)).gt.dabs(values(l))) then
                  temporary = values(l)
                  values(l) = values(l+1)
                  values(l+1) = temporary
               endif
            enddo
            enddo

            do l=1,5
               zFlow(k,kk) = zFlow(k,kk) + values(l)
            enddo

            enddo
         enddo

      end subroutine zFlowCalculation

! ----------------------------------------------------------------------
! --- The best ever known program --------------------------------------
! ----------------------------------------------------------------------
      
      subroutine calcWaveFront

         implicit none

         include "mpif.h"

! ----------------------------------------------------------------------

         common /localCord/     x,     y,     z
         common /globCoord/ xGlob, yGlob, zGlob

         common /chiefData/ dens, velx, vely, velz,
     &                      pres, enth, srho, wave

         common /procStuff/ myRank,
     &                         xRank, yRank,
     &                         xOfset,yOfset,
     &                      xLeftBlock,xRghtBlock,
     &                      yLeftBlock,yRghtBlock,
     &                         ialgn,timeStep,courantCoef,
     &                      imin1,jmin1,imax1,jmax1,
     &                      imin2,imax2,jmin2,jmax2,
     &                         imax,jmax,kmax,
     &                      ialgn2,errorFlag

         common /windParam/ windDensAmbient, windVelsAmbient, windTempAmbient

         common /xyBorders/ xMinBorder, xMaxBorder,
     &                      yMinBorder, yMaxBorder

! ----------------------------------------------------------------------

         real(8) :: initWindDens,initWindVelx,initWindVely,initWindVelz

! ----------------------------------------------------------------------

         real(8) ::     x(X_BLOCK),     y(Y_BLOCK),     z(Z_BLOCK)
         real(8) :: xGlob(X_TOTAL), yGlob(Y_TOTAL), zGlob(Z_TOTAL)

         real(8) :: dens(X_BLOCK,Y_BLOCK,Z_BLOCK)
         real(8) :: velx(X_BLOCK,Y_BLOCK,Z_BLOCK)
         real(8) :: vely(X_BLOCK,Y_BLOCK,Z_BLOCK)
         real(8) :: velz(X_BLOCK,Y_BLOCK,Z_BLOCK)
         real(8) :: pres(X_BLOCK,Y_BLOCK,Z_BLOCK)
         real(8) :: enth(X_BLOCK,Y_BLOCK,Z_BLOCK)
         real(8) :: srho(X_BLOCK,Y_BLOCK,Z_BLOCK)
         real(8) :: wave(X_BLOCK,Y_BLOCK,Z_BLOCK)

         integer(4) :: myRank
         integer(4) :: xRank,  yRank
         integer(4) :: xOfset, yOfset
         integer(4) :: xLeftBlock, xRghtBlock
         integer(4) :: yLeftBlock, yRghtBlock

         integer(4) :: imin1, jmin1, imax1, jmax1
         integer(4) :: imin2, jmin2, imax2, jmax2

         integer(4) :: imax, jmax, kmax

         integer(4) :: ialgn, ialgn2

         real(8) :: timeStep, courantCoef

         real(8) :: errorFlag

         real(8) :: windDensAmbient, windVelsAmbient, windTempAmbient

         real(8) :: xMinBorder 
         real(8) :: xMaxBorder 
         real(8) :: yMinBorder 
         real(8) :: yMaxBorder 

! ----------------------------------------------------------------------

         real(8) :: waveBuffer(Y_BLOCK,Z_BLOCK)

         real(8) :: paramRight, paramLeft, paramLimiter
         real(8) :: rh0 !!! delete

         integer(4) :: iresc, ierr
         integer(4) :: istats(MPI_STATUS_SIZE)

         integer(4) :: i, j, k

! ----------------------------------------------------------------------

         paramLimiter = 0.22

         if (xRank.eq.0) then
            do j=1,Y_BLOCK
            do k=1,Z_BLOCK
               wave(1,j,k) = 0.0
            enddo
            enddo
         else
            call MPI_RECV(waveBuffer,Y_BLOCK*Z_BLOCK,MPI_DOUBLE_PRECISION,
     &                 xLeftBlock,909,MPI_COMM_WORLD,istats,ierr)
            do j=1,Y_BLOCK
            do k=1,Z_BLOCK
               wave(1,j,k) = waveBuffer(j,k)
            enddo
            enddo
         endif
        
         do i=2,X_BLOCK
         do j=1,Y_BLOCK
         do k=1,Z_BLOCK
!           if (sqrt(x(i)**2+y(j)**2).lt.
!    &         1.1*sqrt(xMinBorder**2+yMaxBorder**2)) then
!  
!              wave(i,j,k) = 0
!  
!           else

!              paramLeft = (velx(i-1,j,k)**2 +
!    &                      vely(i-1,j,k)**2 +
!    &                      velz(i-1,j,k)**2)**4
               paramLeft = (pres(i-1,j,k)/dens(i-1,j,k))
     &                    *(velx(i-1,j,k)**2 + 
     &                      vely(i-1,j,k)**2 + 
     &                      velz(i-1,j,k)**2)

!              paramRight = (velx(i,j,k)**2 +
!    &                       vely(i,j,k)**2 +
!    &                       velz(i,j,k)**2)**4

               paramRight = (pres(i,j,k)/dens(i,j,k))
     &                     *(velx(i,j,k)**2 + 
     &                       vely(i,j,k)**2 + 
     &                       velz(i,j,k)**2)
               
               if ((dabs(paramRight-paramLeft)/
     &                              paramRight).gt.paramLimiter) then
                  wave(i,j,k) = 10.0
               else
                  wave(i,j,k) = wave(i-1,j,k)
               endif

!           endif
         enddo
         enddo
         enddo
         
         if (xRank.lt.X_PROC-1) then
            do j=1,Y_BLOCK
            do k=1,Z_BLOCK
               waveBuffer(j,k) = wave(X_BLOCK,j,k)
            enddo
            enddo
            call MPI_SEND(waveBuffer,Y_BLOCK*Z_BLOCK,MPI_DOUBLE_PRECISION,
     &                 xRghtBlock,909,MPI_COMM_WORLD,ierr)
         endif
 
c        do i=1,X_BLOCK
c        do j=1,Y_BLOCK
c        do k=1,Z_BLOCK
c           wave(i,j,k) = 0.0
c        enddo
c        enddo
c        enddo
      
      end subroutine calcWaveFront

! ----------------------------------------------------------------------
! --- The best ever known program --------------------------------------
! ----------------------------------------------------------------------

      subroutine exchangeBoundCells

         implicit none
       
         include "mpif.h"

! ----------------------------------------------------------------------

         common /chiefData/ dens, velx, vely, velz,
     &                      pres, enth, srho, wave
         
         common /dirtyData/ dens_1, velx_1, vely_1, velz_1, pres_1
         
         common /procStuff/ myRank,
     &                         xRank, yRank,
     &                         xOfset,yOfset,
     &                      xLeftBlock,xRghtBlock,
     &                      yLeftBlock,yRghtBlock,
     &                         ialgn,timeStep,courantCoef,
     &                      imin1,jmin1,imax1,jmax1,
     &                      imin2,imax2,jmin2,jmax2,
     &                         imax,jmax,kmax,
     &                      ialgn2,errorFlag

! ----------------------------------------------------------------------

         real(8) :: dens(X_BLOCK,Y_BLOCK,Z_BLOCK)
         real(8) :: velx(X_BLOCK,Y_BLOCK,Z_BLOCK)
         real(8) :: vely(X_BLOCK,Y_BLOCK,Z_BLOCK)
         real(8) :: velz(X_BLOCK,Y_BLOCK,Z_BLOCK)
         real(8) :: pres(X_BLOCK,Y_BLOCK,Z_BLOCK)
         real(8) :: enth(X_BLOCK,Y_BLOCK,Z_BLOCK)
         real(8) :: srho(X_BLOCK,Y_BLOCK,Z_BLOCK)
         real(8) :: wave(X_BLOCK,Y_BLOCK,Z_BLOCK)
 
         real(8) :: dens_1(X_BLOCK,Y_BLOCK,Z_BLOCK)
         real(8) :: velx_1(X_BLOCK,Y_BLOCK,Z_BLOCK)
         real(8) :: vely_1(X_BLOCK,Y_BLOCK,Z_BLOCK)
         real(8) :: velz_1(X_BLOCK,Y_BLOCK,Z_BLOCK)
         real(8) :: pres_1(X_BLOCK,Y_BLOCK,Z_BLOCK)

         integer(4) :: myRank
         integer(4) :: xRank,  yRank
         integer(4) :: xOfset, yOfset
         integer(4) :: xLeftBlock, xRghtBlock
         integer(4) :: yLeftBlock, yRghtBlock

         integer(4) :: imin1, jmin1, imax1, jmax1
         integer(4) :: imin2, jmin2, imax2, jmax2

         integer(4) :: imax, jmax, kmax

         integer(4) :: ialgn, ialgn2

         real(8) :: timeStep, courantCoef

         real(8) :: errorFlag

! ----------------------------------------------------------------------

         real(8) :: sendBuffer_1(2,Y_BLOCK,Z_BLOCK,5)
         real(8) :: sendBuffer_2(2,Y_BLOCK,Z_BLOCK,5)
         real(8) :: sendBuffer_3(2,X_BLOCK,Z_BLOCK,5)
         real(8) :: sendBuffer_4(2,X_BLOCK,Z_BLOCK,5)

         real(8) :: recvBuffer_1(2,Y_BLOCK,Z_BLOCK,5)
         real(8) :: recvBuffer_2(2,Y_BLOCK,Z_BLOCK,5)
         real(8) :: recvBuffer_3(2,X_BLOCK,Z_BLOCK,5)
         real(8) :: recvBuffer_4(2,X_BLOCK,Z_BLOCK,5)

         integer(4) :: iresc(20),istats(20)

         integer(4) :: inr, ierr

         integer(4) :: i,j,k
         integer(4) :: xBoundType, yBoundType, zBoundType
         integer(4) :: xBoundCell, yBoundCell, zBoundCell

! ----------------------------------------------------------------------

         do i=1,X_BLOCK
         do j=1,Y_BLOCK
         do k=1,Z_BLOCK

            xBoundType = xBoundCell(i)
            yBoundType = yBoundCell(j)
            zBoundType = zBoundCell(k)

            if ((xBoundType.eq.1).or.
     &          (yBoundType.eq.1).or.
     &          (zBoundType.eq.1)) then

               dens_1(i,j,k) = dens(i,j,k)
               velx_1(i,j,k) = velx(i,j,k)
               vely_1(i,j,k) = vely(i,j,k)
               velz_1(i,j,k) = velz(i,j,k)
               pres_1(i,j,k) = pres(i,j,k)

            else

               if (xBoundType.eq.221) then
                  sendBuffer_1(1,j,k,1) = dens_1(i,j,k)
                  sendBuffer_1(1,j,k,2) = velx_1(i,j,k)
                  sendBuffer_1(1,j,k,3) = vely_1(i,j,k)
                  sendBuffer_1(1,j,k,4) = velz_1(i,j,k)
                  sendBuffer_1(1,j,k,5) = pres_1(i,j,k)
               endif
               if (xBoundType.eq.222) then
                  sendBuffer_1(2,j,k,1) = dens_1(i,j,k)
                  sendBuffer_1(2,j,k,2) = velx_1(i,j,k)
                  sendBuffer_1(2,j,k,3) = vely_1(i,j,k)
                  sendBuffer_1(2,j,k,4) = velz_1(i,j,k)
                  sendBuffer_1(2,j,k,5) = pres_1(i,j,k)
               endif
               if (xBoundType.eq.321) then
                  sendBuffer_2(2,j,k,1) = dens_1(i,j,k)
                  sendBuffer_2(2,j,k,2) = velx_1(i,j,k)
                  sendBuffer_2(2,j,k,3) = vely_1(i,j,k)
                  sendBuffer_2(2,j,k,4) = velz_1(i,j,k)
                  sendBuffer_2(2,j,k,5) = pres_1(i,j,k)
               endif
               if (xBoundType.eq.322) then
                  sendBuffer_2(1,j,k,1) = dens_1(i,j,k)
                  sendBuffer_2(1,j,k,2) = velx_1(i,j,k)
                  sendBuffer_2(1,j,k,3) = vely_1(i,j,k)
                  sendBuffer_2(1,j,k,4) = velz_1(i,j,k)
                  sendBuffer_2(1,j,k,5) = pres_1(i,j,k)
               endif
       
               if (yBoundType.eq.221) then
                  sendBuffer_3(1,i,k,1) = dens_1(i,j,k)
                  sendBuffer_3(1,i,k,2) = velx_1(i,j,k)
                  sendBuffer_3(1,i,k,3) = vely_1(i,j,k)
                  sendBuffer_3(1,i,k,4) = velz_1(i,j,k)
                  sendBuffer_3(1,i,k,5) = pres_1(i,j,k)
               endif
               if (yBoundType.eq.222) then
                  sendBuffer_3(2,i,k,1) = dens_1(i,j,k)
                  sendBuffer_3(2,i,k,2) = velx_1(i,j,k)
                  sendBuffer_3(2,i,k,3) = vely_1(i,j,k)
                  sendBuffer_3(2,i,k,4) = velz_1(i,j,k)
                  sendBuffer_3(2,i,k,5) = pres_1(i,j,k)
               endif
               if (yBoundType.eq.321) then
                  sendBuffer_4(2,i,k,1) = dens_1(i,j,k)
                  sendBuffer_4(2,i,k,2) = velx_1(i,j,k)
                  sendBuffer_4(2,i,k,3) = vely_1(i,j,k)
                  sendBuffer_4(2,i,k,4) = velz_1(i,j,k)
                  sendBuffer_4(2,i,k,5) = pres_1(i,j,k)
               endif
               if (yBoundType.eq.322) then
                  sendBuffer_4(1,i,k,1) = dens_1(i,j,k)
                  sendBuffer_4(1,i,k,2) = velx_1(i,j,k)
                  sendBuffer_4(1,i,k,3) = vely_1(i,j,k)
                  sendBuffer_4(1,i,k,4) = velz_1(i,j,k)
                  sendBuffer_4(1,i,k,5) = pres_1(i,j,k)
               endif
            endif
         enddo
         enddo
         enddo
       
         inr = 1
       
         if (xRank.gt.0) then
            call MPI_ISEND(sendBuffer_1,Y_BLOCK*Z_BLOCK*5*2,MPI_DOUBLE_PRECISION,
     &               xLeftBlock,308,MPI_COMM_WORLD,istats(inr),ierr)
            inr = inr+1
            call MPI_IRECV(recvBuffer_1,Y_BLOCK*Z_BLOCK*5*2,MPI_DOUBLE_PRECISION,
     &               xLeftBlock,309,MPI_COMM_WORLD,istats(inr),ierr)
            inr = inr+1
         endif

         if (xRank.lt.X_PROC-1) then
            call MPI_ISEND(sendBuffer_2,Y_BLOCK*Z_BLOCK*5*2,MPI_DOUBLE_PRECISION,
     &               xRghtBlock,309,MPI_COMM_WORLD,istats(inr),ierr)
            inr = inr+1
            call MPI_IRECV(recvBuffer_2,Y_BLOCK*Z_BLOCK*5*2,MPI_DOUBLE_PRECISION,
     &               xRghtBlock,308,MPI_COMM_WORLD,istats(inr),ierr)
            inr = inr+1
         endif

         if (yRank.gt.0) then
            call MPI_ISEND(sendBuffer_3,X_BLOCK*Z_BLOCK*5*2,MPI_DOUBLE_PRECISION,
     &               yLeftBlock,408,MPI_COMM_WORLD,istats(inr),ierr)
            inr = inr+1
            call MPI_IRECV(recvBuffer_3,X_BLOCK*Z_BLOCK*5*2,MPI_DOUBLE_PRECISION,
     &               yLeftBlock,409,MPI_COMM_WORLD,istats(inr),ierr)
            inr = inr+1
         endif

         if (yRank.lt.Y_PROC-1) then
            call MPI_ISEND(sendBuffer_4,X_BLOCK*Z_BLOCK*5*2,MPI_DOUBLE_PRECISION,
     &            yRghtBlock,409,MPI_COMM_WORLD,istats(inr),ierr)
            inr = inr+1
            call MPI_IRECV(recvBuffer_4,X_BLOCK*Z_BLOCK*5*2,MPI_DOUBLE_PRECISION,
     &            yRghtBlock,408,MPI_COMM_WORLD,istats(inr),ierr)
            inr = inr+1
         endif

         call calcTimeStep(dens_1,velx_1,vely_1,velz_1,pres_1)
      
c        if (inr.gt.1) call MPI_WAITALL(inr-1,istats,iresc,ierr)
         if (inr.gt.1) call MPI_WAITALL(inr-1,istats,iresc,ierr)

         if (errorFlag.eq.0.) then
       
            do i=1,X_BLOCK
            do j=1,Y_BLOCK
            do k=1,Z_BLOCK

               xBoundType = xBoundCell(i)
               yBoundType = yBoundCell(j)
               zBoundType = zBoundCell(k)

               if ((xBoundType.eq.1).or.
     &             (yBoundType.eq.1).or.
     &             (zBoundType.eq.1)) then
               else

                  if (xBoundType.eq.211) then
                     dens_1(i,j,k) = recvBuffer_1(1,j,k,1)
                     velx_1(i,j,k) = recvBuffer_1(1,j,k,2)
                     vely_1(i,j,k) = recvBuffer_1(1,j,k,3)
                     velz_1(i,j,k) = recvBuffer_1(1,j,k,4)
                     pres_1(i,j,k) = recvBuffer_1(1,j,k,5)
                  endif
                  if (xBoundType.eq.212) then
                     dens_1(i,j,k) = recvBuffer_1(2,j,k,1)
                     velx_1(i,j,k) = recvBuffer_1(2,j,k,2)
                     vely_1(i,j,k) = recvBuffer_1(2,j,k,3)
                     velz_1(i,j,k) = recvBuffer_1(2,j,k,4)
                     pres_1(i,j,k) = recvBuffer_1(2,j,k,5)
                  endif
                  if (xBoundType.eq.311) then
                     dens_1(i,j,k) = recvBuffer_2(2,j,k,1)
                     velx_1(i,j,k) = recvBuffer_2(2,j,k,2)
                     vely_1(i,j,k) = recvBuffer_2(2,j,k,3)
                     velz_1(i,j,k) = recvBuffer_2(2,j,k,4)
                     pres_1(i,j,k) = recvBuffer_2(2,j,k,5)
                  endif
                  if (xBoundType.eq.312) then
                     dens_1(i,j,k) = recvBuffer_2(1,j,k,1)
                     velx_1(i,j,k) = recvBuffer_2(1,j,k,2)
                     vely_1(i,j,k) = recvBuffer_2(1,j,k,3)
                     velz_1(i,j,k) = recvBuffer_2(1,j,k,4)
                     pres_1(i,j,k) = recvBuffer_2(1,j,k,5)
                  endif
       
                  if (yBoundType.eq.211) then
                     dens_1(i,j,k) = recvBuffer_3(1,i,k,1)
                     velx_1(i,j,k) = recvBuffer_3(1,i,k,2)
                     vely_1(i,j,k) = recvBuffer_3(1,i,k,3)
                     velz_1(i,j,k) = recvBuffer_3(1,i,k,4)
                     pres_1(i,j,k) = recvBuffer_3(1,i,k,5)
                  endif
                  if (yBoundType.eq.212) then
                     dens_1(i,j,k) = recvBuffer_3(2,i,k,1)
                     velx_1(i,j,k) = recvBuffer_3(2,i,k,2)
                     vely_1(i,j,k) = recvBuffer_3(2,i,k,3)
                     velz_1(i,j,k) = recvBuffer_3(2,i,k,4)
                     pres_1(i,j,k) = recvBuffer_3(2,i,k,5)
                  endif
                  if (yBoundType.eq.311) then
                     dens_1(i,j,k) = recvBuffer_4(2,i,k,1)
                     velx_1(i,j,k) = recvBuffer_4(2,i,k,2)
                     vely_1(i,j,k) = recvBuffer_4(2,i,k,3)
                     velz_1(i,j,k) = recvBuffer_4(2,i,k,4)
                     pres_1(i,j,k) = recvBuffer_4(2,i,k,5)
                  endif
                  if (yBoundType.eq.312) then
                     dens_1(i,j,k) = recvBuffer_4(1,i,k,1)
                     velx_1(i,j,k) = recvBuffer_4(1,i,k,2)
                     vely_1(i,j,k) = recvBuffer_4(1,i,k,3)
                     velz_1(i,j,k) = recvBuffer_4(1,i,k,4)
                     pres_1(i,j,k) = recvBuffer_4(1,i,k,5)
                  endif

               endif
            enddo
            enddo
            enddo
       
         endif

      end subroutine exchangeBoundCells

! ----------------------------------------------------------------------
! --- The best ever known program --------------------------------------
! ----------------------------------------------------------------------

      function xRocheShort (x,y,z)

         implicit none

         common /physParam/ prop, gravMass, plntOrbit, omega, innrRadius
         real(8) :: prop, gravMass, plntOrbit, omega, innrRadius

         real(8) :: xRocheShort, x, y, z

         xRocheShort = -omega**2*(x-(1.d0-prop)*plntOrbit) !!! rewrite
    
      end function xRocheShort

      function xRoche (x,y,z)

         implicit none

         common /physParam/ prop, gravMass, plntOrbit, omega, innrRadius
         real(8) :: prop, gravMass, plntOrbit, omega, innrRadius

         real(8) :: xRoche, x, y ,z

         xRoche =
     &           + (1.d0-prop)*gravMass*(x-plntOrbit)/                   !!! rewrite
     &                                 ((x-plntOrbit)**2+y**2+z**2)**1.5 !!! rewrite

      end function xRoche

      function yRocheShort (x,y,z)

         implicit none

         common /physParam/ prop, gravMass, plntOrbit, omega, innrRadius
         real(8) :: prop, gravMass, plntOrbit, omega, innrRadius

         real(8) :: yRocheShort, x, y, z

         yRocheShort = 0.0

      end function yRocheShort

      function yRoche (x,y,z)

         implicit none

         common /physParam/ prop, gravMass, plntOrbit, omega, innrRadius
         real(8) :: prop, gravMass, plntOrbit, omega, innrRadius
 
         real(8) :: yRoche, x, y, z

         yRoche = 
     &           + (1.d0-prop)*gravMass*y/                            !!! rewrite
     &                              ((x-plntOrbit)**2+y**2+z**2)**1.5 !!! rewrite

      end function yRoche

      function zRocheShort (x,y,z)

         implicit none

         common /physParam/ prop, gravMass, plntOrbit, omega, innrRadius
         real(8) :: prop, gravMass, plntOrbit, omega, innrRadius
  
         real(8) :: zRocheShort, x, y, z

         zRocheShort = 0.0

      end function zRocheShort

! ----------------------------------------------------------------------
! --- The best ever known program --------------------------------------
! ----------------------------------------------------------------------

      function zRoche (x,y,z)

         implicit none

         common /physParam/ prop, gravMass, plntOrbit, omega, innrRadius
         real(8) :: prop, gravMass, plntOrbit, omega, innrRadius
         real(8) :: zRoche, x,y,z

         zRoche = 
     &            +(1.d0-prop)*gravMass*z/                    !!! rewrite
     &                      ((x-plntOrbit)**2+y**2+z**2)**1.5 !!! rewrite

      end function zRoche

! ----------------------------------------------------------------------
! --- The best ever known program --------------------------------------
! ----------------------------------------------------------------------

      subroutine calcSqrtDens

         implicit none

! ----------------------------------------------------------------------
   
         common /chiefData/ dens, velx, vely, velz,
     &                      pres, enth, srho, wave

! ----------------------------------------------------------------------

         real(8) :: dens(X_BLOCK,Y_BLOCK,Z_BLOCK)
         real(8) :: velx(X_BLOCK,Y_BLOCK,Z_BLOCK)
         real(8) :: vely(X_BLOCK,Y_BLOCK,Z_BLOCK)
         real(8) :: velz(X_BLOCK,Y_BLOCK,Z_BLOCK)
         real(8) :: pres(X_BLOCK,Y_BLOCK,Z_BLOCK)
         real(8) :: enth(X_BLOCK,Y_BLOCK,Z_BLOCK)
         real(8) :: srho(X_BLOCK,Y_BLOCK,Z_BLOCK)
         real(8) :: wave(X_BLOCK,Y_BLOCK,Z_BLOCK)

         integer(4) :: i, j, k

! ----------------------------------------------------------------------

         do i=1,X_BLOCK
         do j=1,Y_BLOCK
         do k=1,Z_BLOCK
            srho(i,j,k) = dsqrt(dens(i,j,k))
         enddo
         enddo
         enddo

! ----------------------------------------------------------------------

      end subroutine calcSqrtDens

! ----------------------------------------------------------------------
! --- The best ever known program --------------------------------------
! ----------------------------------------------------------------------

      function calcEnthalpy(i,j,k)

         implicit none

! ----------------------------------------------------------------------

         real(8) :: GAMMA_CONSTREAL, gam1 !!! delete
   
         parameter (GAMMA_CONSTREAL=5.0/3.0, gam1=GAMMA_CONSTREAL/(GAMMA_CONSTREAL-1.)) !!! delete

! ----------------------------------------------------------------------

         common /chiefData/ dens, velx, vely, velz,
     &                      pres, enth, srho, wave

! ----------------------------------------------------------------------

         real(8) :: dens(X_BLOCK,Y_BLOCK,Z_BLOCK)
         real(8) :: velx(X_BLOCK,Y_BLOCK,Z_BLOCK)
         real(8) :: vely(X_BLOCK,Y_BLOCK,Z_BLOCK)
         real(8) :: velz(X_BLOCK,Y_BLOCK,Z_BLOCK)
         real(8) :: pres(X_BLOCK,Y_BLOCK,Z_BLOCK)
         real(8) :: enth(X_BLOCK,Y_BLOCK,Z_BLOCK)
         real(8) :: srho(X_BLOCK,Y_BLOCK,Z_BLOCK)
         real(8) :: wave(X_BLOCK,Y_BLOCK,Z_BLOCK)

         real(8) :: calcEnthalpy
         real(8) :: gammaModified

         integer(4) :: i,j,k

! ----------------------------------------------------------------------

         gammaModified = GAMMA_CONST/(GAMMA_CONST - 1.0)

         calcEnthalpy = gam1*pres(i,j,k)/dens(i,j,k) !!! variable : gav1 -> gammaModified
     &                             + (velx(i,j,k)**2 + 
     &                                vely(i,j,k)**2 +
     &                                velz(i,j,k)**2)*0.5d0

! ----------------------------------------------------------------------

      end function calcEnthalpy

! ----------------------------------------------------------------------
! --- The best ever known program --------------------------------------
! ----------------------------------------------------------------------

      subroutine reflect

         implicit none

! ----------------------------------------------------------------------

         common /chiefData/ dens, velx, vely, velz,
     &                      pres, enth, srho, wave

! ----------------------------------------------------------------------

         real(8) :: dens(X_BLOCK,Y_BLOCK,Z_BLOCK)
         real(8) :: velx(X_BLOCK,Y_BLOCK,Z_BLOCK)
         real(8) :: vely(X_BLOCK,Y_BLOCK,Z_BLOCK)
         real(8) :: velz(X_BLOCK,Y_BLOCK,Z_BLOCK)
         real(8) :: pres(X_BLOCK,Y_BLOCK,Z_BLOCK)
         real(8) :: enth(X_BLOCK,Y_BLOCK,Z_BLOCK)
         real(8) :: srho(X_BLOCK,Y_BLOCK,Z_BLOCK)
         real(8) :: wave(X_BLOCK,Y_BLOCK,Z_BLOCK)

         integer(4) :: i, j

! ----------------------------------------------------------------------
   
         do i=1,X_BLOCK
         do j=1,Y_BLOCK

            dens(i,j,1) =  dens(i,j,3)
            velx(i,j,1) =  velx(i,j,3)
            vely(i,j,1) =  vely(i,j,3)
            velz(i,j,1) = -velz(i,j,3)
            pres(i,j,1) =  pres(i,j,3)
            enth(i,j,1) =  enth(i,j,3)
            wave(i,j,1) =  wave(i,j,3)

         enddo
         enddo

! ----------------------------------------------------------------------

      end subroutine reflect

! ----------------------------------------------------------------------
! --- The best ever known program --------------------------------------
! ----------------------------------------------------------------------

      function minmod (x,y)

         implicit none

         real(8) :: minmod, x, y 

         if (x*y.le.0.0d0) then
            minmod = 0.0d0
         else
            if (x.lt.0.0d0) then
               minmod = dmax1(x,y)
            else
               minmod = dmin1(x,y)
            endif
         endif

      end function minmod

! ----------------------------------------------------------------------
! --- The best ever known program --------------------------------------
! ----------------------------------------------------------------------

      function xBoundCell (i)

         implicit none

! ----------------------------------------------------------------------
   
         common /procStuff/ myRank,
     &                         xRank, yRank,
     &                         xOfset,yOfset,
     &                      xLeftBlock,xRghtBlock,
     &                      yLeftBlock,yRghtBlock,
     &                         ialgn,timeStep,courantCoef,
     &                      imin1,jmin1,imax1,jmax1,
     &                      imin2,imax2,jmin2,jmax2,
     &                         imax,jmax,kmax,
     &                      ialgn2,errorFlag

! ----------------------------------------------------------------------

         integer(4) :: myRank
         integer(4) :: xRank,  yRank
         integer(4) :: xOfset, yOfset
         integer(4) :: xLeftBlock, xRghtBlock
         integer(4) :: yLeftBlock, yRghtBlock

         integer(4) :: imin1, jmin1, imax1, jmax1
         integer(4) :: imin2, jmin2, imax2, jmax2

         integer(4) :: imax, jmax, kmax

         integer(4) :: ialgn, ialgn2

         real(8) :: timeStep, courantCoef

         real(8) :: errorFlag

! ----------------------------------------------------------------------

         integer(4) :: xBoundCell, i

! ----------------------------------------------------------------------

         xBoundCell = 0

         if (i.eq.1.and.xRank.eq.0)              xBoundCell = 1
         if (i.eq.X_BLOCK.and.xRank.eq.X_PROC-1) xBoundCell = 1

         if (i.eq.1.and.xRank.ne.0)                xBoundCell = 211
         if (i.eq.2.and.xRank.ne.0)                xBoundCell = 212
         if (i.eq.3.and.xRank.ne.0)                xBoundCell = 221
         if (i.eq.4.and.xRank.ne.0)                xBoundCell = 222

         if (i.eq.X_BLOCK  .and.xRank.ne.X_PROC-1) xBoundCell = 311
         if (i.eq.X_BLOCK-1.and.xRank.ne.X_PROC-1) xBoundCell = 312
         if (i.eq.X_BLOCK-2.and.xRank.ne.X_PROC-1) xBoundCell = 321
         if (i.eq.X_BLOCK-3.and.xRank.ne.X_PROC-1) xBoundCell = 322

! ----------------------------------------------------------------------

      end function xBoundCell

! ----------------------------------------------------------------------
! --- The best ever known program --------------------------------------
! ----------------------------------------------------------------------

      function yBoundCell(j)

         implicit none

! ----------------------------------------------------------------------
   
         common /procStuff/ myRank,
     &                         xRank, yRank,
     &                         xOfset,yOfset,
     &                      xLeftBlock,xRghtBlock,
     &                      yLeftBlock,yRghtBlock,
     &                         ialgn,timeStep,courantCoef,
     &                      imin1,jmin1,imax1,jmax1,
     &                      imin2,imax2,jmin2,jmax2,
     &                         imax,jmax,kmax,
     &                      ialgn2,errorFlag

! ----------------------------------------------------------------------

         integer(4) :: myRank
         integer(4) :: xRank,  yRank
         integer(4) :: xOfset, yOfset
         integer(4) :: xLeftBlock, xRghtBlock
         integer(4) :: yLeftBlock, yRghtBlock

         integer(4) :: imin1, jmin1, imax1, jmax1
         integer(4) :: imin2, jmin2, imax2, jmax2

         integer(4) :: imax, jmax, kmax

         integer(4) :: ialgn, ialgn2

         real(8) :: timeStep, courantCoef

         real(8) :: errorFlag

! ----------------------------------------------------------------------

         integer(4) :: yBoundCell, j

! ----------------------------------------------------------------------

         yBoundCell = 0

         if (j.eq.1      .and.yRank.eq.0)        yBoundCell = 1
         if (j.eq.Y_BLOCK.and.yRank.eq.Y_PROC-1) yBoundCell = 1

         if (j.eq.1.and.yRank.ne.0)                yBoundCell = 211
         if (j.eq.2.and.yRank.ne.0)                yBoundCell = 212
         if (j.eq.3.and.yRank.ne.0)                yBoundCell = 221
         if (j.eq.4.and.yRank.ne.0)                yBoundCell = 222

         if (j.eq.Y_BLOCK  .and.yRank.ne.Y_PROC-1) yBoundCell = 311
         if (j.eq.Y_BLOCK-1.and.yRank.ne.Y_PROC-1) yBoundCell = 312
         if (j.eq.Y_BLOCK-2.and.yRank.ne.Y_PROC-1) yBoundCell = 321
         if (j.eq.Y_BLOCK-3.and.yRank.ne.Y_PROC-1) yBoundCell = 322

! ----------------------------------------------------------------------

      end function yBoundCell

! ----------------------------------------------------------------------
! --- The best ever known program --------------------------------------
! ----------------------------------------------------------------------

      function zBoundCell(k)

         implicit none
   
! ----------------------------------------------------------------------
   
         common /procStuff/ myRank,
     &                         xRank, yRank,
     &                         xOfset,yOfset,
     &                      xLeftBlock,xRghtBlock,
     &                      yLeftBlock,yRghtBlock,
     &                         ialgn,timeStep,courantCoef,
     &                      imin1,jmin1,imax1,jmax1,
     &                      imin2,imax2,jmin2,jmax2,
     &                         imax,jmax,kmax,
     &                      ialgn2,errorFlag

! ----------------------------------------------------------------------

         integer(4) :: myRank
         integer(4) :: xRank,  yRank
         integer(4) :: xOfset, yOfset
         integer(4) :: xLeftBlock, xRghtBlock
         integer(4) :: yLeftBlock, yRghtBlock

         integer(4) :: imin1, jmin1, imax1, jmax1
         integer(4) :: imin2, jmin2, imax2, jmax2

         integer(4) :: imax, jmax, kmax

         integer(4) :: ialgn, ialgn2

         real(8) :: timeStep, courantCoef

         real(8) :: errorFlag

! ----------------------------------------------------------------------

         integer(4) :: zBoundCell, k

! ----------------------------------------------------------------------

         zBoundCell = 0

         if (k.eq.1)       zBoundCell = 1
         if (k.eq.Z_BLOCK) zBoundCell = 1

! ----------------------------------------------------------------------

      end function zBoundCell

! ----------------------------------------------------------------------
! --- The best ever known program --------------------------------------
! ----------------------------------------------------------------------
      
      function initWindDens(windDens,xx,yy,zz)

         implicit none

! ----------------------------------------------------------------------

         common /physParam/ prop,gravMass,plntOrbit,omega,innrRadius

         real(8) :: prop, gravMass, plntOrbit, omega, innrRadius

! ----------------------------------------------------------------------

         real(8) :: initWindDens, windDens, xx, yy, zz
         real(8) :: starDist

! ----------------------------------------------------------------------

         starDist = dsqrt(xx**2+yy**2+zz**2)
         initWindDens = windDens/((starDist/plntOrbit)**2)

! ----------------------------------------------------------------------

      end function initWindDens

! ----------------------------------------------------------------------
! --- The best ever known program --------------------------------------
! ----------------------------------------------------------------------

      function initWindVelx(windVels,xx,yy,zz)

         implicit none
       
! ----------------------------------------------------------------------

         common /physParam/ prop,gravMass,plntOrbit,omega,innrRadius

         real(8) :: prop, gravMass, plntOrbit, omega, innrRadius

! ----------------------------------------------------------------------

         real(8) :: initWindVelx, windVels, xx, yy, zz
         real(8) :: starDist

! ----------------------------------------------------------------------

         starDist = dsqrt(xx**2+yy**2+zz**2)
         initWindVelx = windVels*xx/starDist + omega*yy

      end function initWindVelx

! ----------------------------------------------------------------------
! --- The best ever known program --------------------------------------
! ----------------------------------------------------------------------

      function initWindVely(windVels,xx,yy,zz)

         implicit none

! ----------------------------------------------------------------------

         common /physParam/ prop,gravMass,plntOrbit,omega,innrRadius

         real(8) :: prop, gravMass, plntOrbit, omega, innrRadius

! ----------------------------------------------------------------------

         real(8) :: initWindVely, windVels, xx, yy, zz
         real(8) :: starDist

! ----------------------------------------------------------------------

         starDist = dsqrt(xx**2+yy**2+zz**2)
         initWindVely = windVels*yy/starDist - omega*xx

! ----------------------------------------------------------------------

      end function initWindVely

! ----------------------------------------------------------------------
! --- The best ever known program --------------------------------------
! ----------------------------------------------------------------------

      function initWindVelz (windVels,xx,yy,zz)

         implicit none
       
! ----------------------------------------------------------------------

         common /physParam/ prop,gravMass,plntOrbit,omega,innrRadius

         real(8) :: prop, gravMass, plntOrbit, omega, innrRadius

! ----------------------------------------------------------------------

         real(8) :: initWindVelz, windVels, xx, yy, zz
         real(8) :: starDist

! ----------------------------------------------------------------------

         starDist = dsqrt(xx**2+yy**2+zz**2)

         initWindVelz = windVels*zz/starDist

! ----------------------------------------------------------------------

      end function initWindVelz

! ----------------------------------------------------------------------
! --- The best ever known program --------------------------------------
! ----------------------------------------------------------------------

      subroutine drawPlot

         implicit none
       
         include "mpif.h"
         
! ----------------------------------------------------------------------
         
         common /localCord/ x, y, z

         common /globCoord/ xGlob, yGlob, zGlob

         common /chiefData/ dens, velx, vely, velz,
     &                      pres, enth, srho, wave

         common /procStuff/ myRank,
     &                         xRank, yRank,
     &                         xOfset,yOfset,
     &                      xLeftBlock,xRghtBlock,
     &                      yLeftBlock,yRghtBlock,
     &                         ialgn,timeStep,courantCoef,
     &                      imin1,jmin1,imax1,jmax1,
     &                      imin2,imax2,jmin2,jmax2,
     &                         imax,jmax,kmax,
     &                      ialgn2,errorFlag

         common /physParam/ prop, gravMass, plntOrbit, omega, innrRadius

         common /plntParam/ plntRadius, lagrangePoint_1,
     &                                  lagrangePoint_2,
     &                                  lagrangePoint_3

         common /windParam/ windDensAmbient,windVelsAmbient,windTempAmbient

         common /someCount/ fileNumber

         common /timeParam/ time, period

! ----------------------------------------------------------------------

         real(8) :: fullRochePotential

! ----------------------------------------------------------------------

         real(8) ::     x(X_BLOCK),     y(Y_BLOCK),     z(Z_BLOCK)
         real(8) :: xGlob(X_TOTAL), yGlob(Y_TOTAL), zGlob(Z_TOTAL)

         real(8) :: dens(X_BLOCK,Y_BLOCK,Z_BLOCK)
         real(8) :: velx(X_BLOCK,Y_BLOCK,Z_BLOCK)
         real(8) :: vely(X_BLOCK,Y_BLOCK,Z_BLOCK)
         real(8) :: velz(X_BLOCK,Y_BLOCK,Z_BLOCK)
         real(8) :: pres(X_BLOCK,Y_BLOCK,Z_BLOCK)
         real(8) :: enth(X_BLOCK,Y_BLOCK,Z_BLOCK)
         real(8) :: srho(X_BLOCK,Y_BLOCK,Z_BLOCK)
         real(8) :: wave(X_BLOCK,Y_BLOCK,Z_BLOCK)

         integer(4) :: myRank
         integer(4) :: xRank,  yRank
         integer(4) :: xOfset, yOfset
         integer(4) :: xLeftBlock, xRghtBlock
         integer(4) :: yLeftBlock, yRghtBlock

         integer(4) :: imin1, jmin1, imax1, jmax1
         integer(4) :: imin2, jmin2, imax2, jmax2

         integer(4) :: imax, jmax, kmax

         integer(4) :: ialgn, ialgn2

         real(8) :: timeStep, courantCoef

         real(8) :: errorFlag

         real(8) :: prop, gravMass, plntOrbit, omega, innrRadius

         real(8) :: plntRadius

         real(8) :: lagrangePoint_1
         real(8) :: lagrangePoint_2
         real(8) :: lagrangePoint_3
         
         real(8) :: windDensAmbient,windVelsAmbient,windTempAmbient

         real(8) :: time, period

         integer(4) :: fileNumber

! ----------------------------------------------------------------------

         integer(4) :: numberContour

         integer(4) :: xNumberVector
         integer(4) :: yNumberVector
         integer(4) :: zNumberVector
       
         real(4) :: buffer_xy(SLICE_SIZE_XY*4+2)
         real(4) :: buffer_xz(SLICE_SIZE_XZ*4)

         real(4) :: dens_xy(X_DRAW,Y_DRAW)
         real(4) :: wave_xy(X_DRAW,Y_DRAW)
         real(4) :: velx_xy(X_DRAW,Y_DRAW)
         real(4) :: vely_xy(X_DRAW,Y_DRAW)
         real(4) :: roch_xy(X_DRAW,Y_DRAW)

         real(4) :: dens_xz(X_DRAW,Y_DRAW)
         real(4) :: wave_xz(X_DRAW,Y_DRAW)
         real(4) :: velx_xz(X_DRAW,Y_DRAW)
         real(4) :: velz_xz(X_DRAW,Y_DRAW)
         real(4) :: roch_xz(X_DRAW,Y_DRAW)
         
         real(4) :: xDraw(X_TOTAL)
         real(4) :: yDraw(Y_TOTAL)
         real(4) :: zDraw(Z_TOTAL)

         integer(4) :: xCellMap(500)
         integer(4) :: yCellMap(500)
         integer(4) :: zCellMap(500)

         real(4) :: regularPoint

         integer(4) :: processor

         integer(4) :: xProcOfset
         integer(4) :: yProcOfset
 
         integer(4) :: ierr, istats(MPI_STATUS_SIZE)
         
         real(4) :: x0,y0,z0
         real(4) :: x1,y1,z1
         real(4) :: x2,y2,z2

         real(4) :: densMin,densMax

         real(4) :: vectorLength
         real(4) :: vectorLengthFile

         real(4) :: concMin,concMax
         real(4) :: rocheLevel_1,rocheLevel_2,rocheLevel_3
         real(4) :: densLevel

         real(4) :: yStep,xStep,zStep
         real(4) :: xFirstTick,yFirstTick,zFirstTick
         real(4) :: xMin,yMin,zMin,xMax,yMax,zMax

         character(len=25) :: plotFileNameXY
         character(len=25) :: plotFileNameXZ
         character(len=29) :: plotSaveFileNameXY
         character(len=29) :: plotSaveFileNameXZ
         character(len=16) :: timeLine
         
         integer(4) :: i,j,k
         integer(4) :: zSlice
         integer(4) :: ySlice, yRankCenter, specRank

! ----------------------------------------------------------------------
        
         zSlice = 1
         vectorLength = 1.0
         
         do i=1,Z_BLOCK-1
            if (z(i  ).le.0.0.and.
     &          z(i+1).gt.0.0) then
               zSlice = i
               goto 220
            endif
         enddo

220      continue

         yRankCenter = (Y_PROC/2)
         specRank = yRankCenter*X_PROC
c        print*, "Y RANKE CNETER", yRankCenter, specRank
         ySlice = 3
         
         if (myRank.eq.0) then

            do processor=1,(X_PROC*Y_PROC-1)

                call MPI_RECV(buffer_xy,(SLICE_SIZE_XY*4+2),MPI_REAL,
     &                     processor,971,MPI_COMM_WORLD,istats,ierr)
             
                xProcOfset = int(buffer_xy(1))
                yProcOfset = int(buffer_xy(2))
             
                do i=1,X_BLOCK-4
                do j=1,Y_BLOCK-4
                   dens_xy(i+xProcOfset,j+yProcOfset) = buffer_xy(2+0*SLICE_SIZE_XY+i-1+(X_BLOCK-4)*(j-1)+1)
                   velx_xy(i+xProcOfset,j+yProcOfset) = buffer_xy(2+1*SLICE_SIZE_XY+i-1+(X_BLOCK-4)*(j-1)+1)
                   vely_xy(i+xProcOfset,j+yProcOfset) = buffer_xy(2+2*SLICE_SIZE_XY+i-1+(X_BLOCK-4)*(j-1)+1)
                   wave_xy(i+xProcOfset,j+yProcOfset) = buffer_xy(2+3*SLICE_SIZE_XY+i-1+(X_BLOCK-4)*(j-1)+1)
                enddo
                enddo

            enddo

            do i=1,X_BLOCK-4
            do j=1,Y_BLOCK-4
               dens_xy(i+xOfset,j+yOfset) = dens(i+2,j+2,zSlice)
               wave_xy(i+xOfset,j+yOfset) = wave(i+2,j+2,zSlice)
               velx_xy(i+xOfset,j+yOfset) = velx(i+2,j+2,zSlice)
               vely_xy(i+xOfset,j+yOfset) = vely(i+2,j+2,zSlice)
            enddo
            enddo

! --------- Write to file all plot data

225         format ('./Plots/plotData_xy_',i5.5,'.dat')

            write (plotSaveFileNameXY,225)  fileNumber
            write (*,*) 'writing to file ', plotSaveFileNameXY
            open (5,file=plotSaveFileNameXY,form='unformatted')
               do i=1,X_DRAW
               do j=1,Y_DRAW
                  write (5) dens_xy(i,j)
                  write (5) velx_xy(i,j)
                  write (5) vely_xy(i,j)
               enddo
               enddo
            write (5) time
            close (5)

! --------- Write to file all plot data

            densMin = dens_xy(1,1)
            densMax = densMin
            
            do i=1,X_DRAW
            do j=1,Y_DRAW
               densMax = max(dens_xy(i,j),densMax) !!! variable :
               densMin = min(dens_xy(i,j),densMin) !!! variable :
            enddo
            enddo
            
            write(*,*) 'RHO MIN MAX: ', densMin, densMax
            
            open (1,file='plotParameters.dat')
               read (1,*) numberContour
               read (1,*) xNumberVector
               read (1,*) vectorLengthFile
               read (1,*) concMin
               read (1,*) concMax
            close (1)
            
            if (concMin.ge.0.0) then
               densMin = concMin*HYGE_MASS
            endif

            if (concMax.ge.0.0) then
               densMax = concMax*HYGE_MASS
            endif

            if (vectorLengthFile.ge.0.0) then
               vectorLength = vectorLengthFile
            endif
        
            do i=1,X_DRAW
               xDraw(i) = (xGlob(i+2)-plntOrbit)/plntRadius
            enddo
           
            do i=1,Y_DRAW
               yDraw(i) = yGlob(i+2)/plntRadius
            enddo
            
            do i=1,X_DRAW
            do j=1,Y_DRAW
               roch_xy(i,j) = fullRochePotential(xGlob(i+2),yGlob(j+2),0.0d0)
            enddo
            enddo
            
            xMin = xDraw(1)
            xMax = xDraw(X_DRAW)
           
            yMin = yDraw(1)
            yMax = yDraw(Y_DRAW)
            
            yNumberVector = int(xNumberVector*(yMax-yMin)/(xMax-xMin))
            
            j = 1
            do i=1,X_DRAW-1

               regularPoint = xMin +
     &                  (xMax-xMin)*(j-1.0)/(xNumberVector-1.0)

               if (xDraw(i  ).le.regularPoint.and.
     &             xDraw(i+1).ge.regularPoint.and.
     &                                j.le.500) then
                  xCellMap(j) = i
                  j = j+1

               endif

            enddo
          
            j = 1
            do i=1,Y_DRAW-1

               regularPoint = yMin +
     &                  (yMax-yMin)*(j-1.0)/(yNumberVector-1.0)

               if (yDraw(i  ).le.regularPoint.and.
     &             yDraw(i+1).ge.regularPoint.and.
     &                                j.le.500) then
                  yCellMap(j) = i
                  j = j+1

               endif

            enddo

            call page(1700+350,int(1700*(yMax-yMin)/(xMax-xMin))+300)
            call pagmod('NONE')
            call filmod('DELETE')
            CALL METAFL ('png') !!! lowercase
            
230         format ('./Plots/plot_xy_',i5.5,'.png') !!! rename plot files : ./Data/data_ -> plot_
            write (plotFileNameXY,230)  fileNumber !!! variable : plotFileName -> plotName
          
            call setfil(plotFileNameXY) !!! variable : plotFileName -> plotName
            call disini
              
            CALL PSFONT('Times-Roman')
            call nochek
            call shdmod('CELL','CONTUR')
            call digits(1,'XYZ')
            call texmod('ON')
              
            call axslen(1700,int(1700*(yMax-yMin)/(xMax-xMin)))    
            call axspos(300, int(1700*(yMax-yMin)/(xMax-xMin))+300)
           
            call name('X/Rp','X')
            call name('Y/Rp','Y')
            call center
        
            xStep = ceiling((xMax-xMin)/15.0)
            yStep = ceiling((yMax-yMin)/15.0)
            
            xFirstTick = xStep*ceiling(xMin/xStep)
            yFirstTick = yStep*ceiling(yMin/yStep)
            
            call graf(xMin,xMax,xFirstTick,xStep,
     &                yMin,yMax,yFirstTick,yStep)
            
            call setvlt('RGREY')
            
            call penwid(0.5)
            call solid()
            
            do i=1,X_DRAW
            do j=1,Y_DRAW
               dens_xy(i,j)=log(dens_xy(i,j))/log(10.0)
            enddo
            enddo
            
            densMin = log(densMin)/log(10.0)
            densMax = log(densMax)/log(10.0)
       
            do i=1,numberContour
               densLevel = densMin +  
     &                       (densMax-densMin)*(i-1.0)/(numberContour-1.0)
               call contur(xDraw,X_DRAW,yDraw,Y_DRAW,dens_xy, densLevel)
            enddo
          

            do i=1,xNumberVector
            do j=1,yNumberVector
          
               x0 = xMin + (xMax-xMin)*(i-1.0)/(xNumberVector-1.0)
               y0 = yMin + (yMax-yMin)*(j-1.0)/(yNumberVector-1.0)
                    
               x1 = x0 - 0.5*vectorLength*velx_xy(xCellMap(i),yCellMap(j))/windVelsAmbient
               x2 = x0 + 0.5*vectorLength*velx_xy(xCellMap(i),yCellMap(j))/windVelsAmbient
               y1 = y0 - 0.5*vectorLength*vely_xy(xCellMap(i),yCellMap(j))/windVelsAmbient
               y2 = y0 + 0.5*vectorLength*vely_xy(xCellMap(i),yCellMap(j))/windVelsAmbient
               
               call rlvec(x1,y1,x2,y2,1021)
            
            enddo
            enddo
            
            call penwid(2.5)
            call dash
            call setrgb(0.5,0.5,1.0)
            
            rocheLevel_1 = fullRochePotential(lagrangePoint_1,0.0d0,0.0d0) !!! variable : !!! function  :
            rocheLevel_2 = fullRochePotential(lagrangePoint_2,0.0d0,0.0d0) !!! variable : !!! function  :
            rocheLevel_3 = fullRochePotential(lagrangePoint_3,0.0d0,0.0d0) !!! variable : !!! function  :

            call contur(xDraw,X_DRAW,yDraw,Y_DRAW,roch_xy,rocheLevel_1)
            call contur(xDraw,X_DRAW,yDraw,Y_DRAW,roch_xy,rocheLevel_2)
            call contur(xDraw,X_DRAW,yDraw,Y_DRAW,roch_xy,rocheLevel_3)
            
            call penwid(1.0)
            call solid
            call setrgb(1.0,0.0,0.0)
            
            call contur(xDraw,X_DRAW,yDraw,Y_DRAW,wave_xy,1.0)

            write (timeLine,1686) (time/period)

1686        format ("Time: ", f6.3)

            call messag(timeLine, 50, 20)
            
            call endgrf
            call disfin

         else

            do i=3,X_BLOCK-2
            do j=3,Y_BLOCK-2
               buffer_xy(2+0*SLICE_SIZE_XY+i-3+(X_BLOCK-4)*(j-3)+1) = dens(i,j,zSlice)
               buffer_xy(2+1*SLICE_SIZE_XY+i-3+(X_BLOCK-4)*(j-3)+1) = velx(i,j,zSlice)
               buffer_xy(2+2*SLICE_SIZE_XY+i-3+(X_BLOCK-4)*(j-3)+1) = vely(i,j,zSlice)
               buffer_xy(2+3*SLICE_SIZE_XY+i-3+(X_BLOCK-4)*(j-3)+1) = wave(i,j,zSlice)
            enddo
            enddo

            buffer_xy(1) = xOfset
            buffer_xy(2) = yOfset

            call MPI_SEND(buffer_xy,(2 + 4*SLICE_SIZE_XY),MPI_REAL,
     &                                0,971,MPI_COMM_WORLD,ierr)

! --- Draw xz slice

            if (yRank.eq.yRankCenter) then

               if (myRank.eq.specRank) then
             
                  do processor=1,(X_PROC-1)
                
                      call MPI_RECV(buffer_xz,(4*SLICE_SIZE_XZ),MPI_REAL,
     &                         specRank + processor,972,MPI_COMM_WORLD,istats,ierr)
                   
                      xProcOfset = processor*(X_BLOCK-4)
                   
                      do i=1,X_BLOCK-4
                      do k=1,Z_BLOCK-4
                         dens_xz(i+xProcOfset,k) = buffer_xz(0*SLICE_SIZE_XZ+i-1+(X_BLOCK-4)*(k-1)+1)
                         velx_xz(i+xProcOfset,k) = buffer_xz(1*SLICE_SIZE_XZ+i-1+(X_BLOCK-4)*(k-1)+1)
                         velz_xz(i+xProcOfset,k) = buffer_xz(2*SLICE_SIZE_XZ+i-1+(X_BLOCK-4)*(k-1)+1)
                         wave_xz(i+xProcOfset,k) = buffer_xz(3*SLICE_SIZE_XZ+i-1+(X_BLOCK-4)*(k-1)+1)
                      enddo
                      enddo
                
                  enddo
             
                  do i=1,X_BLOCK-4
                  do k=1,Z_BLOCK-4
                     dens_xz(i,k) = dens(i+2,ySlice,k+2)
                     wave_xz(i,k) = wave(i+2,ySlice,k+2)
                     velx_xz(i,k) = velx(i+2,ySlice,k+2)
                     velz_xz(i,k) = velz(i+2,ySlice,k+2)
                  enddo
                  enddo

! --------- Write to file all plot data

226         format ('./Plots/plotData_xz_',i5.5,'.dat')

            write (plotSaveFileNameXZ,226)  fileNumber
            write (*,*) 'writing to file ', plotSaveFileNameXZ
            open (5,file=plotSaveFileNameXZ,form='unformatted')
               do i=1,X_DRAW
               do k=1,Z_DRAW
                  write (5) dens_xz(i,k)
                  write (5) velx_xz(i,k)
                  write (5) velz_xz(i,k)
               enddo
               enddo
            write (5) time
            close (5)

! --------- Write to file all plot data
! ----------------------------------------------------------------------
! --- Really huge block ------------------------------------------------
! ----------------------------------------------------------------------

                  densMin = dens_xz(1,1)
                  densMax = densMin
                  
                  do i=1,X_DRAW
                  do k=1,Z_DRAW
                     densMax = max(dens_xz(i,k),densMax)
                     densMin = min(dens_xz(i,k),densMin)
                  enddo
                  enddo
                  
                  open (1,file='plotParameters.dat')
                     read (1,*) numberContour
                     read (1,*) xNumberVector
                     read (1,*) vectorLengthFile
                     read (1,*) concMin
                     read (1,*) concMax
                  close (1)
                  
                  if (concMin.ge.0.0) then
                     densMin = concMin*HYGE_MASS
                  endif
                
                  if (concMax.ge.0.0) then
                     densMax = concMax*HYGE_MASS
                  endif
                
                  if (vectorLengthFile.ge.0.0) then
                     vectorLength = vectorLengthFile
                  endif
                
                  do i=1,X_DRAW
                     xDraw(i) = (xGlob(i+2)-plntOrbit)/plntRadius
                  enddo
                 
                  do i=1,Y_DRAW
                     yDraw(i) = yGlob(i+2)/plntRadius
                  enddo

                  do i=1,Z_DRAW
                     zDraw(i) = zGlob(i+2)/plntRadius
                  enddo
                  
                  do i=1,X_DRAW
                  do k=1,Z_DRAW
                     roch_xz(i,k) = fullRochePotential(xGlob(i+2),
     &                                                 yGlob(ySlice + yRankCenter*(Y_BLOCK-4) + 2),
     &                                                 zGlob(k+2))
                  enddo
                  enddo
                  
                  xMin = xDraw(1)
                  xMax = xDraw(X_DRAW)
                 
                  zMin = zDraw(1)
                  zMax = zDraw(Z_DRAW)
                  
                  zNumberVector = int(xNumberVector*(zMax-zMin)/(xMax-xMin))
                  
                  j = 1
                  do i=1,X_DRAW-1
                
                     regularPoint = xMin +
     &                        (xMax-xMin)*(j-1.0)/(xNumberVector-1.0)
                
                     if (xDraw(i  ).le.regularPoint.and.
     &                   xDraw(i+1).ge.regularPoint.and.
     &                                      j.le.500) then
                        xCellMap(j) = i
                        j = j+1
                
                     endif
                
                  enddo
            
                  j = 1
                  do i=1,Z_DRAW-1
                
                     regularPoint = zMin +
     &                        (zMax-zMin)*(j-1.0)/(zNumberVector-1.0)
                
                     if (zDraw(i  ).le.regularPoint.and.
     &                   zDraw(i+1).ge.regularPoint.and.
     &                                      j.le.500) then
                        zCellMap(j) = i
                        j = j+1
                
                     endif
                
                  enddo
                
                  call page(1700+350,int(1700*(zMax-zMin)/(xMax-xMin))+300)
                  call pagmod('NONE')
                  call filmod('DELETE')
                  CALL METAFL ('png') !!! lowercase
                  
231               format ('./Plots/plot_xz_',i5.5,'.png') !!! rename plot files : ./Data/data_ -> plot_
                  write (plotFileNameXZ,231)  fileNumber !!! variable : plotFileName -> plotName
            
                  call setfil(plotFileNameXZ) !!! variable : plotFileName -> plotName
                  call disini
                    
                  CALL PSFONT('Times-Roman')
                  call nochek
                  call shdmod('CELL','CONTUR')
                  call digits(1,'XYZ')
                  call texmod('ON')
                    
                  call axslen(1700,int(1700*(zMax-zMin)/(xMax-xMin)))    
                  call axspos(300, int(1700*(zMax-zMin)/(xMax-xMin))+300)
                 
                  call name('X/Rp','X')
                  call name('Z/Rp','Z')
                  call center
                
                  xStep = ceiling((xMax-xMin)/15.0)
                  zStep = ceiling((zMax-zMin)/5.0)
                  
                  xFirstTick = xStep*ceiling(xMin/xStep)
                  zFirstTick = zStep*ceiling(zMin/zStep)
                  
                  call graf(xMin,xMax,xFirstTick,xStep,
     &                      zMin,zMax,zFirstTick,zStep)
                  
                  call setvlt('RGREY')
                  
                  call penwid(0.5)
                  call solid()
                  
                  do i=1,X_DRAW
                  do k=1,Z_DRAW
                     dens_xz(i,k)=log(dens_xz(i,k))/log(10.0)
                  enddo
                  enddo
                  
                  densMin = log(densMin)/log(10.0)
                  densMax = log(densMax)/log(10.0)
                
                  do i=1,numberContour
                     densLevel = densMin +  
     &                             (densMax-densMin)*(i-1.0)/(numberContour-1.0)
                     call contur(xDraw,X_DRAW,zDraw,Z_DRAW,dens_xz, densLevel)
                  enddo
            
                
                  do i=1,xNumberVector
                  do k=1,zNumberVector
            
                     x0 = xMin + (xMax-xMin)*(i-1.0)/(xNumberVector-1.0)
                     z0 = zMin + (zMax-zMin)*(k-1.0)/(zNumberVector-1.0)
                          
                     x1 = x0 - 0.5*vectorLength*velx_xz(xCellMap(i),zCellMap(k))/windVelsAmbient
                     x2 = x0 + 0.5*vectorLength*velx_xz(xCellMap(i),zCellMap(k))/windVelsAmbient
                     z1 = z0 - 0.5*vectorLength*velz_xz(xCellMap(i),zCellMap(k))/windVelsAmbient
                     z2 = z0 + 0.5*vectorLength*velz_xz(xCellMap(i),zCellMap(k))/windVelsAmbient
                     
                     call rlvec(x1,z1,x2,z2,1021)
                  
                  enddo
                  enddo
                  
                  call penwid(2.5)
                  call dash
                  call setrgb(0.5,0.5,1.0)
                  
                  rocheLevel_1 = fullRochePotential(lagrangePoint_1,0.0d0,0.0d0) !!! variable : !!! function  :
                  rocheLevel_2 = fullRochePotential(lagrangePoint_2,0.0d0,0.0d0) !!! variable : !!! function  :
                  rocheLevel_3 = fullRochePotential(lagrangePoint_3,0.0d0,0.0d0) !!! variable : !!! function  :
                
                  call contur(xDraw,X_DRAW,zDraw,Z_DRAW,roch_xz,rocheLevel_1)
                  call contur(xDraw,X_DRAW,zDraw,Z_DRAW,roch_xz,rocheLevel_2)
                  call contur(xDraw,X_DRAW,zDraw,Z_DRAW,roch_xz,rocheLevel_3)
                  
                  call penwid(1.0)
                  call solid
                  call setrgb(1.0,0.0,0.0)
                  
                  call contur(xDraw,X_DRAW,zDraw,Z_DRAW,wave_xz,1.0)
                
                  write (timeLine,1686) (time/period)
                
                  call messag(timeLine, 50, 20)
                  
                  call endgrf
                  call disfin

! ----------------------------------------------------------------------
! --- Really huge block ------------------------------------------------
! ----------------------------------------------------------------------
             
               else

                   do i=3,X_BLOCK-2
                   do k=3,Z_BLOCK-2
                      buffer_xz(0*SLICE_SIZE_XZ+i-3+(X_BLOCK-4)*(k-3)+1) = dens(i,ySlice,k)
                      buffer_xz(1*SLICE_SIZE_XZ+i-3+(X_BLOCK-4)*(k-3)+1) = velx(i,ySlice,k)
                      buffer_xz(2*SLICE_SIZE_XZ+i-3+(X_BLOCK-4)*(k-3)+1) = velz(i,ySlice,k)
                      buffer_xz(3*SLICE_SIZE_XZ+i-3+(X_BLOCK-4)*(k-3)+1) = wave(i,ySlice,k)
                   enddo
                   enddo
             
                   call MPI_SEND(buffer_xz,(4*SLICE_SIZE_XZ),MPI_REAL,
     &                                 specRank,972,MPI_COMM_WORLD,ierr)
             
               endif

            endif

         endif
        
         call MPI_BARRIER(MPI_COMM_WORLD,ierr)        
        
! ----------------------------------------------------------------------
        
      end subroutine drawPlot

! ----------------------------------------------------------------------
! --- The best ever known program --------------------------------------
! ----------------------------------------------------------------------
! ----------------------------------------------------------------------
! --- The best ever known program --------------------------------------
! ----------------------------------------------------------------------
! ----------------------------------------------------------------------
! --- The best ever known program --------------------------------------
! ----------------------------------------------------------------------
! ----------------------------------------------------------------------
! --- The best ever known program --------------------------------------
! ----------------------------------------------------------------------
! ----------------------------------------------------------------------
! --- The best ever known program --------------------------------------
! ----------------------------------------------------------------------

      subroutine saveLine

         implicit none
       
         include "mpif.h"
         
! ----------------------------------------------------------------------
         
         common /localCord/ x, y, z

         common /globCoord/ xGlob, yGlob, zGlob

         common /chiefData/ dens, velx, vely, velz,
     &                      pres, enth, srho, wave

         common /procStuff/ myRank,
     &                         xRank, yRank,
     &                         xOfset,yOfset,
     &                      xLeftBlock,xRghtBlock,
     &                      yLeftBlock,yRghtBlock,
     &                         ialgn,timeStep,courantCoef,
     &                      imin1,jmin1,imax1,jmax1,
     &                      imin2,imax2,jmin2,jmax2,
     &                         imax,jmax,kmax,
     &                      ialgn2,errorFlag

         common /physParam/ prop, gravMass, plntOrbit, omega, innrRadius

         common /plntParam/ plntRadius, lagrangePoint_1,
     &                                  lagrangePoint_2,
     &                                  lagrangePoint_3

         common /windParam/ windDensAmbient,windVelsAmbient,windTempAmbient

         common /someCount/ fileNumber

         common /timeParam/ time, period

         common /densMinim/ densMin, iteration

         common /specAreas/ plntZone, starZone

! ----------------------------------------------------------------------

         real(8) :: fullRochePotential

! ----------------------------------------------------------------------

         real(8) ::     x(X_BLOCK),     y(Y_BLOCK),     z(Z_BLOCK)
         real(8) :: xGlob(X_TOTAL), yGlob(Y_TOTAL), zGlob(Z_TOTAL)

         real(8) :: dens(X_BLOCK,Y_BLOCK,Z_BLOCK)
         real(8) :: velx(X_BLOCK,Y_BLOCK,Z_BLOCK)
         real(8) :: vely(X_BLOCK,Y_BLOCK,Z_BLOCK)
         real(8) :: velz(X_BLOCK,Y_BLOCK,Z_BLOCK)
         real(8) :: pres(X_BLOCK,Y_BLOCK,Z_BLOCK)
         real(8) :: enth(X_BLOCK,Y_BLOCK,Z_BLOCK)
         real(8) :: srho(X_BLOCK,Y_BLOCK,Z_BLOCK)
         real(8) :: wave(X_BLOCK,Y_BLOCK,Z_BLOCK)

         real(8) :: densMin

         integer(4) :: myRank
         integer(4) :: xRank,  yRank
         integer(4) :: xOfset, yOfset
         integer(4) :: xLeftBlock, xRghtBlock
         integer(4) :: yLeftBlock, yRghtBlock

         integer(4) :: iteration

         integer(4) :: imin1, jmin1, imax1, jmax1
         integer(4) :: imin2, jmin2, imax2, jmax2

         integer(4) :: imax, jmax, kmax

         integer(4) :: ialgn, ialgn2

         real(8) :: timeStep, courantCoef

         real(8) :: errorFlag

         real(8) :: prop, gravMass, plntOrbit, omega, innrRadius

         real(8) :: plntRadius

         real(8) :: lagrangePoint_1
         real(8) :: lagrangePoint_2
         real(8) :: lagrangePoint_3
         
         real(8) :: windDensAmbient,windVelsAmbient,windTempAmbient

         real(8) :: time, period

         integer(4) :: fileNumber

! ----------------------------------------------------------------------

         integer(4) :: numberContour

         integer(4) :: xNumberVector
         integer(4) :: yNumberVector
         integer(4) :: zNumberVector
       
         real(8) :: bufferLine(7*X_DRAW*X_PROC)

         real(8) :: xLine(X_DRAW*X_PROC)
         real(8) :: densLine(X_DRAW*X_PROC)
         real(8) :: velxLine(X_DRAW*X_PROC)
         real(8) :: velyLine(X_DRAW*X_PROC)
         real(8) :: velzLine(X_DRAW*X_PROC)
         real(8) :: presLine(X_DRAW*X_PROC)
         real(8) :: plntZoneLine(X_DRAW*X_PROC)

         real(4) :: dens_xz(X_DRAW,Y_DRAW)
         real(4) :: wave_xz(X_DRAW,Y_DRAW)
         real(4) :: velx_xz(X_DRAW,Y_DRAW)
         real(4) :: velz_xz(X_DRAW,Y_DRAW)
         real(4) :: roch_xz(X_DRAW,Y_DRAW)
         
         integer(4) :: processor

         integer(4) :: xProcOfset
         integer(4) :: yProcOfset
 
         integer(4) :: ierr, istats(MPI_STATUS_SIZE)
         
         real(4) :: x0,y0,z0
         real(4) :: x1,y1,z1
         real(4) :: x2,y2,z2

         integer(4) :: i,j,k
         integer(4) :: xSlice, ySlice, zSlice
         integer(4) :: xRankCenter, yRankCenter
         integer(4) :: specRank

         integer(4) :: plntZone(X_BLOCK,Y_BLOCK,Z_BLOCK)
         integer(4) :: starZone(X_BLOCK,Y_BLOCK,Z_BLOCK)

         character(len=25) :: plotSaveFileNameXZ

! ----------------------------------------------------------------------
        
         zSlice = 1
         
         do i=1,Z_BLOCK-1
            if (z(i  ).le.0.0.and.
     &          z(i+1).gt.0.0) then
               zSlice = i
               goto 220
            endif
         enddo

220      continue

         xRankCenter = (X_PROC/2)
         yRankCenter = (Y_PROC/2)
         specRank = yRankCenter*X_PROC
         ySlice = 3
#ifdef BLOCKED
         specRank = xRankCenter
         xSlice = 3
#endif

         zSlice = (Z_BLOCK/2)
         

         if (yRank.eq.yRankCenter) then

            if (myRank.eq.specRank) then
             
               do processor=1,(X_PROC-1)
               
                  call MPI_RECV(bufferLine,(7*X_BLOCK),MPI_DOUBLE_PRECISION,
     &                     specRank + processor,973,MPI_COMM_WORLD,istats,ierr)
                
                  xProcOfset = processor*X_BLOCK
                
                  do i=1,X_BLOCK
                     xLine(i+xProcOfset)    = bufferLine(0*X_BLOCK+(i-1)+1)
                     densLine(i+xProcOfset) = bufferLine(1*X_BLOCK+(i-1)+1)
                     velxLine(i+xProcOfset) = bufferLine(2*X_BLOCK+(i-1)+1)
                     velyLine(i+xProcOfset) = bufferLine(3*X_BLOCK+(i-1)+1)
                     velzLine(i+xProcOfset) = bufferLine(4*X_BLOCK+(i-1)+1)
                     presLine(i+xProcOfset) = bufferLine(5*X_BLOCK+(i-1)+1)
                     plntZoneLine(i+xProcOfset) = bufferLine(6*X_BLOCK+(i-1)+1)
                  enddo
               
               enddo
             
               do i=1,X_BLOCK
                  xLine(i)    = x(i)
                  densLine(i) = dens(i,ySlice,zSlice)
                  velxLine(i) = velx(i,ySlice,zSlice)
                  velyLine(i) = vely(i,ySlice,zSlice)
                  velzLine(i) = velz(i,ySlice,zSlice)
                  presLine(i) = pres(i,ySlice,zSlice)
                  plntZoneLine(i) = plntZone(i,ySlice,zSlice)
               enddo

! --------- Write to file all plot data

117            format ('./Data/dataLine_',i5.5,'.dat')
             
               write (plotSaveFileNameXZ,117)  iteration
               write (*,*) 'writing to file ', plotSaveFileNameXZ
               open (5,file=plotSaveFileNameXZ)
                  do i=1,X_BLOCK*X_PROC
                     write (5,*) (xLine(i) - plntOrbit)/plntRadius, densLine(i), 
     &                           velxLine(i), velyLine(i), velzLine(i), 
     &                           presLine(i), plntZoneLine(i)
                  enddo
               write (5,*) time
               close (5)

! --------- Write to file all plot data
! ----------------------------------------------------------------------
! --- Really huge block ------------------------------------------------
! ----------------------------------------------------------------------
          
            else

                do i=1,X_BLOCK
                   bufferLine(0*X_BLOCK+(i-1)+1) = x(i)
                   bufferLine(1*X_BLOCK+(i-1)+1) = dens(i,ySlice,zSlice)
                   bufferLine(2*X_BLOCK+(i-1)+1) = velx(i,ySlice,zSlice)
                   bufferLine(3*X_BLOCK+(i-1)+1) = vely(i,ySlice,zSlice)
                   bufferLine(4*X_BLOCK+(i-1)+1) = velz(i,ySlice,zSlice)
                   bufferLine(5*X_BLOCK+(i-1)+1) = pres(i,ySlice,zSlice)
                   bufferLine(6*X_BLOCK+(i-1)+1) = plntZone(i,ySlice,zSlice)
                enddo
          
                call MPI_SEND(bufferLine,(7*X_BLOCK),MPI_DOUBLE_PRECISION,
     &                              specRank,973,MPI_COMM_WORLD,ierr)
          
            endif

         endif

        
         call MPI_BARRIER(MPI_COMM_WORLD,ierr)        
        
! ----------------------------------------------------------------------
        
      end subroutine saveLine

! ----------------------------------------------------------------------
! --- The best ever known program --------------------------------------
! ----------------------------------------------------------------------
! ----------------------------------------------------------------------
! --- The best ever known program --------------------------------------
! ----------------------------------------------------------------------
! ----------------------------------------------------------------------
! --- The best ever known program --------------------------------------
! ----------------------------------------------------------------------
! ----------------------------------------------------------------------
! --- The best ever known program --------------------------------------
! ----------------------------------------------------------------------

      subroutine checkSymmetry

         implicit none
       
         include "mpif.h"
         
! ----------------------------------------------------------------------
         
         common /localCord/ x, y, z

         common /globCoord/ xGlob, yGlob, zGlob

         common /chiefData/ dens, velx, vely, velz,
     &                      pres, enth, srho, wave

         common /procStuff/ myRank,
     &                         xRank, yRank,
     &                         xOfset,yOfset,
     &                      xLeftBlock,xRghtBlock,
     &                      yLeftBlock,yRghtBlock,
     &                         ialgn,timeStep,courantCoef,
     &                      imin1,jmin1,imax1,jmax1,
     &                      imin2,imax2,jmin2,jmax2,
     &                         imax,jmax,kmax,
     &                      ialgn2,errorFlag

         common /physParam/ prop, gravMass, plntOrbit, omega, innrRadius

         common /plntParam/ plntRadius, lagrangePoint_1,
     &                                  lagrangePoint_2,
     &                                  lagrangePoint_3

         common /windParam/ windDensAmbient,windVelsAmbient,windTempAmbient

         common /someCount/ fileNumber

         common /timeParam/ time, period

         common /densMinim/ densMin, iteration

         common /specAreas/ plntZone, starZone

! ----------------------------------------------------------------------

         real(8) :: fullRochePotential

! ----------------------------------------------------------------------

         real(8) ::     x(X_BLOCK),     y(Y_BLOCK),     z(Z_BLOCK)
         real(8) :: xGlob(X_TOTAL), yGlob(Y_TOTAL), zGlob(Z_TOTAL)

         real(8) :: dens(X_BLOCK,Y_BLOCK,Z_BLOCK)
         real(8) :: velx(X_BLOCK,Y_BLOCK,Z_BLOCK)
         real(8) :: vely(X_BLOCK,Y_BLOCK,Z_BLOCK)
         real(8) :: velz(X_BLOCK,Y_BLOCK,Z_BLOCK)
         real(8) :: pres(X_BLOCK,Y_BLOCK,Z_BLOCK)
         real(8) :: enth(X_BLOCK,Y_BLOCK,Z_BLOCK)
         real(8) :: srho(X_BLOCK,Y_BLOCK,Z_BLOCK)
         real(8) :: wave(X_BLOCK,Y_BLOCK,Z_BLOCK)

         real(8) :: densMin

         integer(4) :: myRank
         integer(4) :: xRank,  yRank
         integer(4) :: xOfset, yOfset
         integer(4) :: xLeftBlock, xRghtBlock
         integer(4) :: yLeftBlock, yRghtBlock

         integer(4) :: iteration

         integer(4) :: imin1, jmin1, imax1, jmax1
         integer(4) :: imin2, jmin2, imax2, jmax2

         integer(4) :: imax, jmax, kmax

         integer(4) :: ialgn, ialgn2

         real(8) :: timeStep, courantCoef

         real(8) :: errorFlag

         real(8) :: prop, gravMass, plntOrbit, omega, innrRadius

         real(8) :: plntRadius

         real(8) :: lagrangePoint_1
         real(8) :: lagrangePoint_2
         real(8) :: lagrangePoint_3
         
         real(8) :: windDensAmbient,windVelsAmbient,windTempAmbient

         real(8) :: time, period

         integer(4) :: fileNumber

! ----------------------------------------------------------------------

         integer(4) :: numberContour

         integer(4) :: xNumberVector
         integer(4) :: yNumberVector
         integer(4) :: zNumberVector
       
         real(8) :: bufferDens(X_BLOCK*Y_BLOCK*Z_BLOCK)

         real(8) :: densValue
         
         integer(4) :: processor

         integer(4) :: xProcOfset
         integer(4) :: yProcOfset
 
         integer(4) :: ierr, istats(MPI_STATUS_SIZE)
         
         real(4) :: x0,y0,z0
         real(4) :: x1,y1,z1
         real(4) :: x2,y2,z2

         integer(4) :: i,j,k
         integer(4) :: xSlice, ySlice, zSlice
         integer(4) :: xRankCenter, yRankCenter
         integer(4) :: specRank

         integer(4) :: variable

         integer(4) :: plntZone(X_BLOCK,Y_BLOCK,Z_BLOCK)
         integer(4) :: starZone(X_BLOCK,Y_BLOCK,Z_BLOCK)

         character(len=25) :: plotSaveFileNameXZ

! ----------------------------------------------------------------------
        
         if (xRank.lt.(X_PROC/2)) then

            call MPI_RECV(bufferDens,(X_BLOCK*Y_BLOCK*Z_BLOCK),MPI_DOUBLE_PRECISION,
     &               (yRank*Y_PROC + (X_PROC-1-xRank)),974,MPI_COMM_WORLD,istats,ierr)
            
            do i=1,X_BLOCK
            do j=1,Y_BLOCK
            do k=1,Z_BLOCK
               densValue = bufferDens(((X_BLOCK-i+1)-1) + 
     &                                (j-1)*X_BLOCK + 
     &                                (k-1)*X_BLOCK*Y_BLOCK + 1)
               if (dens(i,j,k).ne.densValue) then
                  print*, "=========================="
                  print*, "===Assimmetry detected!==="
                  print*, "=========================="
               endif
                  
            enddo
            enddo
            enddo

c           print*, "Everything is OK"
               
         else

            do i=1,X_BLOCK
            do j=1,Y_BLOCK
            do k=1,Z_BLOCK
               bufferDens((i-1) + 
     &                    (j-1)*X_BLOCK + 
     &                    (k-1)*X_BLOCK*Y_BLOCK + 1) = dens(i,j,k)
            enddo
            enddo
            enddo
          
            call MPI_SEND(bufferDens,(X_BLOCK*Y_BLOCK*Z_BLOCK),MPI_DOUBLE_PRECISION,
     &                       (yRank*Y_PROC + (X_PROC-1-xRank)),974,MPI_COMM_WORLD,ierr)
          
         endif

        
         call MPI_BARRIER(MPI_COMM_WORLD,ierr)        
        
! ----------------------------------------------------------------------
        
      end subroutine checkSymmetry

! ----------------------------------------------------------------------
! --- The best ever known program --------------------------------------
! ----------------------------------------------------------------------
! ----------------------------------------------------------------------
! --- The best ever known program --------------------------------------
! ----------------------------------------------------------------------
! ----------------------------------------------------------------------
! --- The best ever known program --------------------------------------
! ----------------------------------------------------------------------
! ----------------------------------------------------------------------
! --- The best ever known program --------------------------------------
! ----------------------------------------------------------------------

      function fullRochePotential (x,y,z)

         implicit none

! ----------------------------------------------------------------------

      common /physParam/ prop, gravMass, plntOrbit, omega, innrRadius
         real(8) :: prop,gravMass,plntOrbit,omega, innrRadius
         real(8) :: fullRochePotential, x, y, z

! ----------------------------------------------------------------------
       
         real(8) :: starDist, plntDist
         real(8) :: relStarMass, relPlntMass
         real(8) :: massCenter
         real(8) :: massCenterDist

! ----------------------------------------------------------------------

         starDist = dsqrt(x**2+y**2+z**2)
         starDist = starDist !!! delete

         plntDist = dsqrt((x-plntOrbit)**2+y**2+z**2)
         plntDist = plntDist !!! delete
         
         relStarMass = prop
         relPlntMass = 1 - prop

         massCenter = (1-prop)*plntOrbit
         massCenterDist = (x-massCenter)**2 + y**2

         fullRochePotential =         -prop*gravMass/starDist !!! function :, variable : prop -> relStarMass,
     &            - relPlntMass*gravMass/plntDist
     &            - 0.5*((x-(1-prop)*plntOrbit)**2+y**2)*omega**2 !!! expression -> massCenterDist

! ----------------------------------------------------------------------

      end function fullRochePotential

! ----------------------------------------------------------------------
! --- The best ever known program --------------------------------------
! ----------------------------------------------------------------------
