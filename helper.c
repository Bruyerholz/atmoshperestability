#include <stdio.h>
#include <math.h>
#include <sys/time.h>

double lr=0;

double second_(void)
 {
  double res,s1,s2;
  struct timeval tv;
  struct timezone tz;
  tz.tz_minuteswest=0;
  tz.tz_dsttime=0;
  gettimeofday(&tv,&tz);
  s1=tv.tv_sec+0.0;
  s2=tv.tv_usec+0.0;
  res=s1+s2*1.0e-6;
  return(res);
 }

void flush_(void)
 {
  fflush(stdout);
 }

void sleep(int sec)
 {
  sleep(sec);
 }
