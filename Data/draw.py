#!/usr/bin/python

import numpy as np
import matplotlib.pyplot as plt

import sys

from math import sqrt, log10, pi, sin

fileNumber = int(str(sys.argv[1]))
fileNumberString = str(fileNumber).zfill(5)

xProc = 4
xBlock = 44

dataLineFileName = "dataLine_" + fileNumberString + ".dat"
dataLineFileStartName = "dataLine_00000.dat"

dataLineFile      = open(dataLineFileName, 'r')
dataLineFileStart = open(dataLineFileStartName, 'r')

xCoord    = np.zeros(shape=(xProc*xBlock))

dens      = np.zeros(shape=(xProc*xBlock))
velx      = np.zeros(shape=(xProc*xBlock))
vely      = np.zeros(shape=(xProc*xBlock))
velz      = np.zeros(shape=(xProc*xBlock))

velxStart = np.zeros(shape=(xProc*xBlock))
densStart = np.zeros(shape=(xProc*xBlock))

densAssym = np.zeros(shape=(xProc*xBlock))
velxAssym = np.zeros(shape=(xProc*xBlock))
velyAssym = np.zeros(shape=(xProc*xBlock))
velzAssym = np.zeros(shape=(xProc*xBlock))

plntZone  = np.zeros(shape=(xProc*xBlock))
densDiff  = np.zeros(shape=(xProc*xBlock))

for i in xrange(xProc*xBlock):
    line = dataLineFile.readline()
    xCoord[i] = line.split()[0]
    dens[i] = line.split()[1]
    velx[i] = line.split()[2]
    vely[i] = line.split()[3]
    velz[i] = line.split()[4]
    dens[i] = log10(dens[i])
    plntZone[i] = line.split()[6]
    
time = float(dataLineFile.readline().split()[0])

period = 305620.89905149495

timePeriod = time/period

for i in xrange(xProc*xBlock):
    line = dataLineFileStart.readline()
    densStart[i] = line.split()[1]
    velxStart[i] = line.split()[2]
    densStart[i] = log10(densStart[i])

for i in xrange(xProc*xBlock):
    densAssym[i] = 1.0e100*(dens[i] - dens[xProc*xBlock-i-1])
    velxAssym[i] = 1.0e100*(velx[i] - velx[xProc*xBlock-i-1])
    velyAssym[i] = 1.0e100*(vely[i] - vely[xProc*xBlock-i-1])
    velzAssym[i] = 1.0e100*(velz[i] - velz[xProc*xBlock-i-1])
    densDiff[i]  = (dens[i] - densStart[i])

fig = plt.figure()

ax1 = fig.add_subplot(111)

ax1.set_title("Plot title...")    
ax1.set_xlabel('your x label..')
ax1.set_ylabel('your y label...')

ax1.plot(xCoord,dens, 'r.', label='the data')
ax1.plot(xCoord,plntZone, 'g.', label='the data')
ax1.plot(xCoord,densAssym, 'b.', label='the data')
ax1.plot(xCoord,densDiff, 'y.', label='the data')
#ax1.plot(xCoord,velyAssym, 'm.', label='the data')

leg = ax1.legend()

saveFigureFileName = "result_" + fileNumberString + ".png"
ax1.text(0.0, 1.06, str(timePeriod),
         horizontalalignment='center',
         verticalalignment='center',
         transform=ax1.transAxes)


fig.savefig(saveFigureFileName)

print "Figure saved in file", saveFigureFileName
