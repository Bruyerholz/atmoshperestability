#define GAMMA_CONST (5.0/3.0)
#define GRAV_CONST (0.667d-7)
#define GRAV_CONST_CLONE (0.667e-7)
#define GAS_CONST (8.3143d+7)
#define HYGE_MASS (1.66d-24)
#define BOLTZ_CONST (1.381e-16)

#define SUN_MASS (2.0d33)
#define JUP_MASS (1.8986e30)

#define SUN_RADIUS (0.696d11)
#define JUP_RADIUS (6.9911e9)
#define ASTRO_UNIT (1.49597871e13)

#define PI_CONST (3.141592d0)

#define GAMMA_CONST (5.0/3.0)
