! ----------------------------------------------------------------------

      implicit none

! ----------------------------------------------------------------------

#     include "constants.h"
#     include "physicalConstants.h"

! ----------------------------------------------------------------------

      real(8) :: x(X_TOTAL)
      real(8) :: y(Y_TOTAL)
      real(8) :: z(Z_TOTAL)

      real(8) :: plntOrbit, plntRadius, innrRadius, kernRadius
      real(8) :: plntMass, atmsTemp
 
      real(8) :: starMass, gravMass, omega, period
  
      real(8) :: kernGravAccel, kernScaleHeight
      real(8) :: innrGravAccel, innrScaleHeight
      real(8) :: surfGravAccel, surfScaleHeight

      real(8) :: extentCoef

      real(8) :: innerStep

      real(8) :: xStep, xCntrDist
      real(8) :: yStep, yCntrDist
      real(8) :: zStep, zCntrDist
 
      integer(4) :: i, j, k

      plntOrbit = 10.1*SUN_RADIUS

      plntRadius = 0.135*SUN_RADIUS
      innrRadius = 0.75*plntRadius
      kernRadius = 0.75*innrRadius

      plntMass = 0.0006*SUN_MASS
      starMass = 1.1*SUN_MASS

      atmsTemp = 7000
      gravMass = GRAV_CONST*(starMass+plntMass)

      omega = dsqrt(gravMass/plntOrbit**3)
      period = 2.d0*PI_CONST/omega

      print*, "Orbital period: ", period/(24.0*60.0*60.0), "days"

      kernGravAccel = (GRAV_CONST*plntMass)/(kernRadius**2)
      innrGravAccel = (GRAV_CONST*plntMass)/(innrRadius**2)
      surfGravAccel = (GRAV_CONST*plntMass)/(plntRadius**2)

      print*, "Gravitational acceleration for this atmosphere: "
      print*, "on kern radius is ", kernGravAccel, "cm/(sec**2)"
      print*, "on innr radius is ", innrGravAccel, "cm/(sec**2)"
      print*, "on surf radius is ", surfGravAccel, "cm/(sec**2)"

      kernScaleHeight = (BOLTZ_CONST*atmsTemp)/(HYGE_MASS*kernGravAccel)
      innrScaleHeight = (BOLTZ_CONST*atmsTemp)/(HYGE_MASS*innrGravAccel)
      surfScaleHeight = (BOLTZ_CONST*atmsTemp)/(HYGE_MASS*surfGravAccel)

      print*, "Scale height for this atmosphere: "
      print*, "on kern radius is ", kernScaleHeight, "cm",
     &                              kernScaleHeight/plntRadius, "Rpl"
      print*, "on innr radius is ", innrScaleHeight, "cm",
     &                              innrScaleHeight/plntRadius, "Rpl"
      print*, "on surf radius is ", surfScaleHeight, "cm",
     &                              surfScaleHeight/plntRadius, "Rpl"

      extentCoef = 1.011

      innerStep = innrScaleHeight*extentCoef - 
     &                 innrRadius*(extentCoef - 1)

      innerStep = 0.01931*plntRadius
      innerStep = 2*innerStep

      print*, "Coord step in center of planet is: "
      print*, innerStep, "cm", innerStep/plntRadius, "Rpl"

      xStep = innerStep
      xCntrDist = xStep/2.0
      
      x((X_TOTAL/2))   = plntOrbit - xCntrDist
      x((X_TOTAL/2)+1) = plntOrbit + xCntrDist

      do i=1, ((X_TOTAL/2)-1)

         xCntrDist = xCntrDist + (xStep/2.0)
         xStep = xStep*extentCoef
         xCntrDist = xCntrDist + (xStep/2.0)

         x((X_TOTAL/2)-i)   = plntOrbit - xCntrDist
         x((X_TOTAL/2)+1+i) = plntOrbit + xCntrDist

      enddo

      yStep = innerStep
      yCntrDist = yStep/2.0
      
      y((Y_TOTAL/2))   = -yCntrDist
      y((Y_TOTAL/2)+1) = +yCntrDist

      do i=1, ((Y_TOTAL/2)-1)

         yCntrDist = yCntrDist + (yStep/2.0)
         yStep = yStep*extentCoef
         yCntrDist = yCntrDist + (yStep/2.0)

         y((Y_TOTAL/2)-i)   = -yCntrDist
         y((Y_TOTAL/2)+1+i) = +yCntrDist

      enddo

c     zStep = 0.05*plntRadius
      zStep = innerStep
      zCntrDist = zStep/2.0
      
      z((Z_TOTAL/2))   = -zCntrDist
      z((Z_TOTAL/2)+1) = +zCntrDist

      do i=1, ((Z_TOTAL/2)-1)

         zCntrDist = zCntrDist + (zStep/2.0)
         zStep = zStep*extentCoef
         zCntrDist = zCntrDist + (zStep/2.0)

         z((Z_TOTAL/2)-i)   = -zCntrDist
         z((Z_TOTAL/2)+i+1) = +zCntrDist

      enddo

      open (1,file="./Data/x.dat",status='replace')
      write (1,*) X_TOTAL
      do i=1,X_TOTAL
            write (1,*) x(i), (x(i) - plntOrbit)/plntRadius
      enddo
      close (1)

      open (1,file="./Data/y.dat",status='replace')
      write (1,*) Y_TOTAL
      do i=1,Y_TOTAL
            write (1,*) y(i), y(i)/plntRadius
      enddo
      close (1)

      open (1,file="./Data/z.dat",status='replace')
      write (1,*) Z_TOTAL
      do i=1,Z_TOTAL
            write (1,*) z(i), z(i)/plntRadius
      enddo
      close (1)

      end
